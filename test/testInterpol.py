#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 19:59:56 2019

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt
import pythag.quadrature.classicgauss as pqg
import pythag.misc.interpol as ppi

nG = 26
nF = 100
polyOrder = 12
interpOrder = 9

# Define a quadrature rule
quadRule = pqg.ChebyshevRule()

# Compute coarse and fine grid
xG, _ = quadRule.getAsymptoticApproximation(nG)
xF, _ = quadRule.getAsymptoticApproximation(nF)
xAll = np.hstack([xG, xF])

yAll = quadRule.rec.evalMonicPoly(xAll, polyOrder)
yG, yF = np.split(yAll, [nG])

# Interpolate coarse data
hosgi = ppi.HighOrderStructGridInterpolator1D(
        interpOrder, xG, xF, boundary='WALL')

yI = hosgi.interpolate(yG)

# Plot data
plt.plot(xG, yG, 's', label='Coarse data', markersize=16)
plt.plot(xF, yF, 'o-', label='Fine data')
plt.plot(xF, yI, '^--', label='Interpolated data')
plt.legend()
plt.grid()
