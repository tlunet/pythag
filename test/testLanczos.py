#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 11:39:28 2018

@author: telu
"""
import numpy as np
import pythag.algebra.tridiag as td
import pythag.quadrature.classicgauss as quad
import matplotlib.pyplot as plt

# Define quarature rule
n = 50
qRule = 'LEGEND'
if qRule == 'LEGEND':
    quadRule = quad.LegendreRule()
elif qRule.startswith('CHEBY'):
    kind = int(qRule[-1])
    quadRule = quad.ChebyshevRule(kind=kind)
else:
    raise ValueError('Wrong value for qRule')

print(' -- Compute node and weights')
nodes, weights = quadRule.getNodesAndWeights(n)

print(' -- Build A matrix operator')
A = td.quadRuleMatrix(nodes, weights)

# plt.figure('A')
# plt.imshow(A)
# plt.colorbar()

print(' -- Apply Lanczos algorithm')
QL, alphaL, betaL = td.tridiagLanczos(A)
DL = QL.dot(QL.T)

plt.figure('Lanczos, Q')
plt.imshow(QL, vmin=-0.4, vmax=1.)
plt.colorbar()

print(' -- Apply Givens algorithm')
QG, alphaG, betaG = td.tridiagGivens(A)
DG = QG.dot(QG.T)

plt.figure('Givens, Q')
plt.imshow(QG, vmin=-0.4, vmax=1.)
plt.colorbar()

print(' -- Apply RPKW algorithm')
alphaR, betaR = td.tridiagRPKW(nodes, weights)

print(betaG)
print(betaR)

k = np.arange(n, dtype=float)
betaTh = k**2/(2*k+1)/(2*k-1)
betaTh[0] = 2

allAlpha = np.block([[alphaL], [alphaG]]).T
allBeta = np.block([[betaL], [betaG], [betaTh]]).T
