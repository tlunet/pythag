#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 13:22:32 2018

@author: lunet
"""
import numpy as np
import pythag.algebra.tridiag as td
import pythag.quadrature.classic as quad
import matplotlib.pyplot as plt

lC = plt.rcParams['axes.prop_cycle'].by_key()['color']

lN = [(20, '--'),
      (100, '-.'),
      (200, ':')]
lNmax = 200
lQuadRule = [(quad.NewtonCotesRule(), lC[3], 's'),
             (quad.FejerRule(kind=1), lC[0], '>'),
             (quad.FejerRule(kind=2), lC[1], '<'),
             (quad.ClenshawCurtisRule(), lC[2], 'o')]

# Analytical coefficients
kTh = np.arange(lNmax, dtype=float)
betaTh = kTh**2/(2*kTh+1)/(2*kTh-1)
betaTh[0] = 2

N = None
quadRule = None
col = None
sty = None
ls = None


def computeAndPlot():

    nodes, weights = quadRule.getNodesAndWeights(N)
    # weights *= (1-nodes)**(-0.5)*(1+nodes)**(-0.5)
    alpha, beta = td.tridiagRPKW(nodes, weights)

    k = np.arange(len(beta))+1

    lbl = quadRule.name if ls == '--' else None

    plt.figure('beta')
    plt.plot(k, beta, ls+sty,
             label=lbl,
             c=col, markevery=0.1)

    relErr = abs(beta-betaTh[:N])/betaTh[:N]

    plt.figure('error-beta')
    plt.semilogy(k, relErr, ls+sty,
                 label=lbl,
                 c=col, markevery=0.1)


for N, ls in lN:
    for quadRule, col, sty in lQuadRule:
        computeAndPlot()

plt.figure('beta')
plt.plot(betaTh, '-', c='gray', label='Analytical')
plt.grid(color='gray')
plt.ylabel('$\\beta_k$')
plt.xlabel('$k$')
plt.legend()
# plt.savefig('beta_legendre.pdf', bbox_inches='tight')

plt.figure('error-beta')
# plt.title('Relative error for $\\beta_k$')
plt.grid(color='gray')
plt.ylabel('$e_{rel,\\beta_k}$')
plt.xlabel('$k$')
plt.legend()
# plt.savefig('err_beta_legendre.pdf', bbox_inches='tight')

plt.show()
