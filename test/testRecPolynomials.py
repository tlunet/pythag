#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 15:23:56 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
import pythag.quadrature.classicgauss as qr
from pythag import FactorPoly

quadRule = qr.ChebyshevRule()

t = np.linspace(-1, 1, 5000)

nOrthnPoly = 50
plt.figure('evalOrthnPoly')
for n in range(nOrthnPoly):
    val = quadRule.rec.evalOrthnPoly(t, n)
    plt.plot(t, quadRule.rec.evalOrthnPoly(t, n))
yLim = plt.ylim()

plt.figure('getOrthnPoly')
for n in range(nOrthnPoly):
    p = quadRule.rec.getOrthnPoly(n)
    plt.plot(t, p(t))
plt.ylim(yLim)

plt.figure('monicPoly')
p = quadRule.rec.getMonicPoly(45)
plt.plot(t, p(t), label='evalPoly')
plt.xlim(0.8, 1)
plt.legend()

val = quadRule.rec.evalMonicPoly(t, 45)
plt.plot(t, val, label='evalRec')
plt.legend()

roots, _ = quadRule.getNodesAndWeights(45)
r = FactorPoly(roots)
plt.plot(t, r(t), '--', label='evalRoot')
plt.legend()
