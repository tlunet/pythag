#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 17:06:19 2018

@author: lunet
"""
import sys
import numpy as np
import pythag.quadrature.classic as qc
import matplotlib.pyplot as plt
import scipy.integrate as si


def unifNodes(n):
    return np.linspace(-1, 1, num=n)


def cosNodes(n):
    return np.cos(np.arange(n)*np.pi/(n-1))[-1::-1]


gamma = 1.2


def tanNodes(n):
    eta = np.linspace(-1, 1, num=n+2)[1:-1]
    return np.tan(gamma*eta)/np.tan(gamma)


g = 0.3
e = 2.9


def gaussLaw(n):
    xi = np.linspace(-1, 1, num=n)

    def gauss(x):
        return np.exp(-abs(x)**e/g)

    def gaussInt(xi):
        return si.quad(gauss, 0, xi)[0]

    vec_gaussInt = np.vectorize(gaussInt)
    return (vec_gaussInt(xi)/gaussInt(1.)).tolist()


lQuadRule = [('FEJER-1', qc.FejerRule(kind=1)),
             ('FEJER-2', qc.FejerRule(kind=2)),
             ('CLENSHAW-CURTIS', qc.ClenshawCurtisRule()),
             ('LAG-COS', qc.LagrangeRule(cosNodes))]

if len(sys.argv) > 1:
    nNodes = int(sys.argv[1])
else:
    nNodes = 10

for name, qr in lQuadRule:
    print(name)
    nodes, weights = qr.getNodesAndWeights(nNodes)
    # print 'nodes:', nodes
    # print 'weights:', weights
    print('weights sum =', weights.sum())
    plt.plot(nodes, weights, 'o', label=name)


plt.legend()
plt.show()
