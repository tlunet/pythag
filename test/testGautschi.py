#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 19:57:18 2018

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt
import pythag.recurrence.base as pr


def weightFunc(x):
    return 1 + (x < 0)

b = 1
support = [-b, b]
rec = pr.ThreeTermRec('TEST', support=support, weightFunc=weightFunc)

n = 20
rec.ORTHPOL_PRECISION = 1e-6
rec.buildAlphaBeta(n)
alpha, beta = rec.getAlphaBeta(n)

if False:
    for k, a, b in zip(range(len(alpha)), alpha, beta):
        print('k={}, alpha={:1.3e}, beta={:1.3e}'.format(k, a, b))


plt.figure('alpha')
plt.plot(alpha, label='Support: {}'.format(support))
plt.legend()

plt.figure('beta')
plt.plot(beta, label='Support: {}'.format(support))
plt.legend()

x = np.linspace(*support, num=200)

plt.figure('poly {}'.format(support))
for i in range(5):
    plt.plot(x, rec.evalNormzPoly(x, i+1, xNorm=b), label='P{}'.format(i+1))
plt.legend()

plt.figure('measure')
plt.plot(x, weightFunc(x))
