#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 16:06:40 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
from pythag import FactorPoly
import pythag.quadrature.classicgauss as pqg

quadRule = pqg.JacobiRule(0.25, 0.5)

n = 1000
alpha, beta = quadRule.getAlphaBeta(n)

r1 = quadRule.getNodesAndWeights(n-1)[0]
p1 = FactorPoly(r1.tolist()+[alpha[-1]])

r2 = quadRule.getNodesAndWeights(n-2)[0]
p2 = FactorPoly(r2)


def newtonCorr(t):
    v1, d1 = p1(t, dVal=True)
    v2, d2 = p2(t, dVal=True)
    return (v1 - beta[-1]*v2)/(d1 - beta[-1]*d2)


def newtonCorr2(t):
    h = 1e-8
    v1 = p1(t)
    v2 = p2(t)
    d1 = (p1(t+h)-v1)/h
    d2 = (p2(t+h)-v2)/h
    return (v1 - beta[-1]*v2)/(d1 - beta[-1]*d2)


r3_ref = quadRule.getNodesAndWeights(n)[0]
nIterNewton = 7
r3 = quadRule.getAsymptoticApproximation(n)[0]
err = [np.linalg.norm(r3_ref-r3)]
res = 1e99
eps = np.finfo(float).eps
while res > eps:
    corr = newtonCorr2(r3)
    res = np.linalg.norm(corr, np.inf)
    r3 -= corr
    err.append(np.linalg.norm(r3_ref-r3))
plt.semilogy(err)
