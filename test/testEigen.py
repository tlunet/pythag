#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 21:04:38 2018

@author: lunet
"""
import numpy as np
from time import time
import pandas as pd

from pythag.algebra.eigen import computeQuadRule
from pythag.algebra.tridiag import tridiagRPKW

eps = np.finfo(float).eps

algos = {
    'GOLUB_WELSCH_FORTRAN': 'Golub & Welsch algorithm (FORTRAN)',
    'GOLUB_WELSCH_PYTHON': 'Golub & Welsch algorithm (PYTHON)',
    'EIG_SCIPY': 'DSTEMR LAPACK driver with Scipy (FORTRAN)',
    }

res = pd.DataFrame(index=pd.MultiIndex.from_product(
    [('tComp', 'err'), algos.values()]))

for N in [2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000]:
    alpha = np.zeros(N)
    k = np.arange(N, dtype=float)+1
    beta = k**2/(4*k**2-1)
    beta = np.ones(N)*1./4
    beta[0], beta[1] = np.pi, 1./2

    res[N] = 0
    for name, descr in algos.items():
        tBeg = time()
        nodes, weights = computeQuadRule(alpha, beta, implementation=name)
        tEnd = time()
        res.loc[('tComp', descr), N] = tEnd-tBeg

        alpha2, beta2 = tridiagRPKW(nodes, weights)
        errBeta = np.log10(abs(beta - beta2)/abs(beta)+eps)
        res.loc[('err', descr), N] = max(errBeta)

print(res)
