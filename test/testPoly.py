# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 15:41:18 2018

@author: lunet
"""
from pythag.polynomials.base import Polynom, OrthogPoly, FactorPoly


p = Polynom([3, 2, 1, 0])
print(p)

q = Polynom('x^2 + 1 - x + 5.9x^5')
print(q)

r = Polynom(list(range(1, 50)))

q = OrthogPoly([1, 2], [3, 4], normal=True)
print(q(1))
q.normal = False
print(q(1))

s = FactorPoly([1, 1, 1.1, 2, 2.0, 3])
s.coeff = 2.35
print(s.rootsMult)
print(s)
