#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  7 20:38:55 2018

@author: lunet
"""
import numpy as np
from time import time
from pythag.algebra.eigen import computeQuadRule


print('    N - tCompGWF: - tCompEIL: - ratio:')

for N in [5, 10, 20, 50, 100, 200, 500, 1000, 2000]:

    # Computing Legendre recurrence coefficients
    alpha = np.zeros(N)
    k = np.arange(N, dtype=float)+1
    beta = k**2/(4*k**2-1)

    # Golub & Welsch algorithm (FORTRAN)
    tBeg = time()
    nodes, weights = computeQuadRule(alpha, beta,
                                     implementation='GOLUB_WELSCH_FORTRAN')
    tEnd = time()
    tCompGWF = tEnd - tBeg

    # Eig algorithm (LINPACK)
    tBeg = time()
    nodes, weights = computeQuadRule(alpha, beta,
                                     implementation='EIG_SCIPY')
    tEnd = time()
    tCompEIL = tEnd - tBeg

    print('{:5d} - {:f}s - {:f}s - {:1.2f}'.format(
            N, tCompGWF, tCompEIL, tCompEIL/tCompGWF))
