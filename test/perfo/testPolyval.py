#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 17:07:05 2019

@author: lunet
"""
import numpy as np
from pythag import Polynom, FactorPoly, OrthogPoly
from time import time

xVal = np.ones(1000)*0.5
nTest = 20

print('Testing Polynom object')
for method in ['NUMPY', 'FORTRAN']:
    print(' -- using {} method for eval'.format(method))
    Polynom.POLYVAL_METHOD = method
    for deg in [10, 100, 1000, 10000, 100000]:
        p = Polynom([1.]*deg)
        tComp = 0
        for i in range(nTest):
            tBeg = time()
            p(xVal)
            tEnd = time()
            tComp += tEnd-tBeg
        tComp /= nTest
        print('    deg={:06d}, tEval={:3.2g}'.format(deg, tComp))

print('Testing OrthogPoly object')
for method in ['PYTHON', 'FORTRAN']:
    print(' -- using {} method for evalMonic'.format(method))
    OrthogPoly.EVALMONIC_METHOD = method
    for deg in [10, 100, 1000, 10000, 100000]:
        p = OrthogPoly(None, None, deg=deg)
        tComp = 0
        for i in range(nTest):
            tBeg = time()
            p(xVal)
            tEnd = time()
            tComp += tEnd-tBeg
        tComp /= nTest
        print('    deg={:06d}, tEval={:3.2g}'.format(deg, tComp))
for method in ['PYTHON', 'FORTRAN']:
    print(' -- using {} method for evalOrthn'.format(method))
    OrthogPoly.EVALORTHN_METHOD = method
    for deg in [10, 100, 1000, 10000, 100000]:
        p = OrthogPoly(None, None, normal=True, deg=deg)
        tComp = 0
        for i in range(nTest):
            tBeg = time()
            p(xVal)
            tEnd = time()
            tComp += tEnd-tBeg
        tComp /= nTest
        print('    deg={:06d}, tEval={:3.2g}'.format(deg, tComp))

print('Testing FactorPoly object')
for method in ['NUMPY', 'FORTRAN']:
    print(' -- using {} method for eval'.format(method))
    FactorPoly.EVAL_METHOD = method
    for deg in [10, 100, 1000, 10000, 100000]:
        p = FactorPoly([1.]*deg)
        tComp = 0
        for i in range(nTest):
            tBeg = time()
            p(xVal)
            tEnd = time()
            tComp += tEnd-tBeg
        tComp /= nTest
        print('    deg={:06d}, tEval={:3.2g}'.format(deg, tComp))
for method in ['PYTHON', 'FORTRAN']:
    print(' -- using {} method for evalWithDeriv'.format(method))
    FactorPoly.EVAL_WITH_DERIV_METHOD = method
    for deg in [10, 100, 1000, 10000, 100000]:
        p = FactorPoly([1.]*deg)
        tComp = 0
        for i in range(nTest):
            tBeg = time()
            p(xVal, dVal=True)
            tEnd = time()
            tComp += tEnd-tBeg
        tComp /= nTest
        print('    deg={:06d}, tEval={:3.2g}'.format(deg, tComp))
