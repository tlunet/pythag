#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 14:27:50 2022

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt
# from scipy.special import roots_legendre

from pythag.applications.lagrange import LagrangeApproximation

def getLastPlotCol():
    return plt.gca().get_lines()[-1].get_color()

nMax = 14
nVals = np.arange(3, nMax+1)

def testImplementation(method):

    tBeg = -1
    tEnd = 1
    qType = 'GAUSS'
    numQuad_Q = 'LEGENDRE_NUMPY'

    errors = np.ones((4, nMax-2))
    errWeights = errors[:2]
    errQuad = errors[2:]

    for l, n in enumerate(nVals):

        if qType == 'LOBATTO':
            nodes = np.linspace(tBeg, tEnd, num=n)
        elif qType == 'GAUSS':
            nodes = np.linspace(tBeg, tEnd, num=n+2)[1:-1]

        # nodes = roots_legendre(n)[0]

        approx = LagrangeApproximation(
            nodes, weightComputation=method, useAnalytic=False)

        weights = approx.getIntegrationMatrix(
            [[tBeg, tEnd]], numQuad=numQuad_Q)

        QMatrix = approx.getIntegrationMatrix(
            [[tBeg, tau] for tau in approx.points], numQuad=numQuad_Q)


        def testWeights(weights, nodes, nExp=2000):
            deg = 2*np.size(weights)-1

            err = np.zeros(nExp)
            for i in range(nExp):
                poly_coeff = np.random.rand(deg+1)
                poly_vals  = np.polyval(poly_coeff, nodes)
                poly_int_coeff = np.polyint(poly_coeff)
                int_ex = np.polyval(poly_int_coeff, tEnd) \
                    - np.polyval(poly_int_coeff, tBeg)
                int_coll = np.sum(weights * poly_vals)
                err[i] = abs(int_ex-int_coll)

            return err


        def testQMatrix(QMatrix, nodes, nExp=2000):
            n = np.size(weights)

            err = np.zeros((nExp, n))
            for i in range(nExp):
                poly_coeff = np.random.rand(n-1)
                poly_vals  = np.polyval(poly_coeff, nodes)
                poly_int_coeff = np.polyint(poly_coeff)
                for j in range(n):
                    int_ex = np.polyval(poly_int_coeff, nodes[j]) \
                        - np.polyval(poly_int_coeff, tBeg)
                    int_coll = np.sum(poly_vals * QMatrix[j,:])
                    err[i, j] = abs(int_ex-int_coll)

            return err

        err = testWeights(weights, nodes)
        errWeights[0, l] = np.mean(err)
        errWeights[1, l] = np.max(err)

        err = testQMatrix(QMatrix, nodes)
        errQuad[0, l] = np.mean(err)
        errQuad[1, l] = np.max(err)

    return errors


plt.figure()
for method in ['FAST', 'CHEBFUN', 'STABLE']:
    # method += '_FORTRAN'
    errs = testImplementation(method)
    plt.semilogy(nVals, errs[2], label=method)
    plt.semilogy(nVals, errs[3], '--', c=getLastPlotCol())
plt.legend()
plt.ylim(1e-17, 1e-12)
