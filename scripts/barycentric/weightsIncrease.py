#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 13:19:25 2021

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt

from pythag.applications.lagrange import LagrangeApproximation


lN = [10, 20, 30, 40, 50, 100, 200, 300, 400, 500, 1000, 2000, 5000]
pTypes = ['EQUID']
style = ['-s', '-o']

weights = []
for pType in pTypes:
    weights.append([])
    for nPoints in lN:
        la = LagrangeApproximation((pType, nPoints), useAnalytic=False,
                                   weightComputation='STABLE_FORTRAN')
        weights[-1].append(max(np.abs(la.weights)))

plt.figure('weightsIncrease')
for i, pType in enumerate(pTypes):
    plt.loglog(lN, weights[i], style[i], label=pType)
plt.legend()
plt.grid(True)
plt.xlabel('Number of nodes')
plt.ylabel('Max. absolute value of the weights')
plt.tight_layout()

plt.show()
