#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 09:14:14 2021

@author: telu
"""
import numpy as np
from time import time
import matplotlib.pyplot as plt

from pythag.applications import LagrangeApproximation


nRun = 20

implementations = ['FAST_FORTRAN', 'CHEBFUN_FORTRAN', 'STABLE_FORTRAN']
lSym = ['-o', '-s', '--o', '--s']
lN = [5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000]


def generatePoints(n):
    j = np.arange(n)[-1::-1]
    return np.cos(j*np.pi/(n-1))


lTimes = []
for n in lN:
    points = generatePoints(n)
    lTimes.append([])

    for impl in implementations:

        tComp = 0
        for i in range(nRun):
            if impl.startswith('FAST') and n > 500:
                tComp = -1
            else:
                tBeg = time()
                LagrangeApproximation(
                    points, weightComputation=impl, scaleRef='MAX')
                tComp += time()-tBeg
        tComp /= nRun

        lTimes[-1].append(tComp)

lTimes = np.array(lTimes).T

# %% Plot
plt.figure('tComp barycentric weights')
for i, impl in enumerate(implementations):
    plt.loglog(lN, lTimes[i], lSym[i], label=impl)
plt.legend()
plt.grid()
plt.xlabel('$n$')
plt.ylabel('Computation time')
plt.tight_layout()
