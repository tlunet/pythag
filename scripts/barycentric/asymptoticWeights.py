#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 23:12:42 2021

@author: telu
"""
import matplotlib.pyplot as plt

from pythag.applications.lagrange import LagrangeApproximation, DICO_GQR

nQuad = 10000

quadRule = 'LEGENDRE'

for nQuad in [10, 20, 50, 100, 10000]:
    la = LagrangeApproximation(
        (quadRule, nQuad), useAnalytic=False, weightComputation='STABLE')
    qr = DICO_GQR[quadRule]
    plt.plot(la.points, la.weights, '.')

x = la.points
# plt.plot(la.points, (1-x**2)**0.5, '--')  # 1/\sqrt{1-t^2}
# plt.plot(la.points, (1-x**2), '--')  # \sqrt{1-t^2}
# plt.plot(la.points, (1-x**2)**0.75, '--')  # 1
plt.plot(la.points,
         qr.weightFunc(x)**0.5*(1-x**2)**0.75, '--', c='white')
plt.grid(True)