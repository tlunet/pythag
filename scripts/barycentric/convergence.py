#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 14:36:44 2021

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt

from pythag.applications import LagrangeApproximation


def fun(t):
    # return 1/(1+32*t**4)
    return np.abs(t) + t/2 - t**2 + 0.5


def gaussWeight(t):
    return np.abs(t) + t/2 - t**2 + 0.5
    res = np.copy(t)
    res[t < 0] = (1-(2*t[t < 0] + 1)**2)**0.5
    res[t > 0] = (1-(2*t[t > 0] - 1)**2)**0.5
    return res


tEval = np.linspace(-1, 1, num=50000)
fEval = fun(tEval)


lN = np.array([10, 20, 50, 100, 200, 500, 1000, 2000])+1
pTypes = ['CHEBY-I', 'CHEBY-II', 'ASYMPT']
style = ['-s', '-o', '-^', '-p']


err = []
for pType in pTypes:
    err.append([])
    for n in lN:
        la = LagrangeApproximation((pType, n), useAnalytic=True,
                                   weightComputation='STABLE_FORTRAN',
                                   gaussWeight=gaussWeight)
        f = fun(la.points)
        fInterpol = la.getInterpolationMatrix(tEval).dot(f)
        err[-1].append(np.linalg.norm(fEval - fInterpol, np.inf))

plt.figure('convergence')
for i, pType in enumerate(pTypes):
    plt.loglog(lN, err[i], style[i])
plt.legend(pTypes)
# plt.legend(['Cheby. 1st kind', 'Cheby. 2nd kind', 'Legendre'])
plt.grid(True)
plt.xlabel('Number of nodes')
plt.ylabel('Max. absolute error')
plt.tight_layout()

plt.show()
