#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 20:02:02 2021

@author: telu
"""
import matplotlib.pyplot as plt

from pythag.applications.lagrange import LagrangeApproximation

nQuad = 10000
pTypes = ['CHEBY-I', 'CHEBY-II', 'LEGENDRE']


args = dict(useAnalytic=False, weightComputation='STABLE')

leg = LagrangeApproximation(('LEGENDRE', nQuad), **args)

che1 = LagrangeApproximation(('CHEBY-I', nQuad), **args)
che2 = LagrangeApproximation(('CHEBY-II', nQuad), **args)

cheG1 = LagrangeApproximation(('CHEBY-1', nQuad), **args)
cheG2 = LagrangeApproximation(('CHEBY-2', nQuad), **args)
cheG3 = LagrangeApproximation(('CHEBY-3', nQuad), **args)
cheG4 = LagrangeApproximation(('CHEBY-4', nQuad), **args)


def setup():
    plt.legend()
    plt.grid(True)
    plt.xlabel('Node coordinate')
    plt.ylabel('Log. scaled weight')
    plt.tight_layout()


plt.figure('fig1')
plt.plot(che1.points, che1.weights, '.', label='Cheby. 1st kind')
plt.plot(che2.points, che2.weights, '.', label='Cheby. 2nd kind')
plt.plot(leg.points, leg.weights, '.', label='Legendre')
setup()

plt.figure('fig2')
plt.plot(cheG1.points, cheG1.weights, '.', label='Gauss-Cheby. 1st kind')
plt.plot(cheG2.points, cheG2.weights, '.', label='Gauss-Cheby. 2nd kind')
plt.plot(leg.points, leg.weights, '.', label='Legendre')
setup()

plt.figure('fig3')
plt.plot(cheG3.points, cheG3.weights, '.', label='Gauss-Cheby. 3rd kind')
plt.plot(cheG4.points, cheG4.weights, '.', label='Gauss-Cheby. 4th kind')
plt.plot(leg.points, leg.weights, '.', label='Legendre')
setup()
