#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 10:51:27 2018

@author: telu
"""
import pythag.analytic.givens as pa
from pythag.algebra.tridiag import tridiagRPKW
from pythag.algebra.eigen import computeQuadRule
from time import time
import sympy as sy
import numpy as np

# sy.init_printing()

h = sy.Symbol('h')
c = sy.Symbol('c')

N = 3
A = sy.zeros(N+1)
A[0, 0] = 1
for i in range(1, N+1):
    A[0, i] = sy.sqrt(sy.Symbol('\\omega_{'+str(i)+'}'))
    A[i, 0] = sy.sqrt(sy.Symbol('\\omega_{'+str(i)+'}'))
    A[i, i] = sy.Symbol('\\tau_{'+str(i)+'}')  # -1 + (2*i-1)*h/2


P = sy.Matrix([[1, 1, 1, 1],
               [-1, 0, 0, 0],
               [0, -1, 0, 0],
               [0, 0, -1, 0]])
# Q, alpha, beta, J = pa.tridiagGivens(A)
# beta[0] = beta[0].simplify()

#N = 1000
#for N in [10, 20, 50, 100, 200, 500, 1000, 2000, 5000]:
#    alpha = np.zeros(N)
#    beta = np.ones(N)*1./4
#    beta[0], beta[1] = np.pi, 1./2
#
#    tBeg = time()
#    nodes, weights = computeQuadRule(alpha, beta)
#    tEnd = time()
#    print(' -- N={}, tComp[GoWe]={:f}s'.format(N, tEnd-tBeg))
#
#    tBeg = time()
#    alpha2, beta2 = tridiagRPKW(nodes, weights)
#    tEnd = time()
#    print(' -- N={}, tComp[RPKW]={:f}s'.format(N, tEnd-tBeg))

#alpha = [sy.Symbol('\\alpha_'+str(i)) for i in range(N+1)]
#beta = [sy.Symbol('\\beta_'+str(i)) for i in range(N)]
#J = sy.zeros(N+1)
#J[0, 0] = 1
#for i in range(1, N+1):
#    J[i, i] = alpha[i-1]
#    J[i-1, i] = sy.sqrt(beta[i-1])
#    J[i, i-1] = sy.sqrt(beta[i-1])
#
#R1 = pa.givens(4, alpha[1], sy.sqrt(beta[2]), 2, 3)
