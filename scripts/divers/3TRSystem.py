#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  9 09:26:04 2018

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
import pythag.algebra.tridiag as pat
import pythag.quadrature.classic as pqc

quadRule = pqc.FejerRule()

n = 10
nodes, weights = quadRule.getNodesAndWeights(n)
weights *= (1-nodes)**(-0.5)*(1+nodes)**(-0.5)

alpha, beta = pat.tridiagRPKW(nodes, weights)


def genL(nodes, alpha, beta):
    n = len(nodes)
    L = np.eye(n**2)
    b = np.zeros(n**2)
    for i in range(n):
        L[n*i:n*(i+1), n*i:n*(i+1)] += np.diag(alpha[1:]-nodes[i], k=-1)
        L[n*i:n*(i+1), n*i:n*(i+1)] += np.diag(beta[2:], k=-2)
        b[n*i:n*i+2] = [alpha[0]-nodes[i], beta[0]]
    return L, b


L, b = genL(nodes, alpha, beta)

pF = np.linalg.solve(L, -b)
pC = np.array(pF).reshape((n, n), order='F').ravel()

print(' -- eqAlpha -- ')
for i in range(1, n):
    eqAlpha = np.sum(weights*(nodes - alpha[i])*pC[n*(i-1):n*i]**2)
    print(eqAlpha)
print(' -- eqBeta -- ')
for i in range(1, n-1):
    eqBeta = -beta[i]*np.sum(weights*pC[n*(i-1):n*i]**2) + \
        np.sum(weights*pC[n*i:n*(i+1)]**2)
    print(eqBeta)

plt.figure('P1')
plt.plot(nodes, pC[0:n], 'o-', label='P1')
plt.figure('P2')
plt.plot(nodes, 2*pC[n:2*n], 'o-', label='P2')
plt.figure('P3')
plt.plot(nodes, 4*pC[2*n:3*n], 'o-', label='P3')
plt.figure('P4')
plt.plot(nodes, 8*pC[3*n:4*n], 'o-', label='P4')
plt.grid(color='gray')
plt.legend()
plt.show()
