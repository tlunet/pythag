#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 20:15:54 2018

@author: lunet
"""
import sympy as sy
sy.init_printing(forecolor='CornflowerBlue')

a, b, x, t = sy.symbols('a,b,x,t')

w = sy.Function('w')((t-a)*(b-a)/(x-a) + a)

p = sy.Function('p')(t)

I1 = sy.integrate(p*w, (t, a, x))

dI = sy.diff(I1, x)
dI = dI.factor().simplify().factor().factor()
