#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 16 16:48:32 2018

@author: lunet
"""
import sympy as sy
sy.init_printing(forecolor='White')

order = 2
iDeriv = 0
dXformulation = False
dXunif = False
useLagrange = True


n = order+1
x = sy.Matrix([sy.Symbol('x_{'+str(i)+'}') for i in range(n)])
u = sy.Matrix([sy.Symbol('u_{'+str(i)+'}') for i in range(n)])
xD = x[iDeriv]

if not useLagrange:
    V = sy.Matrix([[x[j]**(i) for i in range(n)] for j in range(n)])
    c = V.LUsolve(u)
    c.simplify()
    deriv = sum([c[i+1]*xD**i*(i+1) for i in range(order)])
else:
    c = []
    for j in range(n):
        den = 0
        for k in range(n):
            prod = 1
            if k != j:
                for l_ in range(n):
                    if l_ != j and l_ != k:
                        prod = prod*(xD - x[l_])
                den = den+prod
        num = 1
        for k in range(n):
            if k != j:
                num = num*(x[j]-x[k])
        a = den/num
        a.simplify()
        c.append(a)
    deriv = sum([u[i]*c[i] for i in range(n)])

deriv = deriv.simplify()
deriv = deriv.factor()

if dXformulation:

    dx = sy.Matrix([sy.Symbol('\\Delta x_{'+str(i+1)+'}')
                    for i in range(order)])

    for i in range(n):
        deriv = deriv.subs(x[i], sum([dx[j] for j in range(i)]))

    deriv = deriv.simplify()
    deriv = deriv.factor()

    if dXunif:

        dxU = sy.Symbol('\\Delta x')
        for i in range(order):
            deriv = deriv.subs(dx[i], dxU)

        deriv = deriv.simplify()
        deriv = deriv.factor()
