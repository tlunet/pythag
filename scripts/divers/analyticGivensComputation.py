#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 11:45:45 2018

@author: lunet
"""
import sympy as sy
import pythag.analytic.givens as pag
sy.init_printing(forecolor='CornflowerBlue')

kwargs = {'symbolType': 'LATEX',
          'quadRule': 'GENERAL'}

A2 = pag.arrowMatrix(2, **kwargs)
Q2, alpha2, beta2, J2 = pag.tridiagGivens(A2, stage=-1, simplify=False)

A3 = pag.arrowMatrix(3, **kwargs)
Q3, alpha3, beta3, J3 = pag.tridiagGivens(A3, stage=4, simplify=False)

A4 = pag.arrowMatrix(4, **kwargs)
Q4, alpha4, beta4, J4 = pag.tridiagGivens(A4, stage=4, simplify=False)

R4 = pag.givens(5, J4[1, 0], J4[4, 0], 1, 4)
J4 = R4*J4*R4.T
Q4 = R4*Q4
J4.simplify()

QL2, alphaL2, betaL2 = pag.tridiagLanczos(A2, stage='ALL')
