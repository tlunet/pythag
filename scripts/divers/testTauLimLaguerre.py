#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 16:37:36 2018

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt

N = 100
for N in [2, 3, 4, 5, 6, 7, 8, 9, 10]:
    i = np.arange(N) + 1

    xi = 2*i/(N+1)-1
    yi = i/(N+1-i)
    si = i/(N+1)

    ci = np.cos(i*np.pi/(N+1))
    taui = (1-ci)/(1+ci)

    plt.plot(si, taui)
