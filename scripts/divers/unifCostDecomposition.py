#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  4 17:51:47 2019

@author: lunet
"""
import matplotlib.pyplot as plt

nQuad = 10000
nCoeff = 51
nTS = 10


def costStieljes(j, nQuad):
    if j == 0:
        return 9*nQuad
    elif j == 1:
        return 19*nQuad-1
    else:
        return 6*nQuad*(2*j+1)-1


cTot = sum([costStieljes(j, nQuad) for j in range(nCoeff)])

cTS = cTot//nTS

lIndexes = []
j = 0
c = 0
while j < nCoeff:
    c += costStieljes(j, nQuad)
    if c > (len(lIndexes)+1)*cTS:
        lIndexes.append(j)
    j += 1
if len(lIndexes) < nTS:
    lIndexes.append(nCoeff-1)

# %%
lIndexes = [16, 22, 27, 31, 35, 38, 41, 44, 47, 50]

lIndexesStart = [0] + lIndexes[:-1]
lDeltaJ = [lEnd - lBeg for lBeg, lEnd in zip(lIndexesStart, lIndexes)]

print('lDeltaJ :', lDeltaJ)

lCost = [sum([costStieljes(j, nQuad) for j in range(jStart, jEnd)])
         for jStart, jEnd in zip(lIndexesStart, lIndexes)]
cMean = sum(lCost)/len(lCost)
cDiff = max(lCost)-min(lCost)


plt.bar(range(nTS), height=lCost)
plt.hlines(cMean, -0.5, nTS-0.5, linestyles='--')
x = 0
for jBeg, jEnd in zip(lIndexesStart, lIndexes):
    c = 0
    for j in range(jBeg, jEnd):
        c += costStieljes(j, nQuad)
        plt.hlines(c, x-0.4, x+0.39, linewidth=1.2)
    x += 1
plt.ylabel('Computational cost in FLOPs')
plt.xlabel('Index of the parallel subinterval')
plt.xticks(range(nTS))
plt.savefig('optimalDecomp.pdf', bbox_inches='tight')
print('cDiff :', cDiff)
