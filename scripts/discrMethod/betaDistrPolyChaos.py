#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 18 15:11:28 2020

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt

from pythag.recurrence.measures import wF
import pythag.quadrature.classicgauss as pqg
import pythag.algebra.tridiag as td

c1, c2 = 0.1, 10
a, b = -1, 1
eta = 20
rho, phi, n = 10, 0, -10

N, ac = 2, 5


def pdfBase(t):
    return (t < 0)*(1-np.sqrt(abs(1-(t+1)**2))) + \
        (t >= 0)*(1-np.sqrt(abs(1-(t-1)**2)))
    return 5/2*(t+1)*(t < -1+2/5) + \
        5/2*(t-3/5)*(t > 3/5) + 1*(t >= -1+2/5) + \
        5/2*(t+1/5)*(t > -1/5)*(t < 0) + \
        5/2*(t-1/5)*(t < 1/5)*(t >= 0)
    return (1-t)**c1 * (1+t)**c2
    return np.exp(-t**2)*np.sinh(ac*t/2)**2
    return np.exp(-eta/t)
    return np.cos(rho*np.sin(t+phi)-n*t)



def measure(t):
    return pdfBase((b-a)/2*t + (b+a)/2)


nCoeff = 50

nQuadVals = np.array([1, 2, 4, 8, 16, 32, 64])*nCoeff
nQuadVals = np.array(nQuadVals, dtype=int)
nQuadRef = 100*nQuadVals.max()


quadRule = pqg.GaussQuadRule(wF(measure, 'betaPC'), support=[-1, 1])
quadRule.weightFunc.plot()

# Reference solution
nodesRef, weightsRef = quadRule.getDiscreteApproximation(nQuadRef)
alphaRef, betaRef = td.tridiagStieltjes(nodesRef, weightsRef, nCoeffMax=nCoeff)

# Computation for different values of nQuad
for nQuad in nQuadVals:
    nodes, weights = quadRule.getDiscreteApproximation(nQuad)
    alpha, beta = td.tridiagStieltjes(nodes, weights, nCoeffMax=nCoeff)
    errAlpha, errBeta = abs(alpha-alphaRef), abs(beta-betaRef)
    plt.figure('errAlpha')
    plt.semilogy(errAlpha, label=f'nQuad={nQuad}')
    plt.figure('errBeta')
    plt.semilogy(errBeta, label=f'nQuad={nQuad}')
    
for figName in ['errAlpha', 'errBeta']:
    plt.figure(figName)
    plt.legend()
    plt.ylabel('Absolute error with reference')
    plt.xlabel('$k$')
    


