#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 19:19:29 2018

@author: lunet
"""
import numpy as np
import pythag.algebra.tridiag as td
import pythag.quadrature.classic as quad
import pythag.recurrence.classic as rec
import matplotlib.pyplot as plt
from time import time

pltFunc = plt.plot

lN = [10, 20, 40, 100, 240]
# lN = np.arange(100)+2
num = 6
# lN = np.logspace(3, num+2, num=num, base=2., dtype=int)
maxN = max(lN)
lSymb = ['<', 's', '>', 'o', '^', '*']*10

k = np.arange(maxN, dtype=float)

wFunc = 'LEGENDRE'
support = [-1, 1]
plotAnalytical = True
if wFunc == 'LEGENDRE':
    recTh = rec.LegendreRec()
elif wFunc.startswith('CHEBY'):
    recTh = rec.ChebyshevRec(kind=int(wFunc[-1]))
elif wFunc.startswith('JACOBI'):
    a = float(wFunc.split('_')[1])
    b = float(wFunc.split('_')[2])
    recTh = rec.JacobieRec(a, b)
else:
    plotAnalytical = False
    recTh = rec.LegendreRec()
alphaTh, betaTh = recTh.getAlphaBeta(maxN)


if wFunc.startswith('LOG') or wFunc.startswith('RAD'):
    coeff = float(wFunc[4:])
    support = [0, 1]

kwargs = {'support': support}

discr = 'F1'
if discr == 'NC':
    quadRule = quad.NewtonCotesRule(**kwargs)
elif discr in ['F1', 'F2']:
    quadRule = quad.FejerRule(kind=int(discr[-1]), **kwargs)
elif discr == 'CC':
    quadRule = quad.ClenshawCurtisRule(**kwargs)

figName = '-'+discr+'-'+wFunc
saveFig = False
plotBeta = True
plotAlphaErr = False

for N, sym in zip(lN, lSymb):

    tBeg = time()
    if discr == 'AA':
        i = np.arange(N)+1
        s = i/(N+1)
        nodes = -np.cos(s*np.pi)
        weights = np.pi*np.sqrt(1-nodes**2)
        weights /= N
    else:
        nodes, weights = quadRule.getNodesAndWeights(N)
    quadRuleTime = time() - tBeg
    print(' -- N={}, quadRuleTime={}'.format(N, quadRuleTime))

    if wFunc == 'CHEBY1':
        weights *= (1-nodes**2)**(-0.5)
    elif wFunc == 'CHEBY2':
        weights *= (1-nodes**2)**(0.5)
    elif wFunc == 'CHEBY3':
        weights *= (1-nodes)**(-0.5)*(1+nodes)**(0.5)
    elif wFunc == 'CHEBY4':
        weights *= (1-nodes)**(0.5)*(1+nodes)**(-0.5)
    elif wFunc.startswith('LOG'):
        weights *= nodes**(coeff)*np.log(1./nodes)
    elif wFunc.startswith('RAD'):
        weights *= np.exp(-coeff/nodes)
    elif wFunc == 'EXP2':
        weights *= np.exp(-nodes**2)
    elif wFunc == 'STEP':
        weights[N//2:] *= 2
    elif wFunc == 'LIN':
        weights *= nodes
    elif wFunc.startswith('JACOBI'):
        weights *= recTh.weightFunc(nodes)

    tBeg = time()
    alpha, beta = td.tridiagStieltjes(nodes, weights)
    print
    compTime = time() - tBeg
    print(' -- N={}, compTime={}'.format(N, compTime))

    if plotBeta:
        plt.figure('beta'+figName)
        plt.plot(k[1:N], beta[1:], sym+'-', label='$N={}'.format(N)+'$',
                 markevery=0.1)

    plt.figure('error-beta'+figName)
    relErr = abs(beta-betaTh[:N])/betaTh[:N]
    plt.semilogy(k[:N], relErr, sym+'-', label='$N={}'.format(N)+'$',
                 markevery=0.1)
    if discr == 'NC' and False:
        plt.semilogy(k[:N], (k[:N]/N)**2, sym+'--', markevery=0.1)

    if plotAlphaErr:
        plt.figure('error-alpha'+figName)
        relErr = abs(alpha-alphaTh[:N])/alphaTh[:N]
        plt.semilogy(k[:N], relErr, sym+'-', label='$N={}'.format(N)+'$',
                     markevery=0.1)

if plotBeta:
    plt.figure('beta'+figName)
    if plotAnalytical:
        plt.plot(k[1:N], betaTh[1:N], '--', c='black', label='Analytical')
    plt.xlabel('$k$')
    plt.ylabel('$\\beta_k$')
    plt.grid()
    plt.legend(loc='upper right')
    if saveFig:
        plt.savefig('beta.pdf', bbox_inches='tight')

plt.figure('error-beta'+figName)
# plt.xlim(xmin=0)
# plt.ylim(1e-8, 10)
plt.xlabel('$j$')
plt.ylabel('$e_{rel,\\beta_j}$')
plt.grid()
# plt.title('Relative error for $\\beta_k$')
plt.xscale('log', base=2)
xTicks = [1, 2, 4] + [N for N in lN]
plt.xticks(xTicks, np.array(xTicks, dtype=str))
plt.legend(loc='upper right')
if saveFig:
    plt.savefig('error_beta.pdf', bbox_inches='tight')

if plotAlphaErr:
    plt.figure('error-alpha'+figName)
    plt.xlabel('$j$')
    plt.ylabel('$e_{rel,\\alpha_j}$')
    plt.grid()
    plt.xscale('log', base=2)
    xTicks = [1, 2, 4] + [N for N in lN]
    plt.xticks(xTicks, np.array(xTicks, dtype=str))
    plt.legend(loc='upper right')
    if saveFig:
        plt.savefig('error_beta.pdf', bbox_inches='tight')

plt.show()
