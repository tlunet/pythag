#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 13:48:32 2019

@author: telu
"""
from time import time
import matplotlib.pyplot as plt
import pythag.quadrature.classicgauss as pqg
import pythag.algebra.tridiag as td

nCoeff = 50
lNQuad = [50*10**i for i in range(6)]
discrMethod = 'F1'
lStyle = '-'
lSym = ['o', '^', 's', '>', '*', '^']
lCol = plt.rcParams['axes.prop_cycle'].by_key()['color']


quadRule = pqg.JacobiRule(1/4, 1/2)
alpha, beta = quadRule.getAlphaBeta(nCoeff)

if discrMethod == 'AA':
    discrRule = quadRule.getAsymptoticApproximation
    discRuleArgs = {}
elif discrMethod in ['F1', 'F2', 'CC', 'CHEBY1']:
    discrRule = quadRule.getDiscreteApproximation
    discRuleArgs = {'kind': discrMethod}
else:
    raise ValueError('Wrong choice for discrMethod : {}'.format(discrMethod))


nodesWeights = [discrRule(nQuad, **discRuleArgs) for nQuad in lNQuad]
alphaBetaD = []
for nodes, weights in nodesWeights:
    tBeg = time()
    alphaBetaD.append(td.tridiagStieltjes(nodes, weights, nCoeffMax=nCoeff))
    tEnd = time()
    print(f'nQuad={len(nodes)}, tComp={tEnd-tBeg}')

# %%
plt.figure('beta-nCoeff{}'.format(nCoeff, discrMethod))
for nQuad, [alphaD, betaD], sym, col in zip(lNQuad, alphaBetaD, lSym, lCol):
    plt.semilogy(abs(beta - betaD[:nCoeff]), sym+lStyle,
                 label='$N_{q}='+'{}$'.format(int(nQuad)),
                 markevery=0.1, c=col)
if discrMethod == 'F1':
    plt.legend(loc=9)
    plt.grid()
plt.xlabel('$j$')
# plt.ylim(1e-14, 100)
plt.ylabel('Absolute error in $\\beta_j^{N_q}$')
plt.savefig('jacobi_DM-error_beta.pdf', bbox_inches='tight')

# %%
plt.figure('alpha-nCoeff{}'.format(nCoeff, discrMethod))
for nQuad, [alphaD, betaD], sym, col in zip(lNQuad, alphaBetaD, lSym, lCol):
    plt.semilogy(abs(alpha - alphaD[:nCoeff]), sym+lStyle,
                 label='$N_{q}='+'{}$'.format(int(nQuad)),
                 markevery=0.1, c=col)
if discrMethod == 'F1':
    plt.legend()
    plt.grid()
plt.xlabel('$j$')
# plt.ylim(1e-14, 100)
plt.ylabel('Absolute error in $\\alpha_j^{N_q}$')
plt.savefig('jacobi_DM-error_alpha.pdf', bbox_inches='tight')

# %%
if False:
    plt.figure('betaRes-nCoeff{}-{}'.format(nCoeff, discrMethod))
    for nQuad, nQuadTild, [alpha_k, beta_k], [alpha_kp1, beta_kp1] in zip(
            lNQuad, lNQuad[1:], alphaBetaD, alphaBetaD[1:]):
        plt.semilogy(abs(beta_k[:nCoeff] - beta_kp1[:nCoeff]),
                     label='$N_{q}='+'{} ({})$'.format(
                        int(nQuad), int(nQuadTild)))
    plt.legend()
    plt.xlabel('$j$')
    plt.grid()
    plt.ylim(1e-14, 1)
