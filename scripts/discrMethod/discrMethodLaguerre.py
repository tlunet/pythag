#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 19:41:33 2018

@author: lunet
"""
import numpy as np
import pythag.algebra.tridiag as td
import pythag.quadrature.classicgauss as quad
import pythag.recurrence.classic as rec
import matplotlib.pyplot as plt
from time import time

pltFunc = plt.plot

lN = [10, 20, 50]
maxN = max(lN)
lSymb = ['<', 's', '>', 'o', '^', '*']*10

k = np.arange(maxN, dtype=float)

# Measure
measure = 'LAGUERRE'
aL = 4
dicoMeasure = {'LAGUERRE': lambda x: x**aL * np.exp(-x)}
if measure not in dicoMeasure.keys():
    raise ValueError('Wrong measure choice')
weightFunc = dicoMeasure[measure]
recTh = rec.GenLaguerre(aL)
alphaTh, betaTh = recTh.getAlphaBeta(maxN)


# Compute quadrature rule
if measure == 'LAGUERRE':
    quadRule = quad.LaguerreRule(a=aL)
nodesAndWeights = [quadRule.getNodesAndWeights(n_) for n_ in lN]

wFunc = 'LAGUERRE'+str(aL)
recTh = rec.GenLaguerre(aL)
alphaTh, betaTh = recTh.getAlphaBeta(maxN)

if wFunc == 'LEGENDRE':
    recTh = rec.LegendreRec()
elif wFunc == 'CHEBY1':
    recTh = rec.ChebyshevRec(kind=int(wFunc[-1]))
else:
    plotAnalytical = False
    recTh = rec.LegendreRec()
betaTh = recTh.getAlphaBeta(maxN)[1]
