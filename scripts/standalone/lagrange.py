#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 14:36:56 2022

@author: telu
"""
import numpy as np


class LagrangeApproximation(object):

    def __init__(self, points, weightComputation='STABLE', scaleRef='MAX'):

        points = np.asarray(points).ravel()

        diffs = points[:, None] - points[None, :]
        diffs[np.diag_indices_from(diffs)] = 1

        def noLogScale(diffs):
            # Fast implementation (unstable for large number of points)
            invProd = np.prod(diffs, axis=1)
            invProd **= -1
            return invProd

        def logScale(diffs):
            # Stable implementation (more expensive)
            sign = np.sign(diffs).prod(axis=1)
            wLog = -np.log(np.abs(diffs)).sum(axis=1)
            if scaleRef == 'ZERO':
                wScale = wLog[np.argmin(np.abs(points))]
            elif scaleRef == 'MAX':
                wScale = wLog.max()
            else:
                raise NotImplementedError(f'scaleRef={scaleRef}')
            invProd = np.exp(wLog-wScale)
            invProd *= sign
            return invProd

        def chebfun(diffs):
            # Implementation used in chebfun
            diffs *= 4 / (points.max() - points.min())
            sign = np.sign(diffs).prod(axis=1)
            vv = np.exp(np.log(np.abs(diffs)).sum(axis=1))
            invProd = 1./(sign*vv)
            invProd /= np.linalg.norm(invProd, np.inf)
            return invProd

        if weightComputation == 'AUTO':
            with np.errstate(divide='raise', over='ignore'):
                try:
                    invProd = noLogScale(diffs)
                except FloatingPointError:
                    invProd = logScale(diffs)
        elif weightComputation == 'FAST':
            invProd = noLogScale(diffs)
        elif weightComputation == 'STABLE':
            invProd = logScale(diffs)
        elif weightComputation == 'CHEBFUN':
            invProd = chebfun(diffs)
        else:
            raise NotImplementedError(
                f'weightComputation={weightComputation}')
        weights = invProd

        # Store attributes
        self.points = points
        self.weights = weights
        self.weightComputation = weightComputation

    @property
    def n(self):
        return self.points.size

    def getInterpolationMatrix(self, times):

        # Compute difference between times and Lagrange points
        times = np.asarray(times)
        with np.errstate(divide='ignore'):
            iDiff = 1/(times[:, None] - self.points[None, :])

        # Find evaluated positions that coincide with one Lagrange point
        concom = (iDiff == np.inf) | (iDiff == -np.inf)
        i, j = np.where(concom)

        # Replace iDiff by on on those lines to get a simple copy of the value
        iDiff[i, :] = concom[i, :]

        # Compute interpolation matrix using weights
        PInter = iDiff * self.weights
        PInter /= PInter.sum(axis=-1)[:, None]

        return PInter

    def getIntegrationMatrix(self, intervals):

        # Legendre gauss rule to integrate exactly polynomials of deg. (n-1)
        iNodes, iWeights = np.polynomial.legendre.leggauss(self.n//2+1)

        # Compute quadrature nodes for each interval
        intervals = np.array(intervals)
        aj, bj = intervals[:, 0][:, None], intervals[:, 1][:, None]
        tau, omega = iNodes[None, :], iWeights[None, :]
        tEval = (bj-aj)/2*tau + (bj+aj)/2

        # Compute the integrand function on nodes
        integrand = self.getInterpolationMatrix(tEval.ravel()).T.reshape(
            (-1,) + tEval.shape)

        # Apply quadrature rule to integrate
        integrand *= omega
        integrand *= (bj-aj)/2
        PInter = integrand.sum(axis=-1).T

        return PInter


if __name__ == '__main__':

    import matplotlib.pyplot as plt

    points = np.polynomial.legendre.leggauss(15)[0]
    la = LagrangeApproximation(points)

    def iFun(t):
        return np.abs(t)*t/2 + t**2/4 - t**3/3

    def fun(t):
        return np.abs(t) + t/2 - t**2

    f = fun(la.points)

    tEval = np.linspace(-1, 1, num=100)
    dt = tEval[1]-tEval[0]
    fEval = la.getInterpolationMatrix(tEval).dot(f)

    plt.figure('Interpolation')
    plt.plot(tEval, fun(tEval))
    plt.plot(la.points, f, 'o')
    plt.plot(tEval, fEval)

    PInteg = la.getIntegrationMatrix([(0, t) for t in tEval])
    ifEval = PInteg.dot(f)

    plt.figure('Integration')
    plt.plot(tEval, iFun(tEval))
    plt.plot(la.points, iFun(la.points), 'o')
    plt.plot(tEval, ifEval)
    plt.show()