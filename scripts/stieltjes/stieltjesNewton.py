#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 11:00:45 2018

@author: telu
"""
import numpy as np
import matplotlib.pyplot as plt
import pythag.quadrature.classicgauss as quad
import pythag.recurrence.classic as rec
import pythag.algebra.tridiag as pat


n_quad = 1000  # Number of quadrature points
n_coeff = 1000
K = 2  # Number of Newton iterations

qr = 'JACOBI_0.5_-0.1'
nw = 'EXACT'
ir = 'JACOBI_0.5_-0.1'
air = 0

plotAlpha = False
plotBeta = True
plotMeasure = False

# Wanted quadrature rule
if qr == 'LEG':
    quadRule = quad.GaussLegendreRule()
elif qr.startswith('CHEBY'):
    kind = int(qr[-1])
    quadRule = quad.GaussChebyshevRule(kind)
elif qr.startswith('JACOBI'):
    a = float(qr.split('_')[1])
    b = float(qr.split('_')[2])
    quadRule = quad.GaussJacobiRule(a, b)
else:
    raise NotImplementedError('qr={} not implemented'.format(qr))

alpha_th, beta_th = quadRule.rec.getAlphaBeta(n_coeff)

# Build nodes and weights
if nw == 'EXACT':
    nodes, weights = quadRule.getNodesAndWeights(n_quad)
elif nw == 'AA':
    nodes, weights = quadRule.getAsymptoticApproximation(n_quad)
elif nw in ['NC', 'F1', 'F2', 'CC']:
    nodes, weights = quadRule.getDiscreteApproximation(n_quad, kind=nw)
W = weights
T = nodes
TW = nodes*weights

# Initial values for alpha, beta
if ir == 'LEG':
    rec_init = rec.LegendreRec()
    alpha_init, beta_init = rec_init.getAlphaBeta(n_coeff)
elif ir.startswith('CHEBY'):
    kind = int(ir[-1])
    rec_init = rec.ChebyshevRec(kind)
    alpha_init, beta_init = rec_init.getAlphaBeta(n_coeff)
elif ir.startswith('JACOBI'):
    a = float(qr.split('_')[1])
    b = float(qr.split('_')[2])
    rec_init = rec.JacobieRec(a, b)
    alpha_init, beta_init = rec_init.getAlphaBeta(n_coeff)
elif ir == 'AA':
    # Asymptotic approximation for first coefficients
    n, w = quadRule.getAsymptoticApproximation(n_coeff)
    alpha_init, beta_init = pat.tridiagRPKW(n, w)
else:
    raise NotImplementedError('ir={} not implemented'.format(ir))
if air > 0:
    alpha_init[-air:] = 0
    beta_init[-air:] = 0.25


# Application of Stieltjes-Newton
alpha, beta = np.zeros((2, K+1, n_coeff))
alpha[0, :], beta[0, :] = alpha_init, beta_init
beta[:, 0] = np.sum(W)
alpha[:, 0] = np.sum(TW)/np.sum(W)

pi = np.zeros((K+1, n_coeff, n_quad))
# -- computation of pi for k=0 using 3-terms-recurrence
pi[:, 0, :] = 1.
pi[0, 1, :] = T-alpha[0, 0]
pi[0, 2, :] = (T-alpha[0, 1])*pi[0, 1, :] - beta[0, 1]
for i in range(3, n_coeff):
    pi[0, i, :] = (T-alpha[0, i-1])*pi[0, i-1, :] - beta[0, i-1]*pi[0, i-2, :]


# Iterations for k = 1, ..., K
for k in range(K):
    print('Iteration {}/{}'.format(k+1, K))

    # Stage for i = 1, ..., n-1
    for i in range(1, n_coeff):

        # -- pi update
        if i == 1:
            pi[k+1, 1, :] = T-alpha[k, 0]
        elif i == 2:
            pi[k+1, 2, :] = -(alpha[k, 1]-T)*pi[k+1, 1, :]
            pi[k+1, 2, :] -= pi[k+1, 1, :]*(alpha[k+1, 1]-alpha[k, 1])
            pi[k+1, 2, :] -= beta[k+1, 1]
        else:
            pi[k+1, i, :] = -beta[k, i-1]*pi[k+1, i-2, :]
            pi[k+1, i, :] -= (alpha[k, i-1]-T)*pi[k+1, i-1, :]
            pi[k+1, i, :] -= (alpha[k+1, i-1]-alpha[k, i-1])*pi[k, i-1, :]
            pi[k+1, i, :] -= (beta[k+1, i-1]-beta[k, i-1])*pi[k, i-2, :]

        # -- alpha update
        alpha[k+1, i] = -2*np.dot(pi[k, i, :],
                                  (alpha[k, i]*W-TW)*pi[k+1, i, :])
        alpha[k+1, i] += 2*alpha[k, i]*np.dot(pi[k, i, :], W*pi[k, i, :])
        alpha[k+1, i] -= np.dot(pi[k, i, :], TW*pi[k, i, :])
        alpha[k+1, i] /= np.dot(pi[k, i, :], W*pi[k, i, :])

        # -- beta update
        beta[k+1, i] = -2*beta[k, i]*np.dot(pi[k, i-1, :],
                                            W*(pi[k+1, i-1, :]-pi[k, i-1, :]))
        beta[k+1, i] += 2*np.dot(pi[k, i, :], W*pi[k+1, i, :])
        beta[k+1, i] -= np.dot(pi[k, i, :], W*pi[k, i, :])
        beta[k+1, i] /= np.dot(pi[k, i-1, :], W*pi[k, i-1, :])

if n_quad <= 1000:
    # Stieltjes
    alpha_stieltjes, beta_stieltjes = pat.tridiagStieltjes(nodes, weights)

# Plot beta
if plotBeta:
    plt.figure('beta')
    plt.plot(beta_th, 'o', c='gray', label='Exact')
    if n_quad <= 1000:
        plt.plot(beta_stieltjes[:n_coeff], 's-', c='black', label='Stieltjes')
    for k in range(0, K+1, 1):
        plt.plot(beta[k, :], label='k={}'.format(k))
    plt.legend()
    plt.xlabel('j')

# Plot alpha
if plotAlpha:
    plt.figure('alpha')
    plt.plot(alpha_th, 'o', c='gray', label='Exact')
    if n_quad <= 1000:
        plt.plot(alpha_stieltjes[:n_coeff], 's-', c='black', label='Stieltjes')
    for k in range(0, K+1, 1):
        plt.plot(alpha[k, :], label='k={}'.format(k))
    plt.legend()
    plt.xlabel('j')

# Plot error
plt.figure('error')
errBeta = [np.linalg.norm(beta[k, :]-beta_th) for k in range(K+1)]
errAlpha = [np.linalg.norm(alpha[k, :]-alpha_th) for k in range(K+1)]
plt.semilogy(range(K+1), errBeta,
             # label='$N_{coeff}='+'{}$'.format(n_coeff)
             label='$Asymp. coeff. ='+'{}$'.format(air))
# plt.semilogy(range(K+1), errAlpha, label='alpha')
plt.legend()
plt.xlabel('Newton iterations')

# Plot measure
if plotMeasure:
    plt.figure('measure')
    t = np.linspace(-1, 1, num=1000)
    plt.plot(t, quadRule.rec.weightFunc(t))
