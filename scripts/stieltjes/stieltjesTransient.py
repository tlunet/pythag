#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 16:26:19 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
import pythag.quadrature.classicgauss as quad
import pythag.algebra.tridiag as pat

n_quad = 1001  # Number of quadrature points
n_coeff = 600

qr = 'JACOBI_0.5_1.5'
nw = 'F1'

# Wanted quadrature rule
if qr == 'LEG':
    quadRule = quad.LegendreRule()
elif qr.startswith('CHEBY'):
    kind = int(qr[-1])
    quadRule = quad.ChebyshevRule(kind)
elif qr.startswith('JACOBI'):
    a = float(qr.split('_')[1])
    b = float(qr.split('_')[2])
    quadRule = quad.JacobiRule(a, b)
else:
    raise NotImplementedError('qr={} not implemented'.format(qr))

alpha_th, beta_th = quadRule.getAlphaBeta(n_coeff+1)

# Build nodes and weights
if nw == 'EXACT':
    nodes, weights = quadRule.getNodesAndWeights(n_quad)
elif nw == 'AA':
    nodes, weights = quadRule.getAsymptoticApproximation(n_quad)
elif nw in ['NC', 'F1', 'F2', 'CC']:
    nodes, weights = quadRule.getDiscreteApproximation(n_quad, kind=nw)
else:
    raise NotImplementedError('nw={} not implemented'.format(nw))


def stieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, nodes, weights):
    pi_jp1 = np.empty_like(pi_j)
    pi_jp1[:] = (nodes-alpha_j)*pi_j - beta_j*pi_jm1
    p_temp = pi_jp1**2
    p_temp *= weights
    print(pi_jm1.max()/pi_j.max())
    alpha_jp1 = sum(nodes*p_temp)/sum(p_temp)
    beta_jp1 = sum(p_temp)/sum(weights*(pi_j**2))
    return alpha_jp1, beta_jp1, pi_jp1, pi_j.copy()


# Initial state
alpha = np.ones(n_coeff+1, dtype=float)
beta = np.ones(n_coeff+1, dtype=float)
beta[0] = sum(weights)
alpha[0] = sum(nodes*weights)/beta[0]

pi_jm1 = np.zeros(n_quad, dtype=float)
pi_j = np.ones(n_quad, dtype=float)

for j in range(n_coeff):
    alpha[j+1], beta[j+1], pi_j, pi_jm1 = stieljesAdvance(
        alpha[j], beta[j], pi_j, pi_jm1, nodes, weights)

if False:
    alpha, beta = pat.tridiagStieltjes(nodes, weights)

# Plots
plt.figure('beta')
plt.plot(beta_th, 'o', c='gray', label='Exact')
plt.plot(beta[:n_coeff+1], label='Stieltjes')
plt.ylim(0, 2)
plt.figure('alpha')
plt.plot(alpha_th, 'o', c='gray', label='Exact')
plt.plot(alpha[:n_coeff+1], label='Stieltjes')
