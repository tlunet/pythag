#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 16:28:44 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
from pythag.polynomials.interpol import HighOrderStructGridInterpolator1D
from pythag.algebra.tridiag import tridiagRPKW
import pythag.quadrature.classicgauss as pqg

# Quadrature rule to compute, and algorithm settings
qr = 1
if qr == 1:
    quadRule = pqg.JacobiRule(0.25, 0.5)
elif qr == 2:
    quadRule = pqg.GaussQuadRule(
        lambda x: 5/2*(x+1)*(x < -1+2/5) +
        5/2*(x-3/5)*(x > 3/5) + 1*(x >= -1+2/5) +
        5/2*(x+1/5)*(x > -1/5)*(x < 0) +
        5/2*(x-1/5)*(x < 1/5)*(x >= 0),
        name='HARD', support=[-1, 1])
nAdvPerTS = 1
nTS = 20
nCoeff = 1 + nTS*nAdvPerTS
nIter = 20
iOrder_GtoF = 11
iOrder_FtoG = 11
includeBndPoints = False
useOrthnPoly = False
useAA_F = True
useAA_G = False
plotBeta = False

# Coarse and fine grid settings
nQuad_G = 2*nCoeff
nQuad_F = 5000

# Figure suffix
suffix = 'polyMG{}-{}_QR{}_{}_k{}({},{})_G{}({})_F{}({})'.format(
    iOrder_GtoF, iOrder_FtoG, qr,
    'Orthn' if useOrthnPoly else 'Monic',
    nIter, nTS, nAdvPerTS,
    nQuad_G, 'AA' if useAA_G else 'F1',
    nQuad_F, 'AA' if useAA_F else 'F1')

# Coarse and fine quadrature rules
if useAA_G:
    nodes_G, weights_G = quadRule.getAsymptoticApproximation(nQuad_G)
else:
    nodes_G, weights_G = quadRule.getDiscreteApproximation(nQuad_G)
if useAA_F:
    nodes_F, weights_F = quadRule.getAsymptoticApproximation(nQuad_F)
else:
    nodes_F, weights_F = quadRule.getDiscreteApproximation(nQuad_F)

# Arrays for recurrence coefficients
alpha = np.zeros((nIter+1, nTS+1))
beta = np.zeros_like(alpha)

# Initial recurrence coefficient for each iterations
beta[:, 0] = np.sum(weights_F)
alpha[:, 0] = np.sum(nodes_F*weights_F)/beta[0, 0]


# Time stepping function
def stieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, nodes, weights):
    if useOrthnPoly:
        p_temp = np.empty_like(pi_j)
        p_temp[:] = pi_j
        p_temp **= 2
        p_temp *= nodes**2
        p_temp *= weights
        beta_jp1 = np.sum(p_temp) - alpha_j**2
        if beta_j != beta[0, 0]:
            beta_jp1 -= beta_j
        pi_jp1 = np.empty_like(pi_j)
        pi_jp1[:] = pi_j
        pi_jp1 *= (nodes-alpha_j)
        pi_jp1 -= beta_j**0.5 * pi_jm1
        pi_jp1 /= beta_jp1**0.5
        p_temp[:] = pi_jp1
        p_temp **= 2
        p_temp *= nodes
        p_temp *= weights
        alpha_jp1 = np.sum(p_temp)
    else:
        pi_jp1 = np.empty_like(pi_j)
        pi_jp1[:] = (nodes-alpha_j)*pi_j - beta_j*pi_jm1
        if includeBndPoints:
            p_temp = pi_jp1[1:-1]**2
            nodes = nodes[1:-1]
        else:
            p_temp = pi_jp1**2
        p_temp *= weights
        alpha_jp1 = sum(nodes*p_temp)/sum(p_temp)
        if includeBndPoints:
            beta_jp1 = sum(p_temp)/sum(weights*(pi_j[1:-1]**2))
        else:
            beta_jp1 = sum(p_temp)/sum(weights*(pi_j**2))
    return alpha_jp1, beta_jp1, pi_jp1, pi_j.copy()


def multiStieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, nodes, weights, nAdv):
    for n in range(nAdv):
        alpha_j, beta_j, pi_j, pi_jm1 = \
            stieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, nodes, weights)
    return alpha_j, beta_j, pi_j, pi_jm1.copy()


# Arrays for orthogonal polynomial values
if includeBndPoints:
    pi_j = np.zeros((nIter+1, nTS+1, nQuad_F+2))
    pi_jm1 = np.zeros_like(pi_j)
else:
    pi_j = np.zeros((nIter+1, nTS+1, nQuad_F))
    pi_jm1 = np.zeros_like(pi_j)
pi_j[:, 0, :] = 1
if useOrthnPoly:
    pi_j[:, 0, :] /= beta[0, 0]**0.5
pi_jm1[:, 0, :] = 0

# Interpolation operators
HighOrderStructGridInterpolator1D.VERBOSE = False
if includeBndPoints:
    grid_G = np.hstack([[-1], nodes_G, [1]])
    grid_F = np.hstack([[-1], nodes_F, [1]])
else:
    grid_G, grid_F = nodes_G, nodes_F
hosgi_GtoF = HighOrderStructGridInterpolator1D(
    iOrder_GtoF, grid_G, grid_F, boundary='WALL')
hosgi_FtoG = HighOrderStructGridInterpolator1D(
    iOrder_GtoF, grid_F, grid_G, boundary='WALL')
gToF = hosgi_GtoF.interpolate
fToG = hosgi_FtoG.interpolate

# Parareal initialization : k=0
print(' -- Parareal initialization')
if False:
    # -- using global givens rotations
    alpha_G, beta_G = tridiagRPKW(nodes_G, weights_G)
    alpha[0, 1:] = alpha_G[nAdvPerTS:nCoeff:nAdvPerTS]
    beta[0, 1:] = beta_G[nAdvPerTS:nCoeff:nAdvPerTS]
    # TODO: correction
    pi_jm1[0, 1, :] = pi_j[0, 0, :]
    pi_j[0, 1, :] = gToF(grid_G-alpha[0, 0])
    for j in range(nCoeff-2):
        pi_jm1[0, j+2, :] = pi_j[0, j+1, :]
        pi_j[0, j+2, :] = gToF((grid_G-alpha[0, j+1])*fToG(pi_j[0, j+1, :])
                               - beta[0, j+1]*fToG(pi_j[0, j, :]))
else:
    # -- using coarse Stieltjes time-stepping operator
    for j in range(nTS):
        alpha_G, beta_G, pi_j_G, pi_jm1_G = multiStieljesAdvance(
            alpha[0, j], beta[0, j],
            fToG(pi_j[0, j, :]), fToG(pi_jm1[0, j, :]),
            grid_G, weights_G, nAdvPerTS)
        alpha[0, j+1] = alpha_G
        beta[0, j+1] = beta_G
        pi_j[0, j+1, :] = gToF(pi_j_G)
        pi_jm1[0, j+1, :] = gToF(pi_jm1_G)


# Parareal iterations
for k in range(nIter):
    print(' -- Parareal iteration {}/{}'.format(k+1, nIter))
    for j in range(nTS):
        # Subinterval solves
        alpha_F, beta_F, pi_j_F, pi_jm1_F = multiStieljesAdvance(
            alpha[k, j], beta[k, j], pi_j[k, j, :], pi_jm1[k, j, :],
            grid_F, weights_F, nAdvPerTS)
        alpha_Gk1, beta_Gk1, pi_j_Gk1, pi_jm1_Gk1 = multiStieljesAdvance(
            alpha[k+1, j], beta[k+1, j],
            fToG(pi_j[k+1, j, :]), fToG(pi_jm1[k+1, j, :]),
            grid_G, weights_G, nAdvPerTS)
        alpha_Gk, beta_Gk, pi_j_Gk, pi_jm1_Gk = multiStieljesAdvance(
            alpha[k, j], beta[k, j],
            fToG(pi_j[k, j, :]), fToG(pi_jm1[k, j, :]),
            grid_G, weights_G, nAdvPerTS)
        # Parareal update formula
        alpha[k+1, j+1] = alpha_F + alpha_Gk1 - alpha_Gk
        beta[k+1, j+1] = beta_F + beta_Gk1 - beta_Gk
        pi_j[k+1, j+1, :] = pi_j_F + gToF(pi_j_Gk1) - gToF(pi_j_Gk)
        pi_jm1[k+1, j+1, :] = pi_jm1_F + gToF(pi_jm1_Gk1) - gToF(pi_jm1_Gk)


# Fine solution
if False:
    alpha_F, beta_F = tridiagRPKW(nodes_F, weights_F)
    j = np.arange(0, nCoeff, nAdvPerTS)
    alpha_F, beta_F = alpha_F[j], beta_F[j]
else:
    alpha_F, beta_F = np.zeros((2, nTS+1))
    beta_F[0] = beta[0, 0]
    alpha_F[0] = alpha[0, 0]
    # -- using fine Stieltjes time-stepping operator
    pi_j_F = pi_j[0, 0, :]
    pi_jm1_F = pi_jm1[0, 0, :]
    for j in range(nTS):
        alpha_F[j+1], beta_F[j+1], pi_j_F, pi_jm1_F = multiStieljesAdvance(
            alpha_F[j], beta_F[j], pi_j_F, pi_jm1_F,
            grid_F, weights_F, nAdvPerTS)

j = np.arange(0, nCoeff, nAdvPerTS)

# %% Beta plots
if plotBeta:
    plt.figure('beta-{}'.format(suffix))
    plt.plot(j, beta_F, '--', c='gray', label='Fine solution')
    for k in range(nIter+1):
        plt.plot(j, beta[k, :], label='k={}'.format(k))
    plt.legend()

# %% Error beta plots
plt.figure('error-beta-{}'.format(suffix))
for k in range(nIter+1):
    plt.semilogy(j, abs(beta[k, :]-beta_F)/abs(beta_F),
                 label='k={}'.format(k))
plt.legend()
plt.grid()

# %% Polynomials plots
if False:
    plt.figure('orthogPoly-{}'.format(suffix))
    degree = 25
    plt.title('Monic polynomial order {}'.format(degree))
    # plt.plot(grid_F, quadRule.rec.evalMonicPoly(grid_F, degree))
    for k in range(nIter+1):
        plt.plot(grid_F, pi_j[k, degree, :], label='k={}'.format(k))
    plt.legend()
