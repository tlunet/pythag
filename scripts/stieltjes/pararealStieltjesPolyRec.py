#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 15:32:14 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
from pythag import OrthogPoly, mDict
from pythag.algebra.tridiag import tridiagRPKW
import pythag.quadrature.classicgauss as pqg

# Quadrature rule to compute, and algorithm settings
qr = 2
if qr == 1:
    quadRule = pqg.JacobiRule(0.25, 0.5)
elif qr == 2:
    name = 'ELECTRO'
    quadRule = pqg.GaussQuadRule(mDict[name], support=[-1, 1], name=name)
elif qr == 3:
    name = 'FUNNEL'
    quadRule = pqg.GaussQuadRule(mDict[name], support=[-1, 1], name=name)
elif qr == 4:
    name = 'RADIATIVE'
    c = 2.5
    quadRule = pqg.GaussQuadRule(mDict[name](c), support=[0, 1], name=name)

unifDecomp = True
if unifDecomp:
    nAdvPerTS = 5
    nTS = 100
    nCoeff = 1 + nTS*nAdvPerTS
    lJBeg = [nAdvPerTS*j for j in range(nTS)]
else:
    nTS = 10
    nCoeff = 51
    nAdvPerTS = 'OPT'
    lJBeg = [0, 16, 22, 27, 31, 35, 38, 41, 44, 47]
lJEnd = lJBeg[1:] + [nCoeff-1]
lDeltaJ = [jEnd-jBeg for jBeg, jEnd in zip(lJBeg, lJEnd)]

nIter = 6
useOrthnPoly = False
useAA_F = True
useAA_G = False
plotBeta = False
lSym = ['o', '^', 's', '>', '*', '>', 'p']
lCol = plt.rcParams['axes.prop_cycle'].by_key()['color']

# Coarse and fine grid settings
nQuad_G = int(nCoeff*2)
nQuad_F = 50000

iSym = 0 if nQuad_G == 102 else 1 if nQuad_G == 51 else 2
iCol = iSym
iStyle = (':' if nTS == 50 else '--') if unifDecomp else '-.'
iLabel = None if iStyle != ':' else '$N_{q,\\mathcal{G}}='+str(nQuad_G)+'$'

# Figure suffix
suffix = 'polyRec_QR{}_{}_k{}({},{})_G{}({})_F{}({})'.format(
    qr,
    'Orthn' if useOrthnPoly else 'Monic',
    nIter, nTS, nAdvPerTS,
    nQuad_G, 'AA' if useAA_G else 'F1',
    nQuad_F, 'AA' if useAA_F else 'F1')

# Arrays for recurrence coefficients
alpha = np.zeros((nIter+1, nTS+1))
beta = np.zeros_like(alpha)

# Coarse and fine quadrature rules
if useAA_G:
    nodes_G, weights_G = quadRule.getAsymptoticApproximation(nQuad_G)
else:
    nodes_G, weights_G = quadRule.getDiscreteApproximation(nQuad_G)
if useAA_F:
    nodes_F, weights_F = quadRule.getAsymptoticApproximation(nQuad_F)
else:
    nodes_F, weights_F = quadRule.getDiscreteApproximation(nQuad_F)

# Initial recurrence coefficient for each iterations
beta[:, 0] = np.sum(weights_F)
alpha[:, 0] = np.sum(nodes_F*weights_F)/beta[0, 0]

# Arrays for orthogonal polynomial values
pi_j = np.array(
    [[OrthogPoly(None, None, useOrthnPoly, deg=j)
      for j in [0]+lJEnd]
     for k in range(nIter+1)])
pi_jm1 = np.array(
    [[OrthogPoly(None, None, useOrthnPoly, deg=j-1)
      for j in [0]+lJEnd]
     for k in range(nIter+1)])
# -- initialization
if useOrthnPoly:
    for k in range(nIter+1):
        pi_j[k, 0].beta[0] = beta[0, 0]
        pi_j[k, 0].alpha[0] = alpha[0, 0]


# Scalar product functions
def getScalarProductFunc(level='FINE'):

    if level == 'FINE':
        nodes, weights = nodes_F, weights_F
    elif level == 'COARSE':
        nodes, weights = nodes_G, weights_G
    else:
        raise ValueError('Cannot return scalarProduct for level={}'
                         .format(level))

    def scalarProduct(pi_jp1, pi_j):
        pi_jp1 = pi_jp1(nodes)
        pi_jp1 **= 2
        pi_jp1 *= weights
        s2 = np.sum(pi_jp1)
        pi_jp1 *= nodes
        s1 = np.sum(pi_jp1)
        pi_j = pi_j(nodes)
        pi_j **= 2
        pi_j *= weights
        s0 = np.sum(pi_j)
        # print(s1, s2, s0)
        alpha, beta = s1/s2, s2/s0
        return alpha, beta

    def scalarProduct2(pi, stage):
        if stage == 1:
            pi_j = pi(nodes)
            pi_j **= 2
            pi_j *= nodes**2
            pi_j *= weights
            s1 = max([0, np.sum(pi_j)])
            return s1
        elif stage == 2:
            pi_jp1 = pi(nodes)
            pi_jp1 **= 2
            pi_jp1 *= nodes
            pi_jp1 *= weights
            s2 = max([0, np.sum(pi_jp1)])
            return s2

    if useOrthnPoly:
        return scalarProduct2
    else:
        return scalarProduct


scalarProductG = getScalarProductFunc('COARSE')
scalarProductF = getScalarProductFunc('FINE')


# Time stepping function
def stieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, scalarProducts):
    if useOrthnPoly:
        beta_jp1 = scalarProducts(pi_j, 1) - alpha_j**2
        if pi_j.deg > 0:
            beta_jp1 -= beta_j
        pi_jp1 = OrthogPoly(
            pi_j.alpha.tolist() + [0], pi_j.beta.tolist() + [beta_jp1],
            normal=True)
        alpha_jp1 = scalarProducts(pi_jp1, 2)
        pi_jp1.alpha[-1] = alpha_jp1
    else:
        pi_jp1 = OrthogPoly(
            pi_j.alpha.tolist() + [alpha_j], pi_j.beta.tolist() + [beta_j])
        alpha_jp1, beta_jp1 = scalarProducts(pi_jp1, pi_j)
    return alpha_jp1, beta_jp1, pi_jp1, pi_j.copy()


def multiStieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, scalarProducts, nAdv):
    for n in range(nAdv):
        alpha_j, beta_j, pi_j, pi_jm1 = \
            stieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, scalarProducts)
    return alpha_j, beta_j, pi_j, pi_jm1.copy()


# Parareal initialization : k=0
print(' -- Parareal initialization')
if False:
    # -- using global givens rotations
    alpha_G, beta_G = tridiagRPKW(nodes_G, weights_G)
    alpha[0, 1:], beta[0, 1:] = alpha_G[1:nCoeff], beta_G[1:nCoeff]
    for j in range(nCoeff-1):
        pi_jm1[0, j+1].recCoeffs[:] = pi_j[0, j].recCoeffs
        pi_j[0, j+1].alpha[:] = alpha[0, :j+1+useOrthnPoly]
        pi_j[0, j+1].beta[:] = beta[0, :j+1+useOrthnPoly]
else:
    # -- using coarse Stieltjes time-stepping operator
    for j, deltaJ in zip(range(nTS), lDeltaJ):
        alpha_G, beta_G, pi_j_G, pi_jm1_G = multiStieljesAdvance(
            alpha[0, j], beta[0, j], pi_j[0, j], pi_jm1[0, j],
            scalarProductG, deltaJ)
        alpha[0, j+1] = alpha_G
        beta[0, j+1] = beta_G
        pi_j[0, j+1].recCoeffs[:] = pi_j_G.recCoeffs
        pi_jm1[0, j+1].recCoeffs[:] = pi_jm1_G.recCoeffs


# Parareal iterations
for k in range(nIter):
    print(' -- Parareal iteration {}/{}'.format(k+1, nIter))
    for j, deltaJ in zip(range(nTS), lDeltaJ):
        # Subinterval solves
        alpha_F, beta_F, pi_j_F, pi_jm1_F = multiStieljesAdvance(
            alpha[k, j], beta[k, j], pi_j[k, j], pi_jm1[k, j],
            scalarProductF, deltaJ)
        alpha_Gk1, beta_Gk1, pi_j_Gk1, pi_jm1_Gk1 = multiStieljesAdvance(
            alpha[k+1, j], beta[k+1, j], pi_j[k+1, j], pi_jm1[k+1, j],
            scalarProductG, deltaJ)
        alpha_Gk, beta_Gk, pi_j_Gk, pi_jm1_Gk = multiStieljesAdvance(
            alpha[k, j], beta[k, j], pi_j[k, j], pi_jm1[k, j],
            scalarProductG, deltaJ)
        # Parareal update formula
        # - alpha, beta
        alpha[k+1, j+1] = alpha_F + alpha_Gk1 - alpha_Gk
        beta[k+1, j+1] = beta_F + beta_Gk1 - beta_Gk
        # - pi_j
        pi_j[k+1, j+1].recCoeffs[:] = pi_j_F.recCoeffs
        pi_j[k+1, j+1].recCoeffs[:] += pi_j_Gk1.recCoeffs
        pi_j[k+1, j+1].recCoeffs[:] -= pi_j_Gk.recCoeffs
        # - pi_jm1
        pi_jm1[k+1, j+1].recCoeffs[:] = pi_jm1_F.recCoeffs
        pi_jm1[k+1, j+1].recCoeffs[:] += pi_jm1_Gk1.recCoeffs
        pi_jm1[k+1, j+1].recCoeffs[:] -= pi_jm1_Gk.recCoeffs


# Fine solution
print(' -- Sequential fine propagation')
if False:
    if True:
        alpha_F, beta_F = tridiagRPKW(nodes_F, weights_F)
    else:
        alpha_F, beta_F = quadRule.getAlphaBeta(nCoeff)
    j = np.arange(0, nCoeff, nAdvPerTS)
    alpha_F, beta_F = alpha_F[j], beta_F[j]
else:
    alpha_F, beta_F = np.zeros((2, nTS+1))
    beta_F[0] = beta[0, 0]
    alpha_F[0] = alpha[0, 0]
    # -- using fine Stieltjes time-stepping operator
    pi_j_F = pi_j[0, 0]
    pi_jm1_F = pi_jm1[0, 0]
    for j, deltaJ in zip(range(nTS), lDeltaJ):
        alpha_F[j+1], beta_F[j+1], pi_j_F, pi_jm1_F = multiStieljesAdvance(
            alpha_F[j], beta_F[j], pi_j_F, pi_jm1_F,
            scalarProductF, deltaJ)

j = np.array([0]+lJEnd)

# %% Beta plots
if plotBeta:
    plt.figure('beta-'+suffix)
    plt.plot(j, beta_F, 'o--', c='gray', label='Fine solution')
    for k in range(nIter+1):
        plt.plot(j, beta[k, :], label='k={}'.format(k))
    plt.legend()

# %% Error alpha plots
if False:
    plt.figure('error-alpha-'+suffix)
    for k in range(nIter+1):
        plt.semilogy(j, abs(alpha[k, :]-alpha_F), lSym[k]+'-',
                     label='k={}'.format(k), markevery=5)
    plt.legend()
    plt.grid(True)
    plt.xlabel('$j$')
    plt.ylabel('$e^k_j$ error in $\\alpha$')
    plt.ylim(1e-17, 1e-3)
    plt.savefig('error-alpha-'+suffix+'.pdf', bbox_inches='tight')


# %% Error beta plots
if True:
    plt.figure('error-beta-'+suffix)
    for k in range(nIter+1):
        plt.semilogy(j, abs(beta[k, :]-beta_F), lSym[k]+'-',
                     label='k={}'.format(k))
    plt.legend()
    plt.grid(True)
    plt.xlabel('$j$')
    plt.ylabel('$e^k_j$ error in $\\beta$')
    plt.ylim(1e-17, 1e-3)
    plt.savefig('error-beta-'+suffix+'.pdf', bbox_inches='tight')

# %% L1 error
if False:
    plt.figure('max-error-beta')
    k = np.array(range(nIter+1))
    
    errK = [max(abs(beta[k, :]-beta_F)) for k in range(nIter+1)]
    plt.semilogy(k, errK, lSym[iSym]+iStyle,
                 label=iLabel, c=lCol[iCol])
    tArgs = dict(horizontalalignment='center', verticalalignment='center',
                 fontsize=16)
    if iSym == 1 and iStyle == ':':
        plt.semilogy(k, errK[0]*7*(0.1)**k, '--', c='k', lw=1.2)
        plt.text(2.5, 0.1, '$\\sim 10^{-k}$', **tArgs)
    if iSym == 0 and iStyle == ':':
        plt.semilogy(k, errK[0]*10*(0.01)**k, '--', c='k', lw=1.2)
        plt.text(4.5, 5e-11, '$\\sim 10^{-2k}$', **tArgs)
    if iSym == 2 and iStyle == ':':
        plt.semilogy(k, errK[0]/50*(10**(-2.7))**k, '--', c='k', lw=1.2)
        plt.text(1, 1e-13, '$\\sim 10^{-2.7k}$', **tArgs)
    plt.legend()
    plt.grid(True)
    plt.xlabel('$k$')
    plt.ylabel('$E_k$ error in $\\beta$')
    plt.ylim(1e-17, 10)
    plt.savefig('max-error-beta.pdf', bbox_inches='tight')

# %% Residuum beta plots
if False:
    plt.figure('residuum-beta-'+suffix)
    for k in range(nIter):
        plt.semilogy(j, abs(beta[k+1, :]-beta[k, :]), lSym[k]+'-',
                     label='k={}'.format(k+1),
                     markevery=1)
    plt.legend()
    plt.grid(True)
    plt.xlabel('$j$')
    plt.ylabel('$e^k_j$ residual in $\\beta$')
    plt.ylim(1e-17, 1e-3)
    plt.savefig('residuum-beta-'+suffix+'.pdf', bbox_inches='tight')

# %% Fine error beta plots
if True:
    plt.figure('fError-beta')
    alpha_th, beta_th = quadRule.getAlphaBeta(nCoeff)
    alpha_th, beta_th = alpha_th[j], beta_th[j]
    plt.semilogy(j, abs(beta_F-beta_th),
                 label='$N_{quad,F}'+'={} ({})$'.format(
                    nQuad_F, 'AA' if useAA_F else 'F1'))
    plt.legend()
    plt.grid(True)

# %% Polynomials plots
if False:
    t = np.linspace(-1, 1, 500)
    plt.figure('orthogPoly-'+suffix)
    degree = 46
    plt.title('Monic polynomial order {}'.format(degree))
    # plt.plot(grid_F, quadRule.rec.evalMonicPoly(grid_F, degree))
    for k in range(nIter+1):
        plt.plot(t, pi_j[k, degree](t), label='k={}'.format(k))
    plt.legend()
