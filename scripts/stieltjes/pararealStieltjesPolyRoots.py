#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 11:56:44 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
from pythag import FactorPoly, mDict
from pythag.algebra.tridiag import tridiagRPKW
import pythag.quadrature.classicgauss as pqg

# Quadrature rule to compute, and algorithm settings
qr = 2
if qr == 1:
    quadRule = pqg.JacobiRule(0.25, 0.5)
elif qr == 2:
    name = 'ELECTRO'
    quadRule = pqg.GaussQuadRule(mDict[name], support=[-1, 1], name=name)
elif qr == 3:
    name = 'FUNNEL'
    quadRule = pqg.GaussQuadRule(mDict[name], support=[-1, 1], name=name)
nAdvPerTS = 1
nTS = 100
nCoeff = 1 + nTS*nAdvPerTS
nIter = 6
useOrthnPoly = False
useAA_F = True
useAA_G = False
plotBeta = False

# Coarse and fine grid settings
nQuad_G = int(nCoeff*2.0)
nQuad_F = 5000

# Figure suffix
suffix = 'polyRoots_QR{}_{}_k{}({},{})_G{}({})_F{}({})'.format(
    qr,
    'Orthn' if useOrthnPoly else 'Monic',
    nIter, nTS, nAdvPerTS,
    nQuad_G, 'AA' if useAA_G else 'F1',
    nQuad_F, 'AA' if useAA_F else 'F1')

# Arrays for recurrence coefficients
alpha = np.zeros((nIter+1, nTS+1))
beta = np.zeros_like(alpha)

# Coarse and fine quadrature rules
if useAA_G:
    nodes_G, weights_G = quadRule.getAsymptoticApproximation(nQuad_G)
else:
    nodes_G, weights_G = quadRule.getDiscreteApproximation(nQuad_G)
if useAA_F:
    nodes_F, weights_F = quadRule.getAsymptoticApproximation(nQuad_F)
else:
    nodes_F, weights_F = quadRule.getDiscreteApproximation(nQuad_F)

# Initial recurrence coefficient for each iterations
beta[:, 0] = np.sum(weights_F)
alpha[:, 0] = np.sum(nodes_F*weights_F)/beta[0, 0]

# Arrays for orthogonal polynomial values
pi_j = np.array(
    [[FactorPoly(None, deg=j) for j in range(0, nCoeff, nAdvPerTS)]
     for k in range(nIter+1)])
pi_jm1 = np.array(
    [[FactorPoly(None, deg=j-1) for j in range(0, nCoeff, nAdvPerTS)]
     for k in range(nIter+1)])
# -- initialization
if useOrthnPoly:
    for k in range(nIter+1):
        pi_j[k, 0].coeff = beta[0, 0]**(-0.5)


# Scalar product functions
def getScalarProductFunc(level='FINE'):

    if level == 'FINE':
        nodes, weights = nodes_F, weights_F
    elif level == 'COARSE':
        nodes, weights = nodes_G, weights_G
    else:
        raise ValueError('Cannot return scalarProduct for level={}'
                         .format(level))

    def scalarProduct(pi_jp1, pi_j):
        pi_jp1 = pi_jp1(nodes)
        pi_jp1 **= 2
        pi_jp1 *= weights
        s2 = np.sum(pi_jp1)
        pi_jp1 *= nodes
        s1 = np.sum(pi_jp1)
        pi_j = pi_j(nodes)
        pi_j **= 2
        pi_j *= weights
        s0 = np.sum(pi_j)
        alpha, beta = s1/s2, s2/s0
        return alpha, beta

    def scalarProduct2(pi, stage):
        if stage == 1:
            pi_j = pi(nodes)
            pi_j **= 2
            pi_j *= nodes**2
            pi_j *= weights
            s1 = max([0, np.sum(pi_j)])
            return s1
        elif stage == 2:
            pi_jp1 = pi(nodes)
            pi_jp1 **= 2
            pi_jp1 *= nodes
            pi_jp1 *= weights
            s2 = max([0, np.sum(pi_jp1)])
            return s2

    if useOrthnPoly:
        return scalarProduct2
    else:
        return scalarProduct


scalarProductG = getScalarProductFunc('COARSE')
scalarProductF = getScalarProductFunc('FINE')


# Time stepping function
def stieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, scalarProducts):
    if useOrthnPoly:
        beta_jp1 = scalarProducts(pi_j, 1) - alpha_j**2
        if pi_j.deg > 0:
            beta_jp1 -= beta_j
        # TODO : computation of pi_jp1
        pi_jp1 = 0
        raise NotImplementedError()
        alpha_jp1 = scalarProducts(pi_jp1, 2)
    else:
        if pi_j.deg == 0:
            pi_jp1 = FactorPoly([alpha_j])
        else:
            # Computation of the root of pi_jp1 using Newton method
            poly = FactorPoly(pi_j.roots.tolist()+[alpha_j])

            def newtonCorr(t):
                h = 1e-8
                v1 = poly(t)
                v2 = pi_jm1(t)
                d1 = poly(t+h)
                d1 -= v1
                d1 /= h
                d2 = pi_jm1(t+h)
                d2 -= v2
                d2 /= h
                return (v1 - beta_j*v2)/(d1 - beta_j*d2)

            eps = np.finfo(float).eps
            res = 1e99
            roots_jp1 = quadRule.getAsymptoticApproximation(pi_j.deg+1)[0]
            i = 0
            # print('---')
            # print(' -', pi_j, '|', pi_jm1)
            while res > eps and i < 10:
                corr = newtonCorr(roots_jp1)
                res = np.linalg.norm(corr, np.inf)
                # print(res)
                roots_jp1 -= corr
                i += 1
            if i == 10:
                print('WARNING : max number of Newton iterations reached')
            pi_jp1 = FactorPoly(roots_jp1)
        alpha_jp1, beta_jp1 = scalarProducts(pi_jp1, pi_j)
    return alpha_jp1, beta_jp1, pi_jp1, pi_j.copy()


def multiStieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, scalarProducts, nAdv):
    for n in range(nAdv):
        alpha_j, beta_j, pi_j, pi_jm1 = \
            stieljesAdvance(alpha_j, beta_j, pi_j, pi_jm1, scalarProducts)
    return alpha_j, beta_j, pi_j, pi_jm1.copy()


# Parareal initialization : k=0
print(' -- Parareal initialization')
# -- using coarse Stieltjes time-stepping operator
for j in range(nTS):
    alpha_G, beta_G, pi_j_G, pi_jm1_G = multiStieljesAdvance(
        alpha[0, j], beta[0, j], pi_j[0, j], pi_jm1[0, j],
        scalarProductG, nAdvPerTS)
    alpha[0, j+1] = alpha_G
    beta[0, j+1] = beta_G
    pi_j[0, j+1].roots[:] = pi_j_G.roots
    pi_jm1[0, j+1].roots[:] = pi_jm1_G.roots


# Parareal iterations
for k in range(nIter):
    print(' -- Parareal iteration {}/{}'.format(k+1, nIter))
    for j in range(nTS):
        # Subinterval solves
        alpha_F, beta_F, pi_j_F, pi_jm1_F = multiStieljesAdvance(
            alpha[k, j], beta[k, j], pi_j[k, j], pi_jm1[k, j],
            scalarProductF, nAdvPerTS)
        alpha_Gk1, beta_Gk1, pi_j_Gk1, pi_jm1_Gk1 = multiStieljesAdvance(
            alpha[k+1, j], beta[k+1, j], pi_j[k+1, j], pi_jm1[k+1, j],
            scalarProductG, nAdvPerTS)
        alpha_Gk, beta_Gk, pi_j_Gk, pi_jm1_Gk = multiStieljesAdvance(
            alpha[k, j], beta[k, j], pi_j[k, j], pi_jm1[k, j],
            scalarProductG, nAdvPerTS)
        # Parareal update formula
        # - alpha, beta
        alpha[k+1, j+1] = alpha_F + alpha_Gk1 - alpha_Gk
        beta[k+1, j+1] = beta_F + beta_Gk1 - beta_Gk
        # - pi_j
        pi_j[k+1, j+1].roots[:] = pi_j_F.roots
        pi_j[k+1, j+1].roots[:] += pi_j_Gk1.roots
        pi_j[k+1, j+1].roots[:] -= pi_j_Gk.roots
        # - pi_jm1
        pi_jm1[k+1, j+1].roots[:] = pi_jm1_F.roots
        pi_jm1[k+1, j+1].roots[:] += pi_jm1_Gk1.roots
        pi_jm1[k+1, j+1].roots[:] -= pi_jm1_Gk.roots


# Fine solution
print(' -- Sequential fine propagation')
if False:
    alpha_F, beta_F = tridiagRPKW(nodes_F, weights_F)
    j = np.arange(0, nCoeff, nAdvPerTS)
    alpha_F, beta_F = alpha_F[j], beta_F[j]
else:
    alpha_F, beta_F = np.zeros((2, nTS+1))
    beta_F[0] = beta[0, 0]
    alpha_F[0] = alpha[0, 0]
    # -- using fine Stieltjes time-stepping operator
    pi_j_F = pi_j[0, 0]
    pi_jm1_F = pi_jm1[0, 0]
    for j in range(nTS):
        alpha_F[j+1], beta_F[j+1], pi_j_F, pi_jm1_F = multiStieljesAdvance(
            alpha_F[j], beta_F[j], pi_j_F, pi_jm1_F,
            scalarProductF, nAdvPerTS)

j = np.arange(0, nCoeff, nAdvPerTS)

# %% Beta plots
if plotBeta:
    plt.figure('beta-'+suffix)
    plt.plot(j, beta_F, 'o--', c='gray', label='Fine solution')
    for k in range(nIter+1):
        plt.plot(j, beta[k, :], label='k={}'.format(k))
    plt.legend()

# %% Error beta plots
plt.figure('error-beta-'+suffix)
for k in range(nIter+1):
    plt.semilogy(j, abs(beta[k, :]-beta_F)/abs(beta_F),
                 label='k={}'.format(k))
plt.legend()
plt.grid()

# %% Polynomials plots
if False:
    t = np.linspace(-1, 1, 500)
    plt.figure('orthogPoly-'+suffix)
    degree = 46
    plt.title('Monic polynomial order {}'.format(degree))
    # plt.plot(grid_F, quadRule.rec.evalMonicPoly(grid_F, degree))
    for k in range(nIter+1):
        plt.plot(t, pi_j[k, degree](t), label='k={}'.format(k))
    plt.legend()
