# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 11:31:29 2018

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit, minimize
import pythag.quadrature.classicgauss as pqg

lN = np.array([2, 4, 5, 10, 20, 30, 50, 100, 200, 300, 500, 1000,
               1500, 2000])
lNLabel = np.array([2, 5, 10, 20, 2000])
lNLabel2 = np.array([2, 5, 10, 20, 50])
tauMax2 = 1

measure = 'LAGUERRE'
saveFig = False
plotWRatio = True
plotChristoffel = False

aL = 0

if measure == 'LAGUERRE':
    quadRule = pqg.LaguerreRule(a=aL)

weightFunc = quadRule.weightFunc
nodesAndWeights = [quadRule.getNodesAndWeights(n_, implementation='EIG_SCIPY')
                   for n_ in lN]
nMax = max(lN)*100
nodes_AA, weights_AA = quadRule.getAsymptoticApproximation(nMax, verbose=True)
rOmega = []

rightVal = []
rightValN = []
derivVal = []
derivN = []

for tau, omega in nodesAndWeights:
    n = len(tau)

    plt.figure('weight-convergence-'+measure)
    if n in lNLabel:
        plt.plot(tau, omega * n**0.5, 'o--', label='$n={}$'.format(n))
    print('n = {}, sumW = {}, tauMax = {}'.format(n, sum(omega), tau.max()))

    xPlot = np.linspace(0, 1, num=n+2)[1:-1]

    if n in lNLabel:
        plt.figure('nodes-convergence-'+measure)
        plt.plot(xPlot, tau/n, 'o--', label='$n={}$'.format(n))

    if n in lNLabel2:
        plt.figure('nodes-density-convergence-'+measure)
        plt.plot(tau, xPlot*(n+1)/n**0.5, 'o--', label='$n={}$'.format(n))
        tauMax2 = tau.max()

    rightVal.append(tau[-1]/n)
    rightValN.append(n)

    if n > 2:
        x2, x1, x0 = xPlot[-3:]
        u2, u1, u0 = (tau/n)[-3:]
        derivN.append(n)
        derivVal.append(
            (u0*(x2-x1)*(2*x0-x1-x2) + u1*(x2-x0)**2 - u2*(x1-x0)**2)
            / ((x1-x0)*(x2-x0)*(x2-x1))
            )


plt.figure('weight-convergence-'+measure)
# x = np.linspace(0, 10, num=500)
# plt.plot(x, np.pi*weightFunc(x) * x**0.5, 'k-', label='W.G conjecture')
plt.plot(nodes_AA, weights_AA*nMax**0.5, 'k-', label='$\\pi w(t) \\sqrt{t}$')
plt.legend()
plt.grid()
plt.ylabel('$\\sqrt{n} \\omega_n(t)$')
plt.xlabel('$t$')
plt.xlim(0, 10)
if saveFig:
    plt.savefig('cwf-{}.pdf'.format(measure), bbox_inches='tight')
plt.gcf().set_tight_layout(True)

s = np.linspace(0, 1, num=nMax+2)[1:-1]
plt.figure('nodes-convergence-'+measure)
# plt.plot(s, 4*(1-(1-s**2)**0.5), label='Ellipsis')
# F = 2/np.pi*((s*(1-s))**0.5 + np.arcsin(s**0.5))
# plt.plot(F, 4*s, c='k', label='Gawronsky')
plt.plot(s, nodes_AA/nMax, c='k', label='Gawronsky')

plt.grid()
plt.xlabel('$s$')
plt.ylabel('$\\tau_n(s)/n$')
plt.legend()
if saveFig:
    plt.savefig('cno-{}.pdf'.format(measure), bbox_inches='tight')
plt.gcf().set_tight_layout(True)


plt.figure('nodes-density-convergence-'+measure)
xi = np.linspace(0, tauMax2, num=500)
plt.plot(xi, 2/np.pi*xi**0.5, c='k', label='Gawronsky')
plt.grid()
plt.xlabel('$\\xi$')
plt.ylabel('$N_n(\\xi)$')
plt.legend()
plt.gcf().set_tight_layout(True)

if False:

    # Right derivative value for asymptotic node distribution
    plt.figure('nodes-conv-right-deriv-'+measure)
    plt.plot(derivN, derivVal, 'o-')

    def func(x, a, b):
        return a*x**b

    [a, b], _ = curve_fit(func, derivN, derivVal)
    plt.plot(derivN, func(np.array(derivN), a, b), '--',
             label='${:1.3f}'.format(a)+'N^{'+'{:1.3f}'.format(b)+'}$')
    plt.grid()
    plt.legend()
    plt.xlabel('$N$')

    # Right value for asymptotic node distribution
    plt.figure('nodes-conv-right-val-'+measure)
    plt.plot(rightValN, rightVal, 'o-')
    rightVal = np.array(rightVal)
    rightValN = np.array(rightValN)

    def func2(x, a, b, c, d):
        return a - np.exp(-b*x**c+d)

    def jac2(x, a, b, c, d):
        e = np.exp(-b*x**c+d)
        return np.array([np.ones_like(x), x**c*e, b*np.log(x)*x**c*e, e]).T

    cOpt, _ = curve_fit(func2, rightValN, rightVal, method='trf')

    def objFunc(x):
        return np.linalg.norm(func2(rightValN, *x)-rightVal)

    res = minimize(objFunc, cOpt, method='BFGS')
    cOpt2 = res.x

    plt.plot(rightValN, func2(rightValN, *cOpt2), '--',
             label='Regression')

    plt.legend()
    plt.grid()
    plt.xlabel('$N$')
