# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 11:31:29 2018

@author: lunet
"""
import numpy as np
import scipy.optimize as sco
import matplotlib.pyplot as plt
import pythag.quadrature.classicgauss as pqg

lN = np.array([2, 4, 5, 10, 20, 30, 50, 100, 500])
lN = lN[-1::-1]
lNLabel = np.array([2, 5, 10, 500])

measure = 'ELECTRO'
saveFig = False
plotWRatio = False
plotChristoffel = False

a = -1
b = 1

dicoMeasure = {'GAUSS1': lambda x: np.exp(-x**2),
               'GAUSS2': lambda x: np.exp(-1.5*x**2),
               'LOG': lambda x: np.log(2-x),
               'PYRAM': lambda x: 1-abs(x),
               'LEG': lambda x: 0*x + 1,
               'CHEBY1': lambda x: (1-x**2)**(-0.5),
               'CHEBY2': lambda x: (1-x**2)**(0.5),
               'CHEBY3': lambda x: (1-x)**(-0.5)*(1+x)**(0.5),
               'CHEBY4': lambda x: (1-x)**(0.5)*(1+x)**(-0.5),
               'SAW': lambda x: x + (x < 0),
               'EXP': lambda x: np.exp(x),
               'COS': lambda x: abs(np.cos(x*np.pi*2)),
               'STEP': lambda x: 1 + (x < 0),
               'HOLE': lambda x: (x > 0.5) + (x < -0.5) + 0.1,
               'GJ1': lambda x: abs(x)**(-0.75)*(1-x**2)**(-0.5),
               'ELECTRO': lambda x: 5/2*(x+1)*(x < -1+2/5) +
               5/2*(x-3/5)*(x > 3/5) + 1*(x >= -1+2/5) +
               5/2*(x+1/5)*(x > -1/5)*(x < 0) +
               5/2*(x-1/5)*(x < 1/5)*(x >= 0),
               'TRUMPET': lambda t: (t < 0)*(1-np.sqrt(abs(1-(t+1)**2))) +
               (t >= 0)*(1-np.sqrt(abs(1-(t-1)**2)))}

if measure not in dicoMeasure.keys() and not measure.startswith('JACOBI'):
    raise ValueError('Wrong measure choice')

if not measure.startswith('JACOBI'):
    weightFunc = dicoMeasure[measure]

if measure.startswith('CHEBY'):

    quadRule = pqg.GaussChebyshevRule(kind=int(measure[-1]))

elif measure.startswith('JACOBI'):
    quadRule = pqg.GaussJacobiRule(
        float(measure.split('_')[1]), float(measure.split('_')[2]))
    weightFunc = quadRule.weightFunc
else:

    if measure in ['DISC', 'GJ1', 'TRUMPET']:
        lN[8:] = 100
        lNLabel[-1] = 100

    if measure in ['COS', 'TRUMPET']:
        lN[9:] = 1000
        lNLabel[-1] = 1000

    quadRule = pqg.GaussQuadRule(weightFunc, name=measure,
                                 support=[a, b])
    quadRule.rec.ORTHPOL_PRECISION = 1e-6
    quadRule.buildAlphaBeta(lN.max())


nodesAndWeights = [quadRule.getNodesAndWeights(n_) for n_ in lN]
rOmega = []


def cwf(x):
    return weightFunc(x)*np.pi*((b-x)*(x-a))**0.5


# %%
lSym = ['s', '^', '*', '>', 'p', 'o']
lSym = lSym[-1::-1]
plt.figure('weight-convergence-'+measure)
t = np.linspace(a, b, num=500)
plt.plot(t, np.pi*quadRule.weightFunc(t), '--', c='black',
         label='$\\pi \\; w(t)$', lw=1.4)
plt.legend()
plt.grid(True)
plt.xlabel('$t$')
plt.ylabel('$J\\;\\omega_J(t)$')
plt.savefig('convWeights-{}-0.pdf'.format(measure), bbox_inches='tight')
i = 1
for tau, omega in nodesAndWeights:

    plt.figure('weight-convergence-'+measure)
    if len(tau) in lNLabel:
        plt.plot(tau, omega*len(tau), lSym[i-1]+'--',
                 label='$J={}$'.format(len(tau)))
        plt.legend()
        plt.grid(True)
        plt.savefig('convWeights-{}-{}.pdf'.format(measure, i),
                    bbox_inches='tight')
    print('Q = {}, sumW ='.format(len(tau)), sum(omega))

    if plotWRatio:
        plt.figure('wRatio-'+measure)
        wRatio = np.mean(cwf(tau) / (omega*len(tau)))
        plt.plot(len(tau), wRatio, 'o')
        rOmega.append(wRatio)

    plt.figure('nodes-convergence-'+measure)
    unifNodes = np.linspace(0, 1, len(tau)+2)[1:-1]

    if len(tau) in lNLabel:
        plt.plot(unifNodes, tau, lSym[i-1]+'--',
                 label='$J={}$'.format(len(tau)))
        plt.legend()
        plt.ylim(-1.2, 1.2)
        plt.xlim(0, 1)
        plt.grid(True)
        plt.xlabel('$s$')
        plt.ylabel('$\\tau_J(s)$')
        plt.savefig('convNodes-{}-{}.pdf'.format(measure, i),
                    bbox_inches='tight')
        i += 1

plt.figure('weight-convergence-'+measure)
plt.plot(t, cwf(t), 'k-',
         label='$\\pi \\sqrt{1-t^2} w(t)$')
plt.legend()
plt.grid(True)
plt.xlabel('$t$')
plt.ylabel('$Q\\;\\omega_J(t)$')
plt.savefig('convWeights-{}-{}.pdf'.format(measure, i),
            bbox_inches='tight')
plt.gcf().set_tight_layout(True)

plt.figure('nodes-convergence-'+measure)
s = np.linspace(0, 1, num=500)
plt.plot(s, (-(b-a)*np.cos(s*np.pi) + b + a)/2, 'k-',
         label='$-\\cos(s\\pi)$')
plt.legend()
plt.grid(True)
plt.xlabel('$s$')
plt.ylabel('$\\delta_J(s)$')
plt.savefig('convNodes-{}-{}.pdf'.format(measure, i),
            bbox_inches='tight')
plt.gcf().set_tight_layout(True)

if plotWRatio:
    plt.figure('wRatio-'+measure)
    rOmega = np.array(rOmega)

    def minFunc(x):
        a = x[0]
        b = 1
        return np.linalg.norm(1. + a/lN**b - rOmega)

    res = sco.minimize(minFunc, [1.], method='Nelder-Mead')
    print(res)

    a = res.x[0]
    b = 1
    plt.plot(lN, 1. + a/lN**b, 'k-', label='Analytical')

if plotChristoffel:
    # -- Christoffel function
    plt.figure('christoffel')
    x = np.linspace(-1, 1, num=200)
    for n in [5, 10, 20]:
        chFunc = quadRule.rec.getChristoffelFunc(n)
        plt.plot(x, chFunc(x), label='$N={}$'.format(n))
    plt.legend()

    plt.show()
