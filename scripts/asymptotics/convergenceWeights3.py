# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 11:31:29 2018

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt
import pythag.quadrature.classicgauss as pqg

lN = np.array([2, 4, 5, 10, 20, 30, 50, 100, 200, 300, 500, 1000, 2000, 4000])
lNLabel = np.array([2, 5, 10, 20, 2000, 4000])
lNLabel2 = np.array([])


measure = 'HERMITE'
saveFig = False
plotWRatio = True
plotChristoffel = False

mu = 0
theta = 0.5
lam = 1
epsilon = 1
pltBounds = (-4, 4)

if measure == 'HERMITE':
    quadRule = pqg.HermiteRule(mu)
    weightFunc = quadRule.weightFunc

    nPowWeight = 0.5
    nPowNodes = 0.5
    coeffNodes = 1/2**0.5


elif measure == 'LOGISTIC':
    quadRule = pqg.Logistic()
    weightFunc = quadRule.weightFunc

    lN = np.array([2, 4, 5, 10, 20, 30, 40])
    lNLabel = np.array([2, 5, 10, 20, 40])

    nPowWeight = 0.1
    nPowNodes = 1
    coeffNodes = 0.245

elif measure == 'MEIXNER-POLLACZEK':
    quadRule = pqg.MeixnerPollaczek(theta, lam)
    weightFunc = quadRule.weightFunc

    nPowWeight = 0.1
    nPowNodes = 1
    coeffNodes = 0.245

elif measure == 'BIMODAL':

    lN = np.array([2, 4, 5, 10, 20, 30, 50, 100])
    lNLabel = np.array([2, 5, 10, 20, 100])

    def weightFunc(t):
        return np.exp(-t**2)

    quadRule = pqg.GaussQuadRule(weightFunc, name=measure,
                                 support=[-np.inf, np.inf])
    quadRule.rec.GAUTSCHI_ALGO_PRECISION = 1e-6
    quadRule.rec.GAUTSCHI_ALGO_DISCRULE = 'HERMITE'
    quadRule.rec.GAUTSCHI_ALGO_NITERMAX = 5
    lNmax = lN.max()
    quadRule.buildAlphaBeta(lNmax)

    nPowWeight = 0.1
    nPowNodes = 1
    coeffNodes = 1/2**0.5

nodesAndWeights = [quadRule.getNodesAndWeights(n_) for n_ in lN]
nMax = max(lN)*100
nodes_AA, weights_AA = quadRule.getAsymptoticApproximation(nMax, verbose=True)
rOmega = []

rightVal = []
rightValN = []
derivVal = []
derivN = []

for tau, omega in nodesAndWeights:
    n = len(tau)

    plt.figure('weight-convergence-'+measure)
    if n in lNLabel:
        plt.plot(tau, omega * n**nPowWeight, 'o--', label='$n={}$'.format(n))
    print('n = {:04d}, sumW = {}, tauMax = {}'.format(
          n, sum(omega), tau.max()))

    xPlot = np.linspace(0, 1, num=n+2)[1:-1]

    if n in lNLabel:
        plt.figure('nodes-convergence-'+measure)
        plt.plot(xPlot, tau/n**nPowNodes, 'o--', label='$n={}$'.format(n))

    if n in lNLabel2:
        plt.figure('nodes-density-convergence-'+measure)
        plt.plot(tau, xPlot*(n+1)/n, 'o--', label='$n={}$'.format(n))
        tauMax2 = tau.max()

    rightVal.append(tau[-1]/n)
    rightValN.append(n)

    if n > 2:
        x2, x1, x0 = xPlot[-3:]
        u2, u1, u0 = (tau/n)[-3:]
        derivN.append(n)
        derivVal.append(
            (u0*(x2-x1)*(2*x0-x1-x2) + u1*(x2-x0)**2 - u2*(x1-x0)**2)
            / ((x1-x0)*(x2-x0)*(x2-x1))
            )


plt.figure('weight-convergence-'+measure)
t = np.linspace(*pltBounds, num=500)
plt.plot(nodes_AA, np.pi*weightFunc(nodes_AA)*coeffNodes, 'k-',
         label='${:3.2f}\\pi w(t) $'.format(coeffNodes))
plt.legend()
plt.grid()
plt.ylabel('$n^{'+'{}'.format(nPowWeight)+'} \\omega_n(t)$')
plt.xlabel('$t$')
plt.xlim(pltBounds)
if saveFig:
    plt.savefig('cwf-{}.pdf'.format(measure), bbox_inches='tight')
plt.gcf().set_tight_layout(True)

s = np.linspace(0, 1, num=nMax+2)[1:-1]
plt.figure('nodes-convergence-'+measure)
plt.plot(s, nodes_AA/nMax**0.5, c='k', label='Gawronsky')
plt.hlines([-2**0.5, 2**0.5], 0, 1, linestyles='--', linewidth=1)

plt.grid()
plt.xlabel('$s$')
plt.ylabel('$\\tau_n(s)/n^{'+'{}'.format(nPowNodes)+'}$')
plt.legend()
if saveFig:
    plt.savefig('cno-{}.pdf'.format(measure), bbox_inches='tight')
plt.gcf().set_tight_layout(True)


if False:
    plt.figure('nodes-density-convergence-'+measure)
    xi = np.linspace(0, tauMax2, num=500)
    # plt.plot(xi, 2/np.pi*xi**0.5, c='k', label='Gawronsky')
    plt.grid()
    plt.xlabel('$\\xi$')
    plt.ylabel('$N_n(\\xi)$')
    plt.legend()
    plt.gcf().set_tight_layout(True)
