#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 14:49:14 2019

@author: lunet
"""
import numpy as np
import pythag.algebra.tridiag as pat
import pythag.quadrature.classicgauss as pqg
import matplotlib.pyplot as plt


quadRule = pqg.LaguerreRule()
alpha, beta = quadRule.getAlphaBeta(400)
errAlpha, errBeta = [], []

for n in [5, 10, 20, 30, 40, 50, 100, 190, 195, 196]:

    if True:
        nodes, weights = quadRule.getNodesAndWeights(
            n, implementation='GOLUB_WELSCH_FORTRAN')
    else:
        nodes, weights = quadRule.getAsymptoticApproximation(n)

    mA = pat.quadRuleMatrix(nodes, weights)
    print('n={}, condition number : {:3.2f}'.format(n, np.linalg.cond(mA)))

    if False:
        alpha1, beta1 = pat.tridiagRPKW(nodes, weights)
    elif True:
        Q, alpha1, beta1 = pat.tridiagLanczos(mA)
        if False:
            plt.figure('N={}'.format(n))
            plt.imshow(Q)
            plt.colorbar()
    else:
        alpha1, beta1 = pat.tridiagStieltjes(nodes, weights)

    errBeta.append(abs(beta[:n]-beta1)/abs(beta[:n]))

if False:
    plt.figure('alpha-beta')
    p = plt.semilogy(alpha, 'o--', markevery=0.1, label='Exact')
    plt.semilogy(alpha1, label='Tridiag', c=p[0].get_color())
    p = plt.semilogy(beta, 'o--', markevery=0.1, label='Exact')
    plt.semilogy(beta1, label='Tridiag', c=p[0].get_color())
    plt.legend()
    plt.grid()
    plt.xlabel('$j$')

if True:

    plt.figure('Err-beta')
    for err in errBeta:
        plt.semilogy(err, label='N={}'.format(len(err)))
    plt.legend()
    plt.grid()
    plt.xlabel('$j$')

plt.show()
