#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 18:30:13 2018

@author: telu
"""
import sympy as sy


def givens(n, a, b, i, j):
    R = sy.eye(n, n)
    sqrt = sy.sqrt(a**2+b**2)
    c, s = a/sqrt, b/sqrt
    R[i, i] = c
    R[i, j] = s
    R[j, i] = -s
    R[j, j] = c
    return R


def arrowMatrix(n, symbolType='LATEX', quadRule='GENERAL'):

    def genSym():
        if quadRule == 'GENERAL':
            if symbolType == 'LATEX':
                return lambda i: sy.Symbol('\\omega_{'+str(i)+'}'), \
                    lambda i: sy.Symbol('\\tau_{'+str(i)+'}')
            elif symbolType == 'TERM':
                return lambda i: sy.Symbol('w'+str(i)), \
                    lambda i: sy.Symbol('t'+str(i))
            else:
                raise ValueError('Wrong argument for symbolType')
        elif quadRule == 'NC':
            h = sy.Symbol('h')
            return lambda i: h, lambda i: -1 + (2*i-1)*h/2
        else:
            raise ValueError('Wrong argument for quadRule')

    omega, tau = genSym()

    A = sy.zeros(n+1)
    A[0, 0] = 1
    for i in range(1, n+1):
        A[0, i] = sy.sqrt(omega(i))
        A[i, 0] = sy.sqrt(omega(i))
        A[i, i] = tau(i)
    return A


def tridiagGivens(A, stage='ALL', simplify=False):
    J = A.copy()
    N = A.shape[0]
    R = sy.eye(N)
    Q = R.copy()

    _s = [0]

    def incrementStage():
        _s[0] += 1
        if _s[0] == stage:
            return True
        return False

    def simplifyJ():
        if simplify:
            J.simplify()

    for i in range(2, N):
        # Compute rotation matrix
        a, b = J[1, 0], J[i, 0]
        sqrt = sy.sqrt(a**2+b**2)
        c, s = a/sqrt, b/sqrt
        R[1, 1] = c
        R[1, i] = s
        R[i, 1] = -s
        R[i, i] = c
        # Apply rotation
        Q = Q*R
        J = R*J
        simplifyJ()
        if incrementStage():
            return Q, None, None, J
        J = J*(R.T)
        simplifyJ()
        if incrementStage():
            return Q, None, None, J
        # Re-initialize rotation matrix
        R[1, 1] = 1
        R[1, i] = 0
        R[i, 1] = 0
        R[i, i] = 1
        for j in range(2, i):
            # Compute rotation matrix
            a, b = J[j, j-1], J[i, j-1]
            sqrt = sy.sqrt(a**2+b**2)
            c, s = a/sqrt, b/sqrt
            R[j, j] = c
            R[j, i] = s
            R[i, j] = -s
            R[i, i] = c
            # Apply rotation
            Q = Q*R
            J = R*J
            simplifyJ()
            if incrementStage():
                return Q, None, None, J
            J = J*(R.T)
            simplifyJ()
            if incrementStage():
                return Q, None, None, J
            # Re-initialize rotation matrix
            R[j, j] = 1
            R[j, i] = 0
            R[i, j] = 0
            R[i, i] = 1

    alpha = [J[i, i] for i in range(1, N)]
    beta = [J[i, i+1] for i in range(1, N-1)]
    return Q, alpha, beta, J


def tridiagLanczos(A, stage='ALL'):

    N = A.shape[0]

    # Build the initial vector r0
    r0 = sy.zeros(N, 1)
    r0[0] = 1
    beta0 = r0.norm()

    # Initialize output variables
    Q = sy.zeros(N)
    alpha = sy.zeros(N, 1)
    beta = sy.zeros(N, 1)

    # Initialize loop variables
    rjm1 = r0
    qjm1 = sy.zeros(N, 1)
    bjm1 = beta0

    # Algorithm require storage of N*N + 7*N float64

    # Loop for Lanczos algorithm
    s = 0
    for j in range(N):
        s += 1
        # Stage 1 - compute orthogonal base vector
        qj = rjm1
        qj /= bjm1
        Q[:, j] = qj
        # Stage 2 - evaluate A operator
        uj = A*qj
        # Stage 3 - compute new r_{j} = u_{j} - q_{j-1}*beta_{j-1}
        rj = uj - bjm1*qjm1
        # Stage 4 - compute alpha
        aj = qj.T*rj
        alpha[j] = aj
        # Stage 5 - save qj value for next iteration
        qjm1 = qj
        # Stage 6 - update r
        qj *= aj
        rj -= qj
        # Stage 7 - compute beta
        bj = rj.norm()
        beta[j] = bj
        # Stage 8 - update indexes
        rjm1 = rj
        bjm1 = bj

        if s == stage:
            break

    beta.applyfunc(lambda x: x**2)

    return Q, alpha[1:], beta[1:-1]
