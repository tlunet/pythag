#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the definition of Legendre polynomials objects,

Classes
-------
Legendre :
    define a Legendre polynomial
"""
import numpy as np
from .base import Polynom
monom = Polynom.monom


def legendreRecurrence(n, x):
    if n == 0:
        return 1 + x*0
    elif n == 1:
        return x
    else:
        return ((2*n-1)*x*legendreRecurrence(n-1, x) -
                (n-1)*legendreRecurrence(n-2, x)) / float(n)


def dLegendreRecurrence(n, x):
    if (n == 0):
        return x*0
    else:
        return (n/(x**2-1))*(x*legendreRecurrence(n, x) -
                             legendreRecurrence(n-1, x))


def ddLegendreRecurrence(n, x):
    return (n*(n+1)*legendreRecurrence(n, x) -
            2.0*x*dLegendreRecurrence(n, x)) / (x**2-1)


def dddLegendreRecurrence(n, x):
    return (2.0*x*ddLegendreRecurrence(n, x) -
            (n*(n+1.0)-2.0)*dLegendreRecurrence(n, x)) / (1-x**2)


class Legendre(Polynom):
    """Define a Legendre polynomial"""

    def __init__(self, n, buildCoeff=False):
        self.n = n
        self.coeff = ['Legendre polynomial of order '+str(n)]
        if buildCoeff:
            self._buildCoeff()

    def _buildCoeff(self):
        print(' -- Building Legendre polynomial coefficients')
        self.coeff = legendreRecurrence(self.n, monom()).coeff.copy()
        self.COEFF_BUILT = True
        print(' -- Done')

    def eval(self, xVal):
        if not self.COEFF_BUILT:
            xVal = np.asarray(xVal).ravel()
            return legendreRecurrence(self.n, xVal)
        else:
            return super(Legendre, self).eval(xVal)

    def evalDeriv(self, xVal, d=1):
        if not self.COEFF_BUILT:
            xVal = np.asarray(xVal).ravel()
            if d == 1:
                return dLegendreRecurrence(self.n, xVal)
            elif d == 2:
                return ddLegendreRecurrence(self.n, xVal)
            elif d == 3:
                return dddLegendreRecurrence(self.n, xVal)
            else:
                return super(Legendre, self).evalDeriv(xVal, d)
        else:
            return super(Legendre, self).evalDeriv(xVal, d)

    def integ(self, a, b):
        n = self.n
        if n == 0:
            return (b-a)
        else:
            return (legendreRecurrence(n+1, b) -
                    legendreRecurrence(n-1, b) -
                    legendreRecurrence(n+1, a) +
                    legendreRecurrence(n-1, a)) / float(2*n+1)


def dLegendreRoots(i, n, tol=1e-16):
    if n < 2:
        raise ValueError("n must be > 2")
    else:
        # Approximation of ???
        x = (1.0-3.*(float(n)-2.0)/(8.0*(float(n)-1.0)**3)) \
            * np.cos(np.pi*(4.0*float(i)-3.0)/(4.0*(float(n)-1.0)+1.0))
        error = 10.*tol
        iters = 0
        while (error > tol) and (iters < 10):
            dx = -(2*dLegendreRecurrence(n, x)*ddLegendreRecurrence(n, x)) / \
                (2*ddLegendreRecurrence(n, x)**2 -
                 dLegendreRecurrence(n, x) * dddLegendreRecurrence(n, x))
            x = x+dx
            iters = iters+1
            error = abs(dx)
    return x


def legendreQuadPoints(n):
    points = []
    for i in range(int(n/2)+1, n):
        points.append(dLegendreRoots(i-1, n-1))
    points.append(-1.0)
    points = np.array(points)
    if n % 2 == 0:
        points = np.concatenate((points[::-1], -1.0*points))
    else:
        points = np.concatenate((points[::-1], -1.0*points[1:]))
    return np.array(points)


def legendreQuadRule(n):
    nodes = legendreQuadPoints(n)
    n = float(n)
    weigths = [2./(n*(n-1)*legendreRecurrence(n-1, xi)**2) for xi in nodes]
    weigths[0] = 2/float(n*(n-1))
    weigths[-1] = 2/float(n*(n-1))
    return nodes, np.array(weigths)
