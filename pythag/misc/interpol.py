#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 18 17:40:43 2017

@author: t.lunet
"""
import numpy as np
from time import time, sleep
import scipy.linalg as lns
from mpi4py import MPI


_dicoUnifCoeff = {
    1: [[1], 2.],
    3: [[-1, 9], 16.],
    5: [[3, -25, 150], 256.],
    7: [[-5, 49, -245, 1225], 2048.],
    9: [[35, -405, 2268, -8820, 39690], 65536.],
    11: [[-63, 847, -5445, 22869, -76230, 320166], 524288.],
    13: [[231, -3549, 26026, -122694, 429429, -1288287, 5153148], 8388608.],
    15: [[-429, 7425, -61425, 325325, -1254825, 3864861, -10735725, 41409225],
         67108864.],
    17: [[6435, -123981, 1144440, -6762600, 28928900, -96705180, 270774504,
          -709171320, 2659392450], 4294967296.],
    19: [[-12155, 258115, -2632773, 17214285, -81376620, 298380940, -895142820,
          2327371332, -5818428330, 21334237210], 34359738368.],
    21: [[46189, -1072071, 11981970, -86003918, 446558805, -1794354471,
          5848266424, -16112570760, 39475798362, -95034329390, 342123585804],
         549755813888.]}


def invvander_th(mV):
    n = mV.shape[0]
    mLinv, mUinv = np.zeros_like(mV), np.zeros_like(mV)
    x = mV[:, 1]

    for i in range(n):
        for j in range(n):
            # Computing mLinv
            if i == 0 and j == 0:
                mLinv[i, j] = 1
            elif i < j:
                pass
            else:
                mLinv[i, j] = 1.
                for k in range(i+1):
                    if k != j:
                        mLinv[i, j] /= (x[j]-x[k])
            # Computing mUinv
            if i == j:
                mUinv[i, j] = 1
            elif j == 0:
                pass
            else:
                mUinv[i, j] = mUinv[i-1, j-1] - mUinv[i, j-1]*x[j-1]

    return mUinv.dot(mLinv)


def invvander_scipy(mV):
    return lns.inv(mV)


invvander = invvander_scipy


class HighOrderStructGridInterpolator(object):

    TOL_NONCONSISTENCY = 1e-8
    TOL_INVERTING = 1e-8
    TOL_EQUALCOORDINATE = 1e-6
    RESCALING_BOUND = 1
    USE_NEUMANN_O2 = True
    VERBOSE = True
    USE_OPT = True
    NDEBUG = True
    MPI_NGROUP_DEFAULT = 5

    def __init__(self, p, xG, yG, zG, xF=None, yF=None, zF=None,
                 xBoundary='PER', yBoundary='PER', zBoundary='PER',
                 xL=None, yL=None, zL=None, xR=None, yR=None, zR=None,
                 keepX=False, keepY=False, keepZ=False):

        dim = 3
        self.dim = dim

        # Coarse coordinates and number of points
        self.xG = [np.array(x).ravel() for x in [xG, yG, zG]]

        self.xGLoc = [None]*dim
        self.iGL = [None]*dim
        self.iGR = [None]*dim

        # Fine coordinates and number of points
        self.xF = [x for x in [xF, yF, zF]]
        self.xFLoc = [None]*dim

        # Boundary conditions
        self.lbType = [bType for bType in [xBoundary, yBoundary, zBoundary]]
        self.rbType = [bType for bType in [xBoundary, yBoundary, zBoundary]]
        # Uniform directions
        self.xUnif = [False]*dim
        # Left and right coordinate bound
        self.xL = [x for x in [xL, yL, zL]]
        self.xR = [x for x in [xR, yR, zR]]
        # Use right or left bound or not
        self.useWallL, self.useWallR = [False]*3, [False]*3

        # Keep coarse coordinates if asked
        if keepX:
            self.xF[0] = self.xG[0].copy()
        if keepY:
            self.xF[1] = self.xG[1].copy()
        if keepZ:
            self.xF[2] = self.xG[2].copy()

        # Indexes arrays
        self.issG = [None]*dim
        self.icpF, self.icpG = [None]*dim, [None]*dim
        self.incpF = [None]*dim

        # Interpolation coefficient arrays
        self.ci = [None]*dim

        # Interpolation order
        self.p = int(p)
        if self.p % 2 == 0:
            raise ValueError('Cannot accept even interpolation order')
        shapeG = np.array(self.nXG)
        if min(shapeG[np.argwhere(shapeG != 1)])[0] < self.p+1:
            raise ValueError('To low number of coarse points for the order {}'
                             .format(self.p))

        # MPI stuff
        self.gComm, self.sComm, self.tComm = self._initMPI()

        self.MPI_RANK = self.sComm.Get_rank()
        self.MPI_SIZE = self.sComm.Get_size()
        self.MPI_ON = (self.MPI_SIZE > 1)
        self.MPI_GROUP = self.tComm.Get_rank()
        self.MPI_NGROUP = self.tComm.Get_size()
        self.MPI_GROUP_ON = (self.MPI_NGROUP == 5)
        self.MPI_GLOBAL_RANK = self.gComm.Get_rank()
        self.MPI_GLOBAL_SIZE = self.gComm.Get_size()

        # Pre-process entry grid data in each direction
        for i in range(dim):
            d = 'x' if i == 0 else 'y' if i == 1 else 'z'
            # Checking phase
            self.msg('Checking coordinates in {} direction'.format(d))
            t0 = time()
            self._checkCoordinate(i)
            tComp = time() - t0
            self.msg('Computation time: {}s'.format(tComp), 0)
            # Indexes computation
            self.msg('Computing indexes in {} direction'.format(d))
            t0 = time()
            self._computeIndexes(i)
            tComp = time() - t0
            self.msg('Computation time: {}s'.format(tComp), 0)
            # Interpolation coefficient computation
            self.msg('Computing interpolation coefficients in {} direction'
                     .format(d))
            t0 = time()
            self._computeInterpolationCoeff(i)
            tComp = time() - t0
            self.msg('Computation time: {}s'.format(tComp), 0)

        self._setupMPIDecomposition()

        if not self.MPI_ON:
            def passingTrough(data):
                return data
            self._scatterDataG = passingTrough
            self._gatherDataF = passingTrough

        if self.USE_OPT:
            # Check for optimizations
            self._checkOptA()
            self._checkOptB()

    @property
    def nXG(self): return [0 if x is None else len(x) for x in self.xG]

    @property
    def nXGLoc(self): return [0 if x is None else len(x) for x in self.xGLoc]

    @property
    def nXF(self): return [0 if x is None else len(x) for x in self.xF]

    @property
    def nXFLoc(self): return [self.nncpF[i] + len(self.icpF[i])
                              for i in range(self.dim)]

    @property
    def nncpF(self): return [0 if x is None else len(x) for x in self.incpF]

    def msg(self, s, level=1, MPI_ALLGROUP=False):
        if not self.VERBOSE:
            return
        header = ' -- ' if level == 0 else ''
        if self.MPI_RANK == 0:
            if self.MPI_GROUP_ON and MPI_ALLGROUP:
                header += 'G{}: '.format(self.MPI_GROUP)
            if (self.MPI_GROUP == 0) or (MPI_ALLGROUP):
                print(header+s)

    def _msgDebug(self, msg):
        if self.NDEBUG:
            return
        header = ''
        mpi = self.gComm
        mpi.Barrier()
        if self.MPI_GROUP_ON:
            sleep(self.MPI_GROUP*0.05)
            header = 'G{}'.format(self.MPI_GROUP)
        sleep(self.MPI_RANK*0.05)
        print(header+'P{}: {}'.format(self.MPI_RANK, msg))
        mpi.Barrier()

    def _checkCoordinate(self, i):
        d = 'x' if i == 0 else 'y' if i == 1 else 'z'
        # Set up data pointer
        cG = self.xG[i]

        # Check for uniform grid direction
        self.xUnif[i] = np.allclose(
            cG, np.linspace(cG[0], cG[-1], num=cG.size)) and len(cG) != 1
        if self.xUnif[i] and self.rbType[i] == 'PER':
            self.xL[i] = cG[0]
            self.xR[i] = 2*cG[-1]-cG[-2]

        # Set up data pointer
        cL, cR = self.xL[i], self.xR[i]

        # Contruct fine grid if not given
        if self.xF[i] is None and self.xUnif[i] and self.rbType[i] == 'PER':
            self.xF[i] = np.linspace(cL, cR, num=2*self.nXG[0], endpoint=False)
        # Get fine grid if given
        elif self.xF[i] is not None:
            self.xF[i] = np.sort(np.array(self.xF[i])).ravel()
        else:
            raise ValueError('Missing fine coordinate for '+d +
                             ' with non uniform coarse grid')

        # Check if coordinate bounds are given
        if (cR is None and self.rbType[i] == 'PER' and len(self.xG[i]) != 1):
            raise ValueError(
                'Missing right coordinate bound for periodic conditions')
        if cL is None:
            cL = self.xG[i][0]
        if cR is None:
            cR = self.xG[i][-1]
        if np.any(self.xF[i] < cL) and self.lbType[i] == 'PER':
            raise ValueError('{} dir.: one fine coordinate out'
                             .format(d) + ' of left bound {}'.format(cL))
        if np.any(self.xF[i] > cR) and self.rbType[i] == 'PER':
            raise ValueError('{} dir.: one fine coordinate out'
                             .format(d) + ' of right bound {}'.format(cR))
        if self.rbType[i] == 'WALL':
            cG = cG.tolist()
            if cL < cG[0]:
                self.msg('{} dir.: using left wall value for coarse grid'
                         .format(d), 0)
                cG = [cL] + cG
                self.useWallL[i] = True
            if cR > cG[-1]:
                self.msg('{} dir.: using right wall value for coarse grid'
                         .format(d), 0)
                cG = cG + [cR]
                self.useWallR[i] = True
            self.xG[i] = np.array(cG)

    def _computeIndexes(self, i):
        d = 'x' if i == 0 else 'y' if i == 1 else 'z'
        # Set up data pointers and lists
        cG, cF = self.xG[i], self.xF[i]
        p = self.p
        issG, icpF, icpG, incpF = [], [], [], []

        # Auxiliary variables
        iF = 0
        cont = True

        # Compute issG, icpF and icpG lists
        if len(cF) == 1:
            icpF = [0]
            icpG = [0]
        else:
            tolBase = abs(min(np.ediff1d(cF)))
            for iG in range(self.nXG[i]):
                while cG[iG]-cF[iF] > self.TOL_EQUALCOORDINATE*tolBase:
                    issG.append(iG-1)
                    iF += 1
                    if iF > len(cF)-1:
                        cont = False
                        break
                if not cont:
                    break
                elif abs(cG[iG]-cF[iF]) <= self.TOL_EQUALCOORDINATE*tolBase:
                    icpF.append(iF)
                    icpG.append(iG)
                    iF += 1
                    if iF > len(cF)-1:
                        break
            while iF < len(cF):
                issG.append(iG)
                iF += 1

        # Transform index list into numpy arrays
        self.icpF[i] = np.array(icpF, dtype=int)
        self.icpG[i] = np.array(icpG, dtype=int)

        # Modify issG according to periodic boundary conditions
        self.issG[i] = np.array(issG, dtype=int)-(p+1)//2+1
        issG = self.issG[i]

        # Modify issG according to wall boundary conditions if used
        if self.rbType[i] == 'WALL':
            np.clip(issG, 0, self.nXG[i]-p-1, out=issG)

        # Compute incpF array
        for l in range(self.nXF[i]):
            if l not in icpF:
                incpF.append(l)
        # Transform index list into numpy arrays
        self.incpF[i] = np.array(incpF, dtype=int)

        # Check arrays
        assert len(issG) == len(incpF)
        assert len(icpF) == len(icpG)
        assert self.nncpF[i] + len(icpF) == self.nXF[i]
        if issG.size > 0:
            assert max(issG) < self.nXG[i]

        # Print informations
        self.msg('{} dir.: {} common points, {} points to interpolate'
                 .format(d, len(icpF), self.nncpF[i]), 0)

    def _computeInterpolationCoeff(self, i):
        d = 'x' if i == 0 else 'y' if i == 1 else 'z'
        # Set up data pointers
        p = self.p
        incpF, nncpF, issG = self.incpF[i], self.nncpF[i], self.issG[i]
        cG, cF = self.xG[i], self.xF[i]
        cL, cR = self.xL[i], self.xR[i]
        dUnif = self.xUnif[i]
        scalBound = self.RESCALING_BOUND

        # Special case if uniform grid and periodic boundary conditions
        if dUnif and self.rbType[i] == 'PER':
            self.ci[i] = np.zeros((p+1, 1, 1, 1))
            ci = self.ci[i]  # Get pointer to ci data
            for j in range((p+1)//2):
                coeff = _dicoUnifCoeff[p][0][j]/_dicoUnifCoeff[p][1]
                ci[j, 0, 0, 0], ci[-j-1, 0, 0, 0] = coeff, coeff
            self._checkInterpolationCoefficients(i)
            return

        # Auxiliary variables
        j0_ = None
        cS = None
        cScal = None
        mVinv = None
        if d == 'x':
            self.ci[0] = np.zeros((p+1, nncpF, 1, 1))
        elif d == 'y':
            self.ci[1] = np.zeros((p+1, 1, nncpF, 1))
        elif d == 'z':
            self.ci[2] = np.zeros((p+1, 1, 1, nncpF))

        # Handling eventual periodic boundary conditions
        npl = 0 if len(issG) == 0 else max([-min(issG), 0])
        npr = 0 if len(issG) == 0 else max([max(issG+p+1)-self.nXG[i], 0])
        cGB = cG.tolist()
        for i in range(npl):
            cGB = [cL-cR+cG[-i-1]] + cGB
        for i in range(npr):
            cGB = cGB + [cR-cR+cG[i]]

        # Loop on each fine point
        for j in range(nncpF):
            j0 = issG[j]
            iI = incpF[j]
            if j0 != j0_:
                # Scale coarse coordinate
                cS = np.array(cGB[npl+j0:npl+j0+p+1])
                cScal = 2*scalBound*(cS-cS[0])/(cS[-1]-cS[0])-scalBound
                # Invert the Vandermonde matrix of the stencil
                mV = np.vander(cScal, N=p+1, increasing=True)
                mVinv = invvander(mV)
                nonDiagDefault = np.max(mVinv.dot(mV)-np.eye(p+1))
                try:
                    assert (nonDiagDefault < self.TOL_INVERTING)
                except AssertionError:
                    self.msg('Warning: found inverting default of {}'
                             .format(nonDiagDefault), 0)
                j0_ = j0
            # Rescale and build interpolation coordinates
            cIScal = 2*scalBound*(cF[iI]-cS[0])/(cS[-1]-cS[0])-scalBound
            cIv = np.vander([cIScal], N=p+1, increasing=True).T
            # Compute the c_j coefficients
            if d == 'x':
                np.sum(cIv*mVinv, axis=0, out=self.ci[0][:, j, 0, 0])
            elif d == 'y':
                np.sum(cIv*mVinv, axis=0, out=self.ci[1][:, 0, j, 0])
            elif d == 'z':
                np.sum(cIv*mVinv, axis=0, out=self.ci[2][:, 0, 0, j])

        # Check coefficients
        self._checkInterpolationCoefficients(i)

    def _checkInterpolationCoefficients(self, i):
        d = 'x' if i == 0 else 'y' if i == 1 else 'z'
        msg = 'Warning: found non consistency default of {}'
        tol = self.TOL_NONCONSISTENCY
        ci = self.ci[i]
        if d == 'x':
            for l in range(ci.shape[1]):
                try:
                    assert (ci[:, l, 0, 0].sum()-1) < tol
                except AssertionError:
                    self.msg(msg.format(ci[:, l, 0, 0].sum()-1), 0)
        elif d == 'y':
            for l in range(ci.shape[2]):
                try:
                    assert (ci[:, 0, l, 0].sum()-1) < tol
                except AssertionError:
                    self.msg(msg.format(ci[:, 0, l, 0].sum()-1), 0)
        elif d == 'z':
            for l in range(ci.shape[3]):
                try:
                    assert (ci[:, 0, 0, l].sum()-1) < tol
                except AssertionError:
                    self.msg(msg.format(ci[:, 0, 0, l].sum()-1), 0)

    def getMemoryUsage(self):
        nBytes = 0
        for elt in self.__dict__:
            attr = self.__getattribute__(elt)
            if isinstance(attr, np.ndarray):
                nBytes += attr.nbytes
        return nBytes

    def _initMPI(self):
        """
        Initialize the MPI COMMWORLDs from the global MPI COMMWORLD
        """

        gComm = MPI.COMM_WORLD
        gRank = gComm.Get_rank()
        gSize = gComm.Get_size()

        if gSize % self.MPI_NGROUP_DEFAULT == 0:
            nps = gSize // self.MPI_NGROUP_DEFAULT
            npt = self.MPI_NGROUP_DEFAULT
        else:
            nps = gSize
            npt = 1

        self.MPI_RANK = gRank
        self.MPI_GROUP = 0
        self.MPI_GROUP_ON = False

        if npt != 1:
            self.msg("MPI: using {} MPI groups".format(npt), 0)

        # Construction of MPI CommWorlds
        tColor = gRank % nps
        tComm = gComm.Split(tColor, gRank)
        gComm.Barrier()
        sColor = (gRank-gRank % nps)//nps
        sComm = gComm.Split(sColor, gRank)
        gComm.Barrier()

        return gComm, sComm, tComm

    def _setupMPIDecomposition(self):
        mpi = self.sComm
        nProcs = self.MPI_SIZE
        nPts = self.nXG

        rest = nProcs
        facs = [2, 3, 1]
        exps = [0, 0, 0]
        for n in [0, 1]:
            while (rest % facs[n]) == 0:
                exps[n] = exps[n] + 1
                rest = rest // facs[n]
        if (rest > 1):
            facs[2] = rest
            exps[2] = 1

        nProcDir = [1, 1, 1]
        for n in [2, 1, 0]:
            while exps[n] > 0:
                dummymax = -1
                dmax = 0
                for d in [0, 1, 2]:
                    dummy = (nPts[d] + nProcDir[d] - 1) // nProcDir[d]
                    if (dummy >= dummymax):
                        dummymax = dummy
                        dmax = d
                nProcDir[dmax] = nProcDir[dmax] * facs[n]
                exps[n] = exps[n] - 1

        if nProcs > 1:
            self.msg('MPI: using {} procs'.format(nProcs), 0)
            self.msg('MPI: nProcX, nProcY, nProcZ = {}'.format(nProcDir), 0)

        periods = np.array(np.array(self.rbType) == 'PER', dtype=int)
        nProcDir = np.array(nProcDir, dtype=int)
        self.nProcDir = nProcDir

        # Create cartesian topology, get neigbour ranks
        cartComm = mpi.Create_cart(nProcDir, periods)
        iProc = np.array(cartComm.Get_coords(self.MPI_RANK))
        self.iProc = iProc
        self.lRank = [None]*self.dim
        self.rRank = [None]*self.dim
        for i in range(self.dim):
            self.lRank[i], self.rRank[i] = \
                cartComm.Shift(i, int(nProcDir[i] > 1))

        # Get local number of coarse points and global starting indexes
        for i in range(self.dim):
            nXLoc0 = self.nXG[i] // nProcDir[i]
            nRest = self.nXG[i] - nProcDir[i]*nXLoc0
            nXLoc = nXLoc0 + 1*(iProc[i] < nRest)

            self.iGL[i] = iProc[i]*nXLoc0 + nRest*(iProc[i] >= nRest) + \
                iProc[i]*(iProc[i] < nRest)
            self.iGR[i] = self.iGL[i] + nXLoc
            self.xGLoc[i] = self.xG[i][self.iGL[i]:self.iGR[i]]

        # Change boundary conditions if necessary
        for i in range(self.dim):
            if iProc[i] < nProcDir[i]-1:
                self.rbType[i] = 'INTF'
            if iProc[i] > 0:
                self.lbType[i] = 'INTF'

        self.issGL = [None]*self.dim
        self.issGR = [None]*self.dim
        self.nHaloL = [None]*self.dim
        self.nHaloR = [None]*self.dim

        self._msgDebug('coord={}, lRank={}, rRank={}'
                       .format(self.iProc, self.lRank, self.rRank))

        # Decompose indexes
        for i in range(self.dim):
            sSize = (self.p+1)//2

            # Consider left side
            if self.lbType[i] == 'WALL':
                issGL = 0
                nHaloL = 0
            elif self.lbType[i] == 'PER':
                issGL = 0
                nHaloL = sSize-1
            else:
                issGL = 0 if len(self.issG[i]) == 0 else \
                    np.argwhere(self.issG[i]+sSize > self.iGL[i]).min()
                nHaloL = sSize-1
            self.issGL[i] = issGL
            self.nHaloL[i] = nHaloL

            # Consider right side
            if self.rbType[i] == 'WALL':
                issGR = None
                nHaloR = 0
            elif self.rbType[i] == 'PER':
                issGR = None
                nHaloR = sSize
            else:
                issGR = None if len(self.issG[i]) == 0 else \
                    np.argwhere(self.issG[i]+sSize-2 < self.iGR[i]).max()
                nHaloR = sSize
            self.issGR[i] = issGR
            self.nHaloR[i] = nHaloR

            # Build local issG
            self.issG[i] = self.issG[i][issGL:issGR]
            if self.lbType[i] == 'INTF':
                self.issG[i] -= self.iGL[i]

            # Build local ci
            if i == 0 and not self.xUnif[i]:
                self.ci[i] = self.ci[i][:, issGL:issGR, :, :]
            if i == 1 and not self.xUnif[i]:
                self.ci[i] = self.ci[i][:, :, issGL:issGR, :]
            if i == 2 and not self.xUnif[i]:
                self.ci[i] = self.ci[i][:, :, :, issGL:issGR]

            # Build local icpG, icpF and incpF
            icpG = []
            icpF = []
            incpF = []
            xF = self.xF[i]
            xGLocMin = self.xGLoc[i][0]
            if self.rbType[i] == 'INTF':
                xGLocMax = self.xG[i][self.iGR[i]]
            else:
                xGLocMax = 1e23

            def isLocalG(l):
                return (l < self.iGR[i]) and not (l < self.iGL[i])

            for lG, lF in zip(self.icpG[i], self.icpF[i]):
                if isLocalG(lG):
                    icpG.append(lG)
                    icpF.append(lF)

            if self.lbType[i] != 'INTF' and self.rbType[i] != 'INTF':
                def isLocal(x):
                    return True
            elif self.rbType[i] != 'INTF':
                def isLocal(x):
                    return (x > xGLocMin)
            elif self.lbType[i] != 'INTF':
                def isLocal(x):
                    return (x < xGLocMax)
            else:
                def isLocal(x):
                    return (x > xGLocMin) and (x < xGLocMax)

            for l in range(self.nXF[i]):
                if l not in self.icpF[i] and isLocal(xF[l]):
                    incpF.append(l)

            self.icpG[i] = np.array(icpG, dtype=int)
            self.icpG[i] -= self.iGL[i]
            iFL = min(1e32 if len(icpF) == 0 else icpF[0],
                      1e32 if len(incpF) == 0 else incpF[0])
            self.icpF[i] = np.array(icpF, dtype=int)
            self.icpF[i] -= iFL
            self.incpF[i] = np.array(incpF, dtype=int)
            self.incpF[i] -= iFL

            assert len(self.issG[i]) == len(self.incpF[i])
            assert len(self.icpF[i]) == len(self.icpG[i])
            if self.issG[i].size > 0:
                assert max(self.issG[i]) < self.nXGLoc[i]

    def _updateHalo(self, i, dataG):
        if self.nProcDir[i] == 1:
            return self._updateHaloLoc(i, dataG)
        else:
            return self._updateHaloMPI(i, dataG)

    def _updateHaloLoc(self, i, dataG):
        if self.rbType[i] == 'WALL':
            return dataG
        else:
            nHL = self.nHaloL[i]
            nHR = self.nHaloR[i]
            if i == 0:
                return np.concatenate(
                    [dataG[-(nHR-1):, :, :], dataG, dataG[:nHL+1, :, :]],
                    axis=i)
            elif i == 1:
                return np.concatenate(
                    [dataG[:, -(nHR-1):, :], dataG, dataG[:, :nHL+1, :]],
                    axis=i)
            elif i == 2:
                return np.concatenate(
                    [dataG[:, :, -(nHR-1):], dataG, dataG[:, :, :nHL+1]],
                    axis=i)
            else:
                raise ValueError('Wrong direction {}'.format(i))

    def _updateHaloMPI(self, i, dataG):
        mpi = self.sComm
        nHL = self.nHaloL[i]
        nHR = self.nHaloR[i]
        nX, nY, nZ = dataG.shape
        localData = [dataG]

        if i == 0:  # x direction ---------------------------------------------
            # Sending part
            if self.rbType[i] in ['PER', 'INTF'] and (nHR-1 > 0):
                sHR = dataG[-(nHR-1):, :, :]
                self._msgDebug('x-send-R {}'.format(sHR.shape))
                mpi.Isend(sHR, dest=self.rRank[i], tag=12)

            if self.lbType[i] in ['PER', 'INTF']:
                sHL = dataG[:nHL+1, :, :]
                self._msgDebug('x-send-L {}'.format(sHL.shape))
                mpi.Isend(sHL, dest=self.lRank[i], tag=21)

            # Receiving part
            if self.lbType[i] in ['PER', 'INTF'] and (nHL > 0):
                rHL = np.empty((nHL, nY, nZ), dtype=dataG.dtype)
                mpi.Recv(rHL, source=self.lRank[i], tag=12)
                self._msgDebug('x-recv-R {}'.format(rHL.shape))
                localData = [rHL] + localData

            if self.rbType[i] in ['PER', 'INTF']:
                rHR = np.empty((nHR, nY, nZ), dtype=dataG.dtype)
                mpi.Recv(rHR, source=self.rRank[i], tag=21)
                self._msgDebug('x-recv-L {}'.format(rHR.shape))
                localData = localData + [rHR]

        elif i == 1:  # y direction -------------------------------------------
            # Sending part
            if self.rbType[i] in ['PER', 'INTF'] and (nHR-1 > 0):
                sHR = dataG[:, -(nHR-1):, :].copy()
                self._msgDebug('y-send-R {}'.format(sHR.shape))
                mpi.Isend(sHR, dest=self.rRank[i], tag=123)

            if self.lbType[i] in ['PER', 'INTF']:
                sHL = dataG[:, :nHL+1, :].copy()
                self._msgDebug('y-send-L {}'.format(sHL.shape))
                mpi.Isend(sHL, dest=self.lRank[i], tag=321)

            # Receiving part
            if self.lbType[i] in ['PER', 'INTF'] and (nHL > 0):
                rHL = np.empty((nX, nHL, nZ), dtype=dataG.dtype)
                mpi.Recv(rHL, source=self.lRank[i], tag=123)
                self._msgDebug('y-recv-L {}'.format(rHL.shape))
                localData = [rHL] + localData

            if self.rbType[i] in ['PER', 'INTF']:
                rHR = np.empty((nX, nHR, nZ), dtype=dataG.dtype)
                mpi.Recv(rHR, source=self.rRank[i], tag=321)
                self._msgDebug('y-recv-R {}'.format(rHR.shape))
                localData = localData + [rHR]

        elif i == 2:  # z direction -------------------------------------------
            # Sending part
            if self.rbType[i] in ['PER', 'INTF'] and (nHR-1 > 0):
                sHR = dataG[:, :, -(nHR-1):].copy()
                self._msgDebug('z-send-R({}) {}'
                               .format(self.rRank[i], sHR.shape))
                mpi.Isend(sHR, dest=self.rRank[i], tag=1234)

            if self.lbType[i] in ['PER', 'INTF']:
                sHL = dataG[:, :, :nHL+1].copy()
                self._msgDebug('z-send-L({}) {}'
                               .format(self.lRank[i], sHL.shape))
                mpi.Isend(sHL, dest=self.lRank[i], tag=4321)

            # Receiving part
            if self.lbType[i] in ['PER', 'INTF'] and (nHL > 0):
                rHL = np.empty((nX, nY, nHL), dtype=dataG.dtype)
                mpi.Recv(rHL, source=self.lRank[i], tag=1234)
                self._msgDebug('z-recv-L {}'.format(rHL.shape))
                localData = [rHL] + localData

            if self.rbType[i] in ['PER', 'INTF']:
                rHR = np.empty((nX, nY, nHR), dtype=dataG.dtype)
                mpi.Recv(rHR, source=self.rRank[i], tag=4321)
                self._msgDebug('z-recv-R {}'.format(rHR.shape))
                localData = localData + [rHR]

        else:  # --------------------------------------------------------------
            raise ValueError('Wrong direction {}'.format(i))

        # Synchronize all processes -- DO NOT REMOVE !!!
        mpi.Barrier()

        return np.concatenate(localData, axis=i)

    def _scatterDataG(self, dataGGlob):
        mpi = self.sComm
        dataGLoc = None

        iProcAll = np.tile([0, 0, 0], (self.MPI_SIZE, 1))
        mpi.Allgather(np.array(self.iProc, dtype=int), iProcAll)

        nXGLocAll = np.zeros_like(iProcAll)
        nXLoc = np.array(self.nXGLoc, dtype=int)
        for i in range(self.dim):
            if self.lbType[i] == 'WALL' and self.useWallL[i]:
                nXLoc[i] -= 1
            if self.rbType[i] == 'WALL' and self.useWallR[i]:
                nXLoc[i] -= 1
        mpi.Allgather(nXLoc, nXGLocAll)

        nXProc = [None]*self.dim
        iGLProc = [None]*self.dim
        for i in range(self.dim):
            nXProc[i] = []
            for l in range(self.nProcDir[i]):
                for k, n in zip(iProcAll, nXGLocAll):
                    if k[i] == l:
                        nXProc[i].append(n[i])
                        break
            iGLProc[i] = [0] + np.cumsum(nXProc[i][:-1]).tolist()

        self.iProcAll = iProcAll

        if self.MPI_RANK == 0:
            lDataG = []
            for iProc in iProcAll:
                lDataG.append(
                    dataGGlob[iGLProc[0][iProc[0]]:iGLProc[0][iProc[0]] +
                              nXProc[0][iProc[0]],
                              iGLProc[1][iProc[1]]:iGLProc[1][iProc[1]] +
                              nXProc[1][iProc[1]],
                              iGLProc[2][iProc[2]]:iGLProc[2][iProc[2]] +
                              nXProc[2][iProc[2]]].ravel())
            dataGGlob = np.concatenate(lDataG)

        splitSize = []
        for iProc in iProcAll:
            nPLoc = 1
            for i in range(self.dim):
                nPLoc *= nXProc[i][iProc[i]]
            splitSize.append(nPLoc)
        dispInput = [0] + np.cumsum(splitSize[:-1]).tolist()

        dataGLoc = np.empty(np.prod(nXLoc))

        mpi.Scatterv([dataGGlob, splitSize, dispInput, MPI.DOUBLE], dataGLoc)

        return dataGLoc.reshape(nXLoc)

    def _gatherDataF(self, dataFLoc):
        mpi = self.sComm
        dataFGlob = None

        iProcAll = self.iProcAll
        nXFLocAll = np.zeros_like(iProcAll)
        mpi.Allgather(np.array(self.nXFLoc, dtype=int), nXFLocAll)

        nXProc = [None]*self.dim
        iFLProc = [None]*self.dim
        for i in range(self.dim):
            nXProc[i] = []
            for l in range(self.nProcDir[i]):
                for k, n in zip(iProcAll, nXFLocAll):
                    if k[i] == l:
                        nXProc[i].append(n[i])
                        break
            iFLProc[i] = [0] + np.cumsum(nXProc[i][:-1]).tolist()

        if self.MPI_RANK == 0:
            dataFGlob = np.empty(self.nXF)
            dataFGlobS = np.empty(np.prod(self.nXF))
        else:
            dataFGlobS = None

        splitSize = []
        for iProc in iProcAll:
            nPLoc = 1
            for i in range(self.dim):
                nPLoc *= nXProc[i][iProc[i]]
            splitSize.append(nPLoc)
        dispInput = [0] + np.cumsum(splitSize[:-1]).tolist()

        dataFLoc = dataFLoc.ravel()
        mpi.Gatherv(dataFLoc, [dataFGlobS, splitSize, dispInput, MPI.DOUBLE])

        if self.MPI_RANK == 0:
            for iProc, iL, nP in zip(iProcAll, dispInput, splitSize):
                np.copyto(
                    dataFGlob[iFLProc[0][iProc[0]]:iFLProc[0][iProc[0]] +
                              nXProc[0][iProc[0]],
                              iFLProc[1][iProc[1]]:iFLProc[1][iProc[1]] +
                              nXProc[1][iProc[1]],
                              iFLProc[2][iProc[2]]:iFLProc[2][iProc[2]] +
                              nXProc[2][iProc[2]]],
                    dataFGlobS[iL:iL+nP].reshape((nXProc[0][iProc[0]],
                                                  nXProc[1][iProc[1]],
                                                  nXProc[2][iProc[2]])))

        return dataFGlob

    def waitPreviousMPIGroup(self):
        mpi = self.tComm
        go = np.array(True)
        if self.MPI_NGROUP == 1:
            return
        elif self.MPI_GROUP == 0:
            return
        elif self.MPI_GROUP < self.MPI_NGROUP-1:
            mpi.Recv(go, source=self.MPI_GROUP-1, tag=55)
        else:
            mpi.Recv(go, source=self.MPI_GROUP-1, tag=55)

    def goNextMPIGroup(self):
        mpi = self.tComm
        go = np.array(True)
        if self.MPI_NGROUP == 1:
            return
        elif self.MPI_GROUP == 0:
            mpi.Isend(go, dest=self.MPI_GROUP+1, tag=55)
        elif self.MPI_GROUP < self.MPI_NGROUP-1:
            mpi.Isend(go, dest=self.MPI_GROUP+1, tag=55)
        else:
            return

    def indexFieldGroupOK(self, i):
        if self.MPI_NGROUP == 1:
            return True
        else:
            return i == self.MPI_GROUP

    def waitAllMPI(self):
        self.gComm.Barrier()

    def interpolate(self, dataG, xWallCond='DIRICHLET', yWallCond='DIRICHLET',
                    zWallCond='DIRICHLET', xWallVal=0, yWallVal=0, zWallVal=0):
        """DOCTODO"""
        dataF = self._gatherDataF(
            self._interpolate(self._scatterDataG(dataG),
                              xWallCond, yWallCond, zWallCond,
                              xWallVal, yWallVal, zWallVal))
        return dataF

    def _interpolate(self, dataG, xWallCond='DIRICHLET', yWallCond='DIRICHLET',
                     zWallCond='DIRICHLET', xWallVal=0, yWallVal=0,
                     zWallVal=0):

        # ---- Modify coarse data to handle wall boundary conditions ----

        # x direction -----------------------------------------------
        xG = self.xGLoc[0]
        # Left side
        if self.lbType[0] == 'WALL' and self.useWallL[0]:
            wallPlane = np.empty((1, dataG.shape[1], dataG.shape[2]))
            self._setWallPlane(
                wallPlane, xWallCond, xWallVal,
                dataG[0:1, :, :], dataG[1:2, :, :],
                xG[1]-xG[0], xG[2]-xG[1])
            dataG = np.concatenate([wallPlane, dataG], axis=0)
        # Right side
        if self.rbType[0] == 'WALL' and self.useWallR[0]:
            wallPlane = np.empty((1, dataG.shape[1], dataG.shape[2]))
            self._setWallPlane(
                wallPlane, xWallCond, xWallVal,
                dataG[-1:, :, :], dataG[-2:-1, :, :],
                xG[-1]-xG[-2], xG[-2]-xG[-3])
            dataG = np.concatenate([dataG, wallPlane], axis=0)

        # y direction -----------------------------------------------
        xG = self.xGLoc[1]
        # Left side
        if self.lbType[1] == 'WALL' and self.useWallL[1]:
            wallPlane = np.empty((dataG.shape[0], 1, dataG.shape[2]))
            self._setWallPlane(
                wallPlane, yWallCond, yWallVal,
                dataG[:, 0:1, :], dataG[:, 1:2, :],
                xG[1]-xG[0], xG[2]-xG[1])
            dataG = np.concatenate([wallPlane, dataG], axis=1)
        # Right side
        if self.rbType[1] == 'WALL' and self.useWallR[1]:
            wallPlane = np.empty((dataG.shape[0], 1, dataG.shape[2]))
            self._setWallPlane(
                wallPlane, yWallCond, yWallVal,
                dataG[:, -1:, :], dataG[:, -2:-1, :],
                xG[-1]-xG[-2], xG[-2]-xG[-3])
            dataG = np.concatenate([dataG, wallPlane], axis=1)

        # z direction -----------------------------------------------
        xG = self.xGLoc[2]
        # Left side
        if self.lbType[2] == 'WALL' and self.useWallL[2]:
            wallPlane = np.empty((dataG.shape[0], dataG.shape[1], 1))
            self._setWallPlane(
                wallPlane, zWallCond, zWallVal,
                dataG[:, :, 0:1], dataG[:, :, 1:2],
                xG[1]-xG[0], xG[2]-xG[1])
            dataG = np.concatenate([wallPlane, dataG], axis=2)
        # Right side
        if self.rbType[2] == 'WALL' and self.useWallR[2]:
            wallPlane = np.empty((dataG.shape[0], dataG.shape[1], 1))
            self._setWallPlane(
                wallPlane, zWallCond, zWallVal,
                dataG[:, :, -1:], dataG[:, :, -2:-1],
                xG[-1]-xG[-2], xG[-2]-xG[-3])
            dataG = np.concatenate([dataG, wallPlane], axis=2)

        # ---- Data pointers ----
        nXF = self.nXFLoc
        nXG = self.nXGLoc

        # ---- Vertex interpolation ----

        # Create data arrays
        dataI_X = np.zeros((nXF[0], nXG[1], nXG[2]))
        dataI_Y = np.zeros((nXG[0], nXF[1], nXG[2]))
        dataI_Z = np.zeros((nXG[0], nXG[1], nXF[2]))

        self._msgDebug('dataG {}'.format(dataG.shape))

        # -- Interpolation of dataG along X axis
        self._interpolateX(dataG, dataI_X)

        # -- Interpolation of dataG along Y axis
        self._interpolateY(dataG, dataI_Y)

        # -- Interpolation of dataG along Z axis
        self._interpolateZ(dataG, dataI_Z)

        # ---- Face interpolation ----

        # Create data arrays
        dataI_XY = np.zeros((nXF[0], nXF[1], nXG[2]))
        dataI_YZ = np.zeros((nXG[0], nXF[1], nXF[2]))
        dataI_XZ = np.zeros((nXF[0], nXG[1], nXF[2]))

        # -- dataI_XY: interpolation of dataI_Y along X axis
        self._interpolateX(dataI_Y, dataI_XY)

        # -- dataI_XY: interpolation of dataI_X along Y axis
        self._interpolateY(dataI_X, dataI_XY)

        # -- dataI_XY: average in the two directions
        dataI_XY /= 2.

        # -- dataI_YZ: interpolation of dataI_Z along Y axis
        self._interpolateY(dataI_Z, dataI_YZ)

        # -- dataI_YZ: interpolation of dataI_Y along Z axis
        self._interpolateZ(dataI_Y, dataI_YZ)

        # -- data_YZ: average in the two directions
        dataI_YZ /= 2.

        # -- data_XZ: interpolation of dataI_Z along X axis
        self._interpolateX(dataI_Z, dataI_XZ)

        # -- data_XZ: interpolation of dataI_X along Z axis
        self._interpolateZ(dataI_X, dataI_XZ)

        # -- dataXZ: average in the two directions
        dataI_XZ /= 2.

        # ---- Volume interpolation ----

        # Create data arrays
        dataF = np.zeros((nXF[0], nXF[1], nXF[2]))

        # -- dataF: interpolation of dataI_YZ in X direction
        self._interpolateX(dataI_YZ, dataF)

        # -- dataF: interpolation of dataI_XZ in Y direction
        self._interpolateY(dataI_XZ, dataF)

        # -- dataF: interpolation of dataI_XY in Z direction
        self._interpolateZ(dataI_XY, dataF)

        # -- dataF: average in the two directions
        dataF /= 3.

        # ---- Cleaning ----
        del dataI_X, dataI_Y, dataI_Z, dataI_XY, dataI_YZ, dataI_XZ

        return dataF

    # Functions for interpolation along X axis
    def _interpolateX(self, dataG, dataI):
        self._msgDebug('x-interpol_base')
        # Common points
        dataI[self.icpF[0], :, :] += dataG[self.icpG[0], :, :]
        # Non-common points
        ci = self.ci[0]
        issG = self.issG[0].copy()
        # Concatenate lHalo + dataG + rHalo
        dataGNew = self._updateHalo(0, dataG)
        issG += self.nHaloL[0]
        for i in range(self.p+1):
            if i > 0:
                issG += 1
            data_temp = dataGNew[issG, :, :].copy()
            data_temp *= ci[i, :, :, :]
            dataI[self.incpF[0], :, :] += data_temp

    # Function for interpolation along Y axis
    def _interpolateY(self, dataG, dataI):
        self._msgDebug('y-interpol_base')
        # Common points
        dataI[:, self.icpF[1], :] += dataG[:, self.icpG[1], :]
        # Non-common points
        ci = self.ci[1]
        issG = self.issG[1].copy()
        # Concatenate lHalo + dataG + rHalo
        dataGNew = self._updateHalo(1, dataG)
        issG += self.nHaloL[1]
        for i in range(self.p+1):
            if i > 0:
                issG += 1
            data_temp = dataGNew[:, issG, :].copy()
            data_temp *= ci[i, :, :, :]
            dataI[:, self.incpF[1], :] += data_temp

    # Function for interpolation along Z axis
    def _interpolateZ(self, dataG, dataI):
        self._msgDebug('z-interpol_base')
        # Common points
        dataI[:, :, self.icpF[2]] += dataG[:, :, self.icpG[2]]
        # Non-common points
        ci = self.ci[2]
        issG = self.issG[2].copy()
        # Concatenate lHalo + dataG + rHalo
        dataGNew = self._updateHalo(2, dataG)
        issG += self.nHaloL[2]
        for i in range(self.p+1):
            if i > 0:
                issG += 1
            data_temp = dataGNew[:, :, issG]
            data_temp *= ci[i, :, :, :]
            dataI[:, :, self.incpF[2]] += data_temp

    # Function for boundary condition values
    def _setWallPlane(self, wallPlane, wallCond, wallVal, v0, v1, h0, h1):
        if wallCond == 'DIRICHLET':
            np.copyto(wallPlane, wallVal)
        elif wallCond == 'NEUMANN':
            np.copyto(wallPlane, v0)
            if self.USE_NEUMANN_O2:
                wallPlane += h0**2/(2*h1*h0+h1**2)*(v0-v1)
        else:
            raise ValueError('Boundary condition () not implemented'
                             .format(wallCond))

    # Function for boundary condition checking
    def _checkBnd(self, i, *args):
        condLeft = 0
        for bType in args:
            condLeft += (self.lbType[i] == bType)
        condRight = 0
        for bType in args:
            condRight += (self.rbType[i] == bType)
        return condLeft*condRight

    # Optimization of type A
    def _checkOptA(self):
        # Checking x dir.
        icpG, icpF = self.icpG[0], self.icpF[0]
        if self._checkBnd(0, 'PER', 'INTF'):
            if np.all(icpG*2 == icpF) and len(icpG) > 0:
                self._interpolateX = self._interpolateX_OptA
                self.msg('x dir.: using optim of type A', 0)
        # Checking y dir.
        icpG, icpF = self.icpG[1], self.icpF[1]
        if self._checkBnd(1, 'PER', 'INTF'):
            if np.all(icpG*2 == icpF) and len(icpG) > 0:
                self._interpolateY = self._interpolateY_OptA
                self.msg('y dir.: using optim of type A', 0)
        # Checking z dir.
        icpG, icpF = self.icpG[2], self.icpF[2]
        if self._checkBnd(2, 'PER', 'INTF'):
            if np.all(icpG*2 == icpF) and len(icpG) > 0:
                self._interpolateZ = self._interpolateZ_OptA
                self.msg('z dir.: using optim of type A', 0)

    def _interpolateX_OptA(self, dataG, dataI):
        # Common points
        dataI[::2, :, :] += dataG[:, :, :]
        data_temp = np.empty_like(dataG)
        # Non-common points
        if len(self.issG[0] > 0):
            ci = self.ci[0]
            issG0 = self.issG[0][0]
            nXG = self.nXGLoc[0]
            # Concatenate lHalo + dataG + rHalo
            dataGNew = self._updateHalo(0, dataG)
            issG0 += self.nHaloL[0]
            for i in range(self.p+1):
                np.copyto(data_temp, dataGNew[issG0+i:issG0+i+nXG, :, :])
                data_temp *= ci[i, :, :, :]
                dataI[1::2, :, :] += data_temp

    def _interpolateY_OptA(self, dataG, dataI):
        # Common points
        dataI[:, ::2, :] += dataG[:, :, :]
        data_temp = np.empty_like(dataG)
        # Non-common points
        if len(self.issG[1] > 0):
            ci = self.ci[1]
            issG0 = self.issG[1][0]
            nXG = self.nXGLoc[1]
            # Concatenate lHalo + dataG + rHalo
            dataGNew = self._updateHalo(1, dataG)
            issG0 += self.nHaloL[1]
            for i in range(self.p+1):
                np.copyto(data_temp, dataGNew[:, issG0+i:issG0+i+nXG, :])
                data_temp *= ci[i, :, :, :]
                dataI[:, 1::2, :] += data_temp

    def _interpolateZ_OptA(self, dataG, dataI):
        # Common points
        dataI[:, :, ::2] += dataG[:, :, :]
        data_temp = np.empty_like(dataG)
        # Non-common points
        if len(self.issG[2] > 0):
            ci = self.ci[2]
            issG0 = self.issG[2][0]
            nXG = self.nXGLoc[2]
            # Concatenate lHalo + dataG + rHalo
            dataGNew = self._updateHalo(2, dataG)
            issG0 += self.nHaloL[2]
            for i in range(self.p+1):
                np.copyto(data_temp, dataGNew[:, :, issG0+i:issG0+i+nXG])
                data_temp *= ci[i, :, :, :]
                dataI[:, :, 1::2] += data_temp

    # Optimization of type B
    def _checkOptB(self):
        # Checking x dir.
        icpG, icpF = self.icpG[0], self.icpF[0]
        if self._checkBnd(0, 'WALL') and self.useWallL[0]*self.useWallR[0]:
            if np.all((icpF+1)//2 == icpG) and len(icpG) > 0:
                self._interpolateX = self._interpolateX_OptB
                self.msg('x dir.: using optim of type B', 0)
        # Checking y dir.
        icpG, icpF = self.icpG[1], self.icpF[1]
        if self._checkBnd(1, 'WALL') and self.useWallL[1]*self.useWallR[1]:
            if np.all((icpF+1)//2 == icpG) and len(icpG) > 0:
                self._interpolateY = self._interpolateY_OptB
                self.msg('y dir.: using optim of type B', 0)
        # Checking z dir.
        icpG, icpF = self.icpG[2], self.icpF[2]
        if self._checkBnd(2, 'WALL') and self.useWallL[2]*self.useWallR[2]:
            if np.all((icpF+1)//2 == icpG) and len(icpG) > 0:
                self._interpolateZ = self._interpolateZ_OptB
                self.msg('z dir.: using optim of type B', 0)

    def _interpolateX_OptB(self, dataG, dataI):
        # Common points
        dataI[1::2, :, :] += dataG[1:-1, :, :]
        # Non-common points
        ci = self.ci[0]
        issG = self.issG[0].copy()
        for i in range(self.p+1):
            if i > 0:
                issG += 1
            data_temp = dataG[issG, :, :]
            data_temp *= ci[i, :, :, :]
            dataI[::2, :, :] += data_temp

    def _interpolateY_OptB(self, dataG, dataI):
        # Common points
        dataI[:, 1::2, :] += dataG[:, 1:-1, :]
        # Non-common points
        ci = self.ci[1]
        issG = self.issG[1].copy()
        for i in range(self.p+1):
            if i > 0:
                issG += 1
            data_temp = dataG[:, issG, :]
            data_temp *= ci[i, :, :, :]
            dataI[:, ::2, :] += data_temp

    def _interpolateZ_OptB(self, dataG, dataI):
        # Common points
        dataI[:, :, 1::2] += dataG[:, :, 1:-1]
        # Non-common points
        ci = self.ci[2]
        issG = self.issG[2].copy()
        for i in range(self.p+1):
            if i > 0:
                issG += 1
            data_temp = dataG[:, :, issG]
            data_temp *= ci[i, :, :, :]
            dataI[:, :, ::2] += data_temp


class HighOrderStructGridInterpolator1D(HighOrderStructGridInterpolator):

    def __init__(self, p, xG, xF, boundary='PER', xL=None, xR=None):
        super().__init__(p, xG=xG, yG=[0], zG=[0], xF=xF, xBoundary=boundary,
                         xL=xL, xR=xR, keepY=True, keepZ=True)

    def interpolate(self, dataG, wallCond='DIRICHLET', wallVal=0):
        # Reshape dataG
        dataG = np.asarray(dataG).ravel()[:, np.newaxis, np.newaxis]
        # Call 3D interpolation method, and reshape output data
        return super().interpolate(
            dataG, xWallCond=wallCond, xWallVal=wallVal)[:, 0, 0]
