subroutine tridiag_rpkw(nodes, weights, alpha, beta, n)
!    The script is adapted from the routine RKPW in
!    W.B. Gragg and W.J. Harrod, ``The numerically stable
!    reconstruction of Jacobi matrices from spectral data'',
!    Numer. Math. 44 (1984), 317-335.

   implicit none

   ! Argument variables
   integer, intent(in) :: n
   double precision, intent(in) :: nodes(n), weights(n)
   double precision, intent(inout) :: alpha(n), beta(n)

   ! Auxilliary variables
   double precision :: pi2, lam, gamma2, sigma2, tau, rho2
   double precision :: oldBeta, oldSigma2, newTau

   ! Loop indexes
   integer :: i, k

   ! Intialize alpha, beta
   alpha(:) = nodes(:)
   beta(:) = 0.
   beta(1) = weights(1)

   ! Loop on each submatrices of the arrow matrix
   do i=1,n-1

      pi2 = weights(i+1)
      lam = nodes(i+1)

      gamma2 = 1.
      sigma2 = 1.
      tau = 0.

      ! Apply rotations successively to tridiagonalize the submatrix
      do k=1,i+1

         oldBeta = beta(k)
         rho2 = oldBeta + pi2
         beta(k) = gamma2*rho2
         oldSigma2 = sigma2

         if (rho2 <= 0) then
            gamma2 = 1.
            sigma2 = 0.
         else
            gamma2 = oldBeta/rho2
            sigma2 = pi2/rho2
         end if

         newTau = sigma2*(alpha(k)-lam) - gamma2*tau
         alpha(k) = alpha(k) - (newTau-tau)
         tau = newTau

         if (sigma2 <= 0) then
            pi2 = oldSigma2*beta(k)
         else
            pi2 = tau**2/sigma2
         end if

      end do

   end do

end subroutine


subroutine tridiag_rpkw_qp(nodes_dp, weights_dp, alpha_dp, beta_dp, n)
   implicit none

   ! Argument variables
   integer, intent(in) :: n
   real(kind=8), intent(in) :: nodes_dp(n), weights_dp(n)
   real(kind=8), intent(inout) :: alpha_dp(n), beta_dp(n)

   ! Quadruple precision variables
   real(kind=16) :: nodes(n), weights(n)
   real(kind=16) :: alpha(n), beta(n)

   ! Auxilliary variables
   real(kind=16) :: pi2, lam, gamma2, sigma2, tau, rho2
   real(kind=16) :: oldBeta, oldSigma2, newTau

   ! Loop indexes
   integer :: i, k

   ! Casting to quadruple precision
   do i=1,n
      nodes(i) = nodes_dp(i)
      weights(i) = weights_dp(i)
   end do

   ! Intialize alpha, beta
   do i=1,n
      alpha(i) = nodes(i)
      beta(i) = 0.
   end do
   beta(1) = weights(1)

   ! Loop on each submatrices of the arrow matrix
   do i=1,n-1

      pi2 = weights(i+1)
      lam = nodes(i+1)

      gamma2 = 1.
      sigma2 = 1.
      tau = 0.

      ! Apply rotations successively to tridiagonalize the submatrix
      do k=1,i+1

         oldBeta = beta(k)
         rho2 = oldBeta + pi2
         beta(k) = gamma2*rho2
         oldSigma2 = sigma2

         if (rho2 <= 0.) then
            gamma2 = 1.
            sigma2 = 0.
         else
            gamma2 = oldBeta/rho2
            sigma2 = pi2/rho2
         end if

         newTau = sigma2*(alpha(k)-lam) - gamma2*tau
         alpha(k) = alpha(k) - (newTau-tau)
         tau = newTau

         if (sigma2 <= 0.) then
            pi2 = oldSigma2*beta(k)
         else
            pi2 = tau**2/sigma2
         end if

      end do

   end do

   ! Casting back to double precision
   do i=1,n
      alpha_dp(i) = alpha(i)
      beta_dp(i) = beta(i)
   end do

end subroutine


subroutine golub_welsch(alpha, beta, nodes, weights, eps, n)
   implicit none

   ! Argument variables
   integer, intent(in) :: n
   double precision, intent(in) :: alpha(n), beta(n)
   double precision, intent(inout) :: nodes(n), weights(n)
   double precision, intent(in) :: eps

   ! Auxilliary variables
   double precision :: a(n), b(n-1), srJ(n)
   double precision :: beta0, normJ, epsJ, lam, lam1, lam2, rho
   double precision :: b2, det, aTemp, eigMax
   double precision :: cj, bk1, cost, sint, r, aj, f, q, wj

   ! Function
   ! double precision :: hypoth

   ! Loop indexes
   integer :: i, m, m1, k, j, nLoop

   ! Initialize copy of alpha, sqrt(beta), that will be modified during computation
   do i=1,n-1
      a(i) = alpha(i)
      b(i) = sqrt(beta(i+1))
   enddo
   a(n) = alpha(n)
   beta0 = beta(1)

   ! Determine error tolerance relative to Jacobian
   srJ(1) = abs(alpha(1)) + b(1)
   do i=2,n-1
      srJ(i) = b(i-1) + abs(alpha(i)) + b(i)
   enddo
   srJ(n) = b(n-1) + abs(alpha(n))
   normJ = maxval(srJ)
   epsJ = eps*normJ
   lam = normJ; lam1 = normJ; lam2 = normJ; rho = normJ

   ! Initialize first weigth
   weights(1) = 1.

   ! Run algorithm, with m starting from last index
   m = n
   m1 = m-1
   i = m1; k=1
   nLoop = 0
   do while (.true.)

      nLoop = nLoop + 1

      if (nLoop > 1e6) then
         print *, 'ERROR - more than 1e6 loop done, exiting'
         exit
      endif

      ! First index reached and converged => END
      if (m == 0) then
         do i=1,n
            ! Normalize weights
            weights(i) = beta0 * weights(i)**2
         enddo
            ! TODO : sort indexes
         exit
      endif

      if (m1 >= 1) then  ! First index not reached yet
         if (abs(b(m1)) <= epsJ) then  ! Precision reached
            ! Update values
            nodes(m) = a(m)
            rho = min(lam1, lam2)
            ! Update indexes
            m = m1
            m1 = m-1
            i = m1
            cycle
         endif
      else
         ! First index was reached
         nodes(1) = a(1)
         m = 0
         cycle
      endif

      ! Small off diagonal element means the matrix can be split
      if (abs(b(i)) <= epsJ) then
         k = i
      else
         k = 1
      endif

      ! Find the shift with the eigenvalues of lower 2x2 block
      b2 = b(m1)**2
      det = sqrt((a(m1) - a(m))**2 + 4*b2)
      ! det = hypoth(a(m1) - a(m), 2*b(m1))
      aTemp = a(m1) + a(m)
      if (aTemp > 0) then
         lam2 = (aTemp + det)/2
      else
         lam2 = (aTemp - det)/2
      endif
      lam1 = (a(m1)*a(m) - b2)/lam2
      eigMax = max(lam1, lam2)
      if (abs(eigMax - rho) <= abs(eigMax)/8) then
         lam = eigMax
      endif
      rho = eigMax

      ! Transform block k to m
      cj = b(k)
      bk1 = a(k) - lam
      do j=k,m1
         ! Compute and apply rotations, cf (3.3) in Golub&Welsch-1969
         r = sqrt(cj**2 + bk1**2)
         ! r = hypoth(cj, bk1)
         sint = cj/r
         cost = bk1/r
         aj = a(j)
         ! print *, r, sint, cost

         if (j > 1) then
            b(j-1) = r
         endif
         if (j < n-1) then
            cj = b(j+1)*sint
            b(j+1) = -cost*b(j+1)
         endif

         f = aj*cost + b(j)*sint
         q = b(j)*cost + a(j+1)*sint

         a(j) = f*cost + q*sint
         b(j) = f*sint - q*cost

         wj = weights(j)
         a(j+1) = aj + a(j+1) - a(j)

         weights(j) = wj*cost + weights(j+1)*sint
         weights(j+1) = wj*sint - weights(j+1)*cost

         bk1 = b(j)

      enddo

   enddo

end subroutine


double precision function hypoth(a, b)

   implicit none

   double precision, intent(in) :: a, b
   double precision :: x, y, z, r

   x = abs(a)
   y = abs(b)
   z = max(x, y)
   r = min(x, y) / z

   hypoth = z * sqrt(1. + r*r)

end function hypoth
