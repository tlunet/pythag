#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 16:37:00 2018

@author: telu
"""
import numpy as np


def quadRuleMatrix(nodes, weights):
    nodes = np.array(nodes).ravel()
    N = nodes.size
    weights = np.array(weights).reshape(N, 1)
    weights **= 0.5

    A = np.vstack((
        np.hstack(([[1]], weights.T)), np.hstack((weights, np.diag(nodes)))))

    return A


def quadRuleMatrixFunc(nodes, weights):
    nodes = np.array(nodes).ravel()
    weights = np.array(weights).ravel()
    weights **= 0.5

    def func(x, out=None):
        x = np.asarray(x)
        x0 = x[0]
        x1N = x[1:]
        if out is None:
            out = np.empty_like(x, dtype=nodes.dtype)
        out[0] = x0 + weights.dot(x1N)
        out[1:] = nodes
        out[1:] *= x1N
        out[1:] += x0*weights
        return out

    func.N = nodes.size + 1

    return func


def tridiagLanczos(A, initVector=None):

    # Build the function evaluating A, depending on the argument form
    if callable(A):
        if hasattr(A, 'N'):
            def evalA(x, out):
                return A(x, out=out)
            N = A.N
        else:
            raise ValueError('Wrong argument for A')
    elif isinstance(A, np.ndarray):
        def evalA(x, out):
            return A.dot(x, out=out)
        N = A.shape[0]
    else:
        raise ValueError('Wrong argument for A')

    # Build the initial vector r0
    if initVector is None:
        r0 = np.zeros(N, dtype=np.float64)
        r0[0] = 1
    else:
        r0 = np.asarray(initVector).ravel()
        if not r0.size == N:
            raise ValueError('Wrong argument for initVector')
    beta0 = np.linalg.norm(r0)

    # Initialize output variables
    Q = np.zeros((N, N), dtype=r0.dtype)
    alpha = np.zeros(N, dtype=r0.dtype)
    beta = np.zeros(N, dtype=r0.dtype)

    # Initialize loop variables
    rjm1 = r0
    qjm1 = np.zeros_like(r0)
    bjm1 = beta0
    qj = np.empty_like(r0)
    rj = np.empty_like(r0)
    uj = np.empty_like(r0)

    # Algorithm require storage of N*N + 7*N float64

    # Loop for Lanczos algorithm
    for j in range(N):
        # Stage 1 - compute orthogonal base vector
        qj[:] = rjm1
        qj /= bjm1
        Q[:, j] = qj
        # Stage 2 - evaluate A operator
        evalA(qj, uj)
        # Stage 3 - compute new r_{j} = u_{j} - q_{j-1}*beta_{j-1}
        rj[:] = qjm1
        rj[:] *= -bjm1
        rj[:] += uj
        # Stage 4 - compute alpha
        aj = qj.dot(rj)
        alpha[j] = aj
        # Stage 5 - save qj value for next iteration
        np.copyto(qjm1, qj)
        # Stage 6 - update r
        qj *= aj
        rj -= qj
        # Stage 7 - compute beta
        bj = np.linalg.norm(rj)
        beta[j] = bj
        # Stage 8 - update indexes
        rjm1[:] = rj
        bjm1 = bj

    beta **= 2

    return Q, alpha[1:], beta[:-1]


def tridiagRPKW(nodes, weights, implementation='FORTRAN',
                fortran_quad_precision=False):
    """
    Compute alpha/beta coefficients with
    the Rutishauser-Kahan-Pal-Walker algorithm
    """

    nodes = np.asarray(nodes)
    weights = np.asarray(weights)

    n = nodes.size

    alpha = np.empty(n)
    beta = np.empty(n)

    if implementation == 'FORTRAN':

        from . import libfor

        if fortran_quad_precision:
            libfor.tridiag_rpkw_qp(nodes, weights, alpha, beta, n)
        else:
            libfor.tridiag_rpkw(nodes, weights, alpha, beta, n)

        return alpha, beta

    elif implementation == 'PYTHON':

        alpha[:] = nodes
        beta[:] = 0
        beta[0] = weights[0]

        for i in range(n-1):

            pi2 = weights[i+1]
            lam = nodes[i+1]

            gamma2 = 1.
            sigma2 = 0.
            tau = 0.

            for k in range(i+2):

                oldBeta = beta[k]
                rho2 = oldBeta + pi2
                beta[k] = gamma2*rho2
                oldSigma2 = sigma2

                if rho2 == 0:
                    gamma2 = 1.
                    sigma2 = 0.
                else:
                    gamma2 = oldBeta/rho2
                    sigma2 = pi2/rho2

                newTau = sigma2*(alpha[k]-lam) - gamma2*tau
                alpha[k] = alpha[k] - (newTau-tau)
                tau = newTau

                if sigma2 == 0:
                    pi2 = oldSigma2*beta[k]
                else:
                    pi2 = tau**2/sigma2

        return alpha, beta
    else:
        raise ValueError('No {} implementation for tridiagRPKW'
                         .format(implementation))


def tridiagStieltjes(nodes, weights, nCoeffMax=None):
    nQuad = len(nodes)
    if nCoeffMax is None:
        N = nQuad
    else:
        N = nCoeffMax
    alpha = np.zeros(N)
    beta = np.zeros(N)

    p0, p1, p2 = np.zeros(nQuad), np.zeros(nQuad), np.ones(nQuad)

    s0 = np.sum(weights)
    alpha[0] = np.sum(nodes*weights)/s0
    beta[0] = s0

    for k in range(N-1):
        p0[:] = p1
        p1[:] = p2
        p2[:] = (nodes - alpha[k])*p1 - beta[k]*p0
        s1 = np.sum(weights*(p2**2))
        s2 = np.sum(nodes*weights*(p2**2))
        alpha[k+1] = s2/s1
        beta[k+1] = s1/s0
        s0 = s1

    return alpha, beta


def tridiagGivens(A):
    if not isinstance(A, np.ndarray):
        raise ValueError('A should be a matrice')
    A = A.copy()
    N = A.shape[0]
    R = np.eye(N, dtype=np.float)
    Q = R.copy()

    def rotate(a, b, i, j):
        sqrt = np.sqrt(a**2+b**2)
        c = a/sqrt
        s = b/sqrt
        R[i, i] = c
        R[i, j] = s
        R[j, i] = -s
        R[j, j] = c

    def invRotate(i, j):
        R[i, i] = 1
        R[i, j] = 0
        R[j, i] = 0
        R[j, j] = 1

    def applyRotation():
        np.dot(Q, R, out=Q)
        np.dot(R, A, out=A)
        np.dot(A, R.T, out=A)

    for i in range(2, N):
        rotate(A[1, 0], A[i, 0], 1, i)
        applyRotation()
        invRotate(1, i)
        for j in range(2, i):
            rotate(A[j, j-1], A[i, j-1], j, i)
            applyRotation()
            invRotate(j, i)

    alpha = np.diag(A)[1:]
    beta = np.diag(A, k=1)**2
    return Q, alpha, beta
