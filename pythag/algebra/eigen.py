#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 11:43:46 2018

@author: lunet
"""
import numpy as np


def jacobiMatrix(alpha, beta):
    alpha = np.asarray(alpha).ravel()
    beta = np.asarray(beta).ravel()
    N = alpha.size
    if beta.size != N:
        raise ValueError('Wrong size for beta vector compared to alpha vector')
    J = np.diag(alpha) + np.diag(beta[1:], k=1) + np.diag(beta[1:], k=-1)
    return J


def computeQuadRule(alpha, beta, implementation='ADAPTIVE'):

    if implementation == 'ADAPTIVE':
        if len(alpha) > 1999:
            implementation = 'EIG_SCIPY'
        else:
            implementation = 'GOLUB_WELSCH_FORTRAN'
    elif implementation not in ['EIG_SCIPY',
                                'GOLUB_WELSCH_FORTRAN',
                                'GOLUB_WELSCH_PYTHON']:
        raise ValueError('Wrong implementation argument')

    if implementation == 'EIG_SCIPY':

        from scipy.linalg import eigh_tridiagonal

        nodes, V = eigh_tridiagonal(alpha, np.sqrt(beta[1:]))
        weights = beta[0]*(V[0, :].real)**2

        return nodes, weights

    elif implementation == 'GOLUB_WELSCH_PYTHON':

        alpha = np.asarray(alpha)
        n = alpha.size
        nodes, weights = np.zeros_like(alpha), np.zeros_like(alpha)

        # Generate copy of alpha, sqrt(beta),
        # that will be modified during computation
        a = alpha.copy()
        b = np.sqrt(beta[1:])
        beta0 = beta[0]

        # Machine precision
        eps = np.finfo(alpha.dtype).eps

        # Determine error tolerance relative to Jacobian
        srJ = abs(a)
        srJ[:-1] += abs(b)
        srJ[1:] += abs(b)
        normJ = max(srJ)
        epsJ = eps*normJ
        lam, lam1, lam2, rho = [normJ]*4

        # Initialize first weight
        weights[0] = 1.

        # Run algorithm, with m starting from last index
        m = n-1
        i, m1 = [m-1]*2
        k = 0
        nLoop = 0
        while True:

            nLoop += 1
            if nLoop > 1e6:
                raise RuntimeError('More than 1e6 loop done, exiting')

            if m == -1:  # First index reached and converged => END
                # First nodes sorting indexes
                ind = np.argsort(nodes)
                # Normalize weigths
                weights **= 2
                weights *= beta0
                return nodes[ind], weights[ind]

            if m1 >= 0:  # First index not reached yet
                if abs(b[m1]) <= epsJ:  # Precision reached
                    # Update values
                    nodes[m] = a[m]
                    rho = min([lam1, lam2])
                    # Update indexes
                    m = m1
                    i, m1 = [m-1]*2
                    continue
            else:
                # First index was reached
                nodes[0] = a[0]
                m = -1
                continue

            # Small off diagonal element means the matrix can be split
            if abs(b[i]) <= epsJ:
                k = i
            else:
                k = 0

            # Find the shift with the eigenvalues of lower 2x2 block
            b2 = b[m1]**2
            det = ((a[m1] - a[m])**2 + 4*b2)**(0.5)
            aTemp = a[m1] + a[m]
            if aTemp > 0:
                lam2 = (aTemp + det)/2.
            else:
                lam2 = (aTemp - det)/2.
            lam1 = (a[m1]*a[m] - b2)/lam2
            eigMax = max([lam1, lam2])
            if abs(eigMax - rho) <= abs(eigMax)/8.:
                lam = eigMax
            rho = eigMax

            # Transform block k to m
            cj = b[k]
            bk1 = a[k] - lam
            for j in range(k, m1+1):
                # Compute and apply rotations, cf (3.3) in Golub&Welsch-1969
                r = (cj**2 + bk1**2)**(0.5)
                sint, cost = cj/r, bk1/r
                aj = a[j]

                if j > 0:
                    b[j-1] = r
                if j < n-2:
                    cj = b[j+1]*sint
                    b[j+1] *= -cost

                f = aj*cost + b[j]*sint
                q = b[j]*cost + a[j+1]*sint

                a[j] = f*cost + q*sint
                b[j] = f*sint - q*cost

                wj = weights[j]
                a[j+1] = aj + a[j+1] - a[j]

                weights[j] = wj*cost + weights[j+1]*sint
                weights[j+1] = wj*sint - weights[j+1]*cost

                bk1 = b[j]

    elif implementation == 'GOLUB_WELSCH_FORTRAN':

        from . import libfor

        alpha, beta = np.asarray(alpha), np.asarray(beta)
        n = alpha.size
        nodes, weights = np.zeros_like(alpha), np.zeros_like(alpha)
        eps = np.finfo(alpha.dtype).eps

        libfor.golub_welsch(alpha, beta, nodes, weights, eps, n)

        ind = np.argsort(nodes)
        return nodes[ind].copy(), weights[ind].copy()
