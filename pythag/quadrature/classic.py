# -*- coding: utf-8 -*-
"""
Description
-----------

TODO
"""
# Dependency packages
import numpy as np
import scipy.interpolate as sp
import scipy.integrate as sq
# Pythag packages
from .base import QuadRule
from ..polynomials.classic import lagrange


class NewtonCotesRule(QuadRule):

    def __init__(self, order=0, distr='GAUSS', support=[-1., 1.]):

        # Inherited constructor
        super(NewtonCotesRule, self).__init__(support=support)

        # Check quadrature type
        if distr not in ['GAUSS', 'RADAU-I', 'RADAU-II', 'LOBATTO']:
            raise ValueError('distr={} for quadrature type is not available'
                             .format(distr))
        self.distr = distr
        # Check order
        if order not in [0, 1]:
            raise ValueError('wrong order ({})'.format(order))
        self.order = order

    @property
    def name(self):
        spec = 'G' if self.distr == 'GAUSS' else \
            'RI' if self.distr == 'RADAU-I' else \
            'RI' if self.distr == 'RADAU-II' else \
            'L' if self.distr == 'LOBATTO' else ''
        return 'NEWTON-COTES ({}{})'.format(spec, self.order)

    def _computeNodes(self, n):
        nodes = np.linspace(-1, 1, num=n, endpoint=(self.distr == 'LOBATTO'))
        h = nodes[1]-nodes[0]
        if self.distr == 'RADAU-II':
            nodes += h
        elif self.distr == 'GAUSS':
            nodes += h/2.

        return nodes

    def _computeWeights(self, n):
        weights = np.ones(n)
        # Order 0: approximate integrand by piecewise constant function
        if self.order == 0:
            if self.distr == 'LOBATTO':
                # Shifted rectangle rule
                h = 2./(n-1)
                weights *= h
                weights[[0, -1]] /= 2.
            elif self.distr in ['GAUSS', 'RADAU-I', 'RADAU-II']:
                # GAUSS: rectangle rule
                # RADAU-I: left rectangle rule
                # RADAU-II: right rectangle rule
                h = 2./n
                weights *= h
        # Order 1: approximate integrand by piecewise linear function
        elif self.order == 1:
            if self.distr == 'LOBATTO':
                h = 2./(n-1)
                weights *= h
                weights[[0, -1]] /= 2.
            else:
                raise NotImplementedError(
                    'Not implemented for order={} and distr={}'.format(
                        self.order, self.distr))
        else:
            raise NotImplementedError(
                    'Not implemented for order={} and distr={}'.format(
                        self.order, self.distr))

        return weights

    def _computeNormalizedNodesAndWeights(self, n):
        return self._computeNodes(n), self._computeWeights(n)


class FejerRule(QuadRule):

    def __init__(self, kind=1, weightComputation='DFT', support=[-1., 1.]):

        # Inherited constructor
        super().__init__(support=support)

        # Check kind
        if kind not in [1, 2]:
            raise ValueError('Fejer rule not implemented for kind={}'
                             .format(kind))
        self.kind = kind

        # Check weights computation
        if weightComputation not in ['SUM', 'DFT']:
            raise ValueError('No implementation with weightComputation={}'
                             .format(weightComputation))
        self.weightComputation = weightComputation

    @property
    def name(self):
        return 'FEJER-{}'.format(self.kind)

    def _computeNormalizedNodesAndWeights(self, n):

        # Initialize output variables
        n = int(n)
        nodes = np.empty(n, dtype=float)
        weights = np.empty(n, dtype=float)

        if self.kind == 1:

            # Compute nodes for first Fejer rule
            theta = np.arange(1, n+1, dtype=float)[-1::-1]
            theta *= 2
            theta -= 1
            theta *= np.pi/(2*n)
            np.cos(theta, out=nodes)

            if self.weightComputation == 'SUM':
                # Numpy-Optimized computation of sum formula
                # -- Initialize sum variables
                j = np.arange(1, int(n)/2 + 1, dtype=float)
                theta, j = np.meshgrid(theta, j)
                # -- Compute sums elements
                theta *= 2
                theta *= j
                np.cos(theta, out=theta)
                j **= 2
                j *= 4
                j -= 1
                theta /= j
                # -- Add sum to weights
                weights[:] = 1.
                s = np.sum(theta, axis=0)
                s *= 2
                weights -= s
                weights *= 2./n

            elif self.weightComputation == 'DFT':
                # Computation using DFT (Waldvogel 2006)
                # Inspired from quadpy code
                # quadpy/line_segment/fejer.py
                # @Nico Schlömer
                # -- Initial variables
                N = np.arange(1, n, 2)
                lN = len(N)
                m = n - lN
                K = np.arange(m)
                # -- Build v0
                v0 = np.concatenate([
                    2 * np.exp(1j*np.pi*K/n) / (1 - 4*K**2),
                    np.zeros(lN+1)
                    ])
                # -- Build v1 from v0
                v1 = np.empty(len(v0)-1, dtype=complex)
                np.conjugate(v0[:0:-1], out=v1)
                v1 += v0[:-1]
                # -- Compute inverse fourier transform
                w = np.fft.ifft(v1)
                if max(w.imag) > 1.0e-15:
                    raise ValueError('Max imaginary value to important for' +
                                     ' ifft: {}'.format(max(w.imag)))
                # -- Store weights
                weights[:] = w.real

        elif self.kind == 2:
            # Compute nodes for second Fejer rule
            theta = np.arange(1, n+1, dtype=float)[-1::-1]
            theta *= np.pi/(n+1)
            np.cos(theta, out=nodes)

            if self.weightComputation == 'SUM':
                # Numpy-Optimized computation of sum formula
                # -- Compute sin factor in front of sum
                np.sin(theta, out=weights)
                # -- Initialize sum variables
                j = np.arange(1, int(n+1)/2 + 1, dtype=float)
                theta, j = np.meshgrid(theta, j)
                # -- Compute sums elements
                j *= 2
                j -= 1
                theta *= j
                np.sin(theta, out=theta)
                theta /= j
                # -- Multiply sum by weights
                s = np.sum(theta, axis=0)
                weights *= s
                weights *= 4./(n+1)

            elif self.weightComputation == 'DFT':
                # Computation using DFT (Waldvogel 2006)
                # Inspired from quadpy code
                # quadpy/line_segment/fejer.py
                # @Nico Schlömer
                # -- Initial variables
                n += 1
                N = np.arange(1, n, 2)
                lN = len(N)
                m = n - lN
                # -- Build v0
                v0 = np.concatenate([2./N/(N-2), [1./N[-1]], np.zeros(m)])
                # -- Build v2 from v0
                v2 = np.empty(len(v0)-1)
                np.copyto(v2, v0[:-1])
                v2 *= -1
                v2 -= v0[:0:-1]
                # -- Compute inverse fourier transform,
                # using hermitian symmetry
                w = np.fft.ihfft(v2)
                if max(w.imag) > 1.0e-15:
                    raise ValueError('Max imaginary value to important for' +
                                     ' ihfft: {}'.format(max(w.imag)))
                w = w.real
                # -- Build weights, depending on n
                if n % 2 == 1:
                    w = np.concatenate([w, w[::-1]])
                else:
                    w = np.concatenate([w, w[len(w)-2::-1]])

                # -- Cut off first and last to obtain the weights vector
                weights[:] = w[1:-1]

        return nodes, weights


class ClenshawCurtisRule(QuadRule):

    def __init__(self, weightComputation='DFT', support=[-1., 1.]):

        # Inherited constructor
        super(ClenshawCurtisRule, self).__init__(support=support)

        # Check weights computation
        if weightComputation not in ['SUM', 'DFT', 'MAT']:
            raise ValueError('No implementation with weightComputation={}'
                             .format(weightComputation))
        self.weightComputation = weightComputation

    @property
    def name(self):
        return 'CLENSHAW-CURTIS'

    def _computeNormalizedNodesAndWeights(self, n):

        # Initialize output variables
        n = int(n)
        nodes = np.empty(n, dtype=float)
        weights = np.ones(n, dtype=float)

        # Compute nodes
        theta = np.arange(n, dtype=float)[-1::-1]
        theta *= np.pi/(n-1)
        np.cos(theta, out=nodes)

        if self.weightComputation == 'SUM':
            # Numpy-Optimized computation of sum formula
            # -- Initialize sum variables
            j = np.arange(1, int(n-1)/2 + 1, dtype=float)
            print(j, (n-1)/2)
            theta, j = np.meshgrid(theta, j)
            # -- Compute sums elements
            theta *= 2.
            theta *= j
            np.cos(theta, out=theta)
            j **= 2.
            j *= 4.
            j -= 1.
            theta /= j
            # -- Multiply sum element by two if needed
            ib2 = -2 - np.mod(n-1, 2)
            print(ib2)
            theta[:ib2, :] *= 2.
            # -- Add sum to weights
            weights[:] = 1.
            s = np.sum(theta, axis=0)
            s *= 2.
            weights -= s
            # -- Multiply by index depending factor
            weights[1:-1] *= 2./(n-1)
            weights[[0, -1]] *= 1./(n-1)

        elif self.weightComputation == 'MAT':
            # Matrix computation of weights
            W = np.arange(n, dtype=float)
            W[2::2] **= 2
            W[2::2] -= 1
            W[2::2] **= -1
            W[2::2] *= -2
            W[0] = 1
            W[1::2] = 0
            tn = np.arange(n, dtype=float)
            tnx, tny = np.meshgrid(tn, tn)
            L = tnx*tny
            L *= np.pi/(n-1)
            np.cos(L, out=L)
            L[:, 0] /= 2
            L[-1, 0] /= 2
            L *= 2./(n-1)
            np.dot(L.T, W, out=weights)

        elif self.weightComputation == 'DFT':
            # Computation using DFT (Waldvogel 2006)
            # Inspired from quadpy code
            # quadpy/line_segment/clenshaw_curtis.py
            # @Nico Schlömer
            # -- Obvious case
            if n == 2:
                return nodes, weights
            # -- Initial variables
            n -= 1
            N = np.arange(1, n, 2)
            lN = len(N)
            m = n - lN
            # -- Build v0
            v0 = np.concatenate([2./(N*(N-2)), [1./N[-1]], np.zeros(m)])
            # -- Build v2 from v0
            v2 = np.empty(len(v0)-1)
            np.copyto(v2, v0[:-1])
            v2 *= -1
            v2 -= v0[:0:-1]
            # -- Build g
            g = np.ones(n)
            g *= -1
            g[lN] += n
            g[m] += n
            # -- Build g0
            g /= (n**2 - 1 + (n % 2))
            # -- Build v2+g
            v2 += g
            # -- Compute inverse fourier transform, using hermitian symmetry
            w = np.fft.ihfft(v2)
            if max(w.imag) > 1.0e-15:
                raise ValueError('Max imaginary value to important for' +
                                 ' ihfft: {}'.format(max(w.imag)))
            w = w.real
            # -- Build weight vector, depending on n
            if n % 2 == 1:
                weights[:] = np.concatenate([w, w[::-1]])
            else:
                weights[:] = np.concatenate([w, w[len(w)-2::-1]])

        return nodes, weights


class LagrangeRule(QuadRule):

    def __init__(self, nodesFunc, support=[-1., 1.], name='LAGRANGE',
                 weightComputation='BARY'):

        super(LagrangeRule, self).__init__(support=support)

        # Store name of the quadrature rule
        self._name = name

        # Check nodesFunc
        nTest = 4
        try:
            nodes = np.asarray(nodesFunc(nTest))
        except TypeError:
            raise ValueError('nodesFunc argument should be a function, ' +
                             'take only one argument, and return a list ' +
                             'of nodes')
        if nodes.ndim != 1:
            raise ValueError('nodesFunc should return a 1d array')
        if nodes.size != nTest:
            raise ValueError('nodesFunc should return the same number of ' +
                             'nodes as asked')
        if np.any(nodes < -1) or np.any(nodes > 1):
            raise ValueError('nodesFunc should return nodes between -1 and 1')
        self._computeNodes = nodesFunc

        # Check weighComputation
        if weightComputation not in ['POLY', 'BARY']:
            raise ValueError('No implementation with weightComputation={}'
                             .format(weightComputation))
        self.weightComputation = weightComputation

    @property
    def name(self):
        return self._name

    def _computeNodes(self, n):
        raise NotImplementedError('Abstract function')

    def _computeNormalizedNodesAndWeights(self, n):

        nodes = self._computeNodes(n)

        if self.weightComputation == 'POLY':
            weights = np.array([lagrange(nodes, j).integ(-1, 1)
                                for j in range(n)])

        elif self.weightComputation == 'BARY':
            weights = np.zeros_like(nodes)
            bary = sp.BarycentricInterpolator
            yi = np.zeros_like(nodes)

            def computeWeight(i):
                yi[i] = 1.
                func = bary(nodes, yi)
                w = sq.quad(func, -1, 1)[0]
                yi[i] = 0.
                return w

            for i in range(n):
                weights[i] = computeWeight(i)

        return nodes, weights


class MonteCarloRule(QuadRule):

    def __init__(self, support=[-1., 1.]):

        # Inherited constructor
        super(MonteCarloRule, self).__init__(support=support)

    @property
    def name(self):
        return 'MONTE-CARLO'

    def _computeNormalizedNodesAndWeights(self, n):

        nodes = np.sort(np.random.uniform(-1., 1., size=n))

        delta = np.ediff1d(nodes)

        weights = np.concatenate([nodes[:-1]+delta/2., [1]]) - \
            np.concatenate([[-1.], nodes[:-1]+delta/2.])

        return nodes, weights
