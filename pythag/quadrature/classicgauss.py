# -*- coding: utf-8 -*-
"""
Description
-----------

TODO
"""
# Dependency packages
import numpy as np

# Pythag packages
from pythag.quadrature.gauss import GaussQuadRule
from pythag.recurrence import classic as _c3tr


class LegendreRule(GaussQuadRule):

    def __init__(self, shifted=False):
        self.rec = _c3tr.LegendreRec(shifted)


class ChebyshevRule(GaussQuadRule):

    def __init__(self, kind=1):
        self.rec = _c3tr.ChebyshevRec(kind)

    @property
    def kind(self):
        return int(self.name[-1])

    def getNodesAndWeights(self, n, qType='GAUSS', implementation='ADAPTIVE'):
        if qType != 'GAUSS':
            return super().getNodesAndWeights(n, qType, implementation)
        else:
            i = np.arange(n, dtype=float) + 1
            i = i[-1::-1]
            if self.kind == 1:
                nodes = np.cos((i - 0.5)/n*np.pi)
                weights = np.ones(n)*np.pi/n
            elif self.kind == 2:
                nodes = np.cos(i/(n+1)*np.pi)
                weights = np.pi/(n+1)*np.sin(i/(n+1)*np.pi)**2
            elif self.kind == 3:
                nodes = np.cos((i - 0.5)/(n + 0.5)*np.pi)
                weights = np.pi/(n + 0.5)*(1 + nodes)
            elif self.kind == 4:
                nodes = np.cos(i/(n + 0.5)*np.pi)
                weights = np.pi/(n + 0.5)*(1 - nodes)
            return nodes, weights


class JacobiRule(GaussQuadRule):
    def __init__(self, a, b):
        self.rec = _c3tr.JacobieRec(a, b)


class LaguerreRule(GaussQuadRule):

    def __init__(self, a=0):
        self.rec = _c3tr.GenLaguerre(a)


class HermiteRule(GaussQuadRule):

    def __init__(self, mu=0):
        self.rec = _c3tr.GenHermite(mu)


class MeixnerPollaczek(GaussQuadRule):

    def __init__(self, theta, lam):
        self.rec = _c3tr.MeixnerPollaczek(theta, lam)


class Bimodal(GaussQuadRule):

    def __init__(self, epsilon):
        self.rec = _c3tr.Bimodal(epsilon)


class Logistic(GaussQuadRule):

    def __init__(self):
        self.rec = _c3tr.Logistic()
