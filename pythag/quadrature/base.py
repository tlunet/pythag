# -*- coding: utf-8 -*-
"""
Description
-----------

TODO
"""
# Dependency packages
import numpy as np


class QuadRule(object):

    def __init__(self, support=[-1., 1.]):

        # Check support
        if support[1] <= support[0]:
            raise ValueError('Right bound ({}) inferior'.format(support[0]) +
                             ' or equal to left bound')
        if support[0] == -np.inf or support[1] == np.inf:
            raise ValueError('Cannot use infinite bound for quadrature')
        self.support = np.array(support, dtype=float).ravel().tolist()

    @property
    def name(self):
        return 'QUADRULE'

    def _computeNormalizedNodesAndWeights(self, n):
        raise NotImplementedError('Cannot use abstract class')

    def getNodesAndWeights(self, n):

        # Get normalized nodes
        nodes, weights = self._computeNormalizedNodesAndWeights(n)

        # Eventualy rescale considering support
        if not self.support == [-1., 1.]:
            a, b = self.support
            nodes *= b-a
            nodes += a+b
            nodes /= 2.
            weights *= (b-a)/2

        return nodes, weights
