#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 28 14:24:22 2018

@author: lunet
"""
# Python packages
import numpy as np

# Pythag packages
import pythag.quadrature.classic as classic
from pythag.quadrature.base import QuadRule
from pythag.recurrence.base import ThreeTermRec
from pythag.algebra.eigen import computeQuadRule


class GaussQuadRule(QuadRule):

    def __init__(self, weightFunc, support=[-1, 1], name='GAUSSQUADRULE'):
        self.rec = ThreeTermRec(name, support, weightFunc)

    @property
    def name(self):
        return self.rec.name

    @property
    def nCoeff(self):
        return self.rec.nCoeff

    @property
    def support(self):
        return self.rec.support

    @property
    def weightFunc(self):
        return self.rec.weightFunc

    def buildAlphaBeta(self, n):
        self.rec.buildAlphaBeta(n)

    def getAlphaBeta(self, n):
        return self.rec.getAlphaBeta(n)

    def resetAlphaBeta(self):
        self.rec.resetAlphaBeta()

    def getNodesAndWeights(self, n, qType='GAUSS', implementation='ADAPTIVE'):

        # Eventually build three term recurrence coefficients
        if self.nCoeff < n:
            self.buildAlphaBeta(n)

        # Get alpha, beta coefficients
        if qType == 'GAUSS':
            alpha, beta = self.rec.getAlphaBeta(n)
        elif qType in ['RADAU-I', 'RADAU-II']:
            alpha, beta = self.rec.getAlphaBeta(n)
            b = self.rec.support[0] if qType == 'RADAU-I' else \
                self.rec.support[-1]
            pi = self.rec.evalMonicPoly
            alpha[-1] = b - beta[-1]*pi(b, n-2)/pi(b, n-1)
        elif qType == 'LOBATTO':
            alpha, beta = self.rec.getAlphaBeta(n)
            pi = self.rec.evalMonicPoly
            a, b = self.rec.support
            a1, a2 = pi(a, n-1), pi(a, n-2)
            b1, b2 = pi(b, n-1), pi(b, n-2)
            alpha[-1], beta[-1] = np.linalg.solve(
                [[a1, a2],
                 [b1, b2]],
                [a*a1, b*b1])
        else:
            raise NotImplementedError(
                    f'{qType} quadrature type not implemented yet')
        # TODO : implementation of KONROD, ANTI-GAUSS, ...

        # Compute quadrature rule
        nodes, weights = computeQuadRule(alpha, beta, implementation)
        return nodes, weights

    def getAsymptoticApproximation(self, n, verbose=False):

        # Finite support [a,b]
        if self.support[0] != -np.inf and self.support[1] != np.inf:

            a, b = self.support
            i = np.arange(n)+1
            s = i/(n+1)
            nodes = (-(b-a)*np.cos(s*np.pi) + b + a)/2
            weights = self.rec.weightFunc(nodes)
            weights *= np.sqrt((nodes-a)*(b-nodes))
            weights *= np.pi/n

        # Infinite support [0,+inf]
        elif self.support[0] == 0 and self.support[1] == np.inf:
            s = np.linspace(0, 1, num=n+2)[1:-1]

            def fGaw(u):
                return 2/np.pi*((u/4*(1-u/4))**0.5 + np.arcsin((u/4)**0.5)) - s

            def invJac(u):
                return np.pi*u**0.5/(1-u/4)**0.5

            nodes = 4*(1-(1-s**2)**0.5)
            residii = np.linalg.norm(fGaw(nodes))
            count = 0
            eps = np.finfo(float).eps
            while residii > eps*n and count < 10:
                nodes = nodes - invJac(nodes)*fGaw(nodes)
                residii = np.linalg.norm(fGaw(nodes))
                count += 1
            if verbose:
                print('Computation of Gauss-Laguerre asymptotic nodes ' +
                      'distribution using Newton method')
                print(' -- nIter: {}'.format(count))
                print(' -- residual norm: {:1.3e} (tol: {:1.3e})'.format(
                         residii, eps*n))
            nodes *= n
            weights = self.rec.weightFunc(nodes)
            weights *= np.sqrt(nodes)
            weights *= np.pi/np.sqrt(n)

        # Infinite support [-inf,+inf]
        elif self.support[0] == -np.inf and self.support[1] == np.inf:
            s = np.linspace(0, 1, num=n+2)[1:-1]

            def fGaw(u):
                s2 = 2**0.5
                return 1/np.pi*(u/s2*(1-u**2/2)**0.5 +
                                np.arcsin(u/s2) + np.pi/2) - s

            def invJac(u):
                return 2**0.5 * np.pi/(4-2*u**2)**0.5

            nodes = 2*2**0.5*(s-0.5)
            residii = np.linalg.norm(fGaw(nodes))
            count = 0
            eps = np.finfo(float).eps
            while residii > eps*n and count < 10:
                nodes = nodes - invJac(nodes)*fGaw(nodes)
                residii = np.linalg.norm(fGaw(nodes))
                count += 1
            if verbose:
                print('Computation of Gauss-Hermite asymptotic nodes ' +
                      'distribution using Newton method')
                print(' -- nIter: {}'.format(count))
                print(' -- residual norm: {:1.3e} (tol: {:1.3e})'.format(
                         residii, eps*n))
            nodes *= n**0.5
            weights = self.rec.weightFunc(nodes)
            weights *= np.pi/np.sqrt(2*n)

        else:
            raise NotImplementedError(
                    'Asymptotic Approximation not implemented for this' +
                    'type of quadrature rule')

        return nodes, weights

    def getDiscreteApproximation(self, n, kind='F1'):
        if self.support[0] != -np.inf and self.support[1] != np.inf:
            kwargs = {'support': self.support}
        else:
            raise NotImplementedError(
                'Discrete approximation not implemented ' +
                'for unbounded support')
        if kind == 'NC':
            quadRule = classic.NewtonCotesRule(**kwargs)
        elif kind in ['F1', 'F2']:
            quadRule = classic.FejerRule(kind=int(kind[-1]), **kwargs)
        elif kind == 'CC':
            quadRule = classic.ClenshawCurtisRule(**kwargs)
        elif kind == 'CHEBY1':
            if not (self.support[0] == -1 and self.support[1] == 1):
                raise ValueError(
                    'Chebychev discrete rule cannot be used with support'
                    + ' other than [-1,1]')
            from .classicgauss import ChebyshevRule
            quadRule = ChebyshevRule(kind=1)
        else:
            raise NotImplementedError(
                'Unknown kind ({}) for discrete approximation'.format(kind))
        nodes, weights = quadRule.getNodesAndWeights(n)
        weights *= self.weightFunc(nodes)
        if kind == 'CHEBY1':
            weights *= (1-nodes**2)**(0.5)
        return nodes, weights
