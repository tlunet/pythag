# -*- coding: utf-8 -*-
"""
Description
-----------

TODO
"""
# Dependency packages
import numpy as np
from scipy.special import gamma
from scipy.integrate import quad
# Pythag packages
from .base import KnownThreeTermRec as _KnownThreeTermRec
from .measures import dico as mDico


class LegendreRec(_KnownThreeTermRec):

    def __init__(self, shifted=False):

        # Initialize object
        if shifted:
            support = [0, 1]
            name = 'LEGENDRE_SHIFTED'
        else:
            support = [-1, 1]
            name = 'LEGENDRE'

        super(LegendreRec, self).__init__(name, support, mDico['LEGENDRE'])

        # Set the correct coefficient computation method, and make the other
        # one unavailable
        def getError(n):
            raise AttributeError(
                'Cannot use this class method, please define the correct ' +
                'object at initialization with the shifted=(False or True) ' +
                'parameter.')
        if shifted:
            setattr(self, 'buildAlphaBeta', self._buildAlphaBetaShifted)
            setattr(self, '_buildAlphaBeta', getError)
        else:
            setattr(self, 'buildAlphaBeta', self._buildAlphaBeta)
            setattr(self, '_buildAlphaBetaShifted', getError)

    def _buildAlphaBeta(self, n):
        k = np.arange(n, dtype=float)
        self._alpha = 0*k
        self._beta = k**2/(4*k**2 - 1)
        self._beta[0] = 2

    def _buildAlphaBetaShifted(self, n):
        k = np.arange(n, dtype=float)
        self._alpha = np.full_like(k, 0.5)
        self._beta = k**2/(4*(4*k**2 - 1))
        self._beta[0] = 1


class ChebyshevRec(_KnownThreeTermRec):

    def __init__(self, kind=1):

        if kind not in [1, 2, 3, 4]:
            raise ValueError('Wrong kind ({}) for Chebyshev polynomial'
                             .format(kind))

        super(ChebyshevRec, self).__init__(
            'CHEBYSHEV_{:d}'.format(kind), [-1, 1], mDico['CHEBYSHEV'](kind))

        # Set the correct coefficient computation method, and make the others
        # unavailable
        def getError(n):
            raise AttributeError(
                'Cannot use this class method, please define the correct ' +
                'object at initialization with the kind=(1, 2, 3 or 4) ' +
                'parameter.')
        setattr(self, 'buildAlphaBeta',
                getattr(self, '_buildAlphaBeta{:d}'.format(kind)))
        kindToBlock = [1, 2, 3, 4]
        kindToBlock.remove(int(kind))
        for k in kindToBlock:
            setattr(self, '_buildAlphaBeta{:d}'.format(k), getError)

    def _buildAlphaBeta1(self, n):
        self._alpha = np.zeros(n)
        self._beta = np.full(n, 0.25)
        self._beta[0] = np.pi
        if n > 1:
            self._beta[1] = 0.5

    def _buildAlphaBeta2(self, n):
        self._alpha = np.zeros(n)
        self._beta = np.full(n, 0.25)
        self._beta[0] = np.pi/2

    def _buildAlphaBeta3(self, n):
        self._alpha = np.zeros(n)
        self._alpha[0] = 0.5
        self._beta = np.full(n, 0.25)
        self._beta[0] = np.pi

    def _buildAlphaBeta4(self, n):
        self._alpha = np.zeros(n)
        self._alpha[0] = -0.5
        self._beta = np.full(n, 0.25)
        self._beta[0] = np.pi


class JacobieRec(_KnownThreeTermRec):

    PRECISION_A = '{:3.2f}'
    PRECISION_B = '{:3.2f}'

    def __init__(self, a, b):

        if (not a > -1) or (not b > -1):
            raise ValueError('Wrong value for a,b coefficients: ' +
                             'they should be strictly greater than -1')

        a = float(self.PRECISION_A.format(a))
        b = float(self.PRECISION_B.format(b))

        super(JacobieRec, self).__init__(
            'JACOBI_{:3.2f}_{:3.2f}'.format(a, b),
            [-1, 1], mDico['JACOBI'](a, b))

    @property
    def a(self):
        return float(self.name.split('_')[1])

    @property
    def b(self):
        return float(self.name.split('_')[2])

    def buildAlphaBeta(self, n):
        k = np.arange(n, dtype=float)
        self._alpha, self._beta = np.empty_like(k), np.empty_like(k)
        a, b = self.a, self.b
        self._alpha[0] = (b-a)/(a+b+2)
        self._beta[0] = 2**(a+b+1)*gamma(a+1)*gamma(b+1)/gamma(a+b+2)
        if n > 1:
            k = k[1:]
            self._alpha[1:] = (b**2-a**2)/(2*k+a+b)/(2*k+a+b+2)
            self._beta[1] = 4*(1+a)*(1+b)/(2+a+b)**2/(3+a+b)
        if n > 2:
            k = k[1:]
            self._beta[2:] = 4*k*(k+a) * (k+b) * (k+a+b) / \
                (2*k+a+b)**2 / (2*k+a+b+1) / (2*k+a+b-1)


class GenLaguerre(_KnownThreeTermRec):

    PRECISION_A = '{:3.2f}'

    def __init__(self, a=0):

        if not a > -1:
            raise ValueError('Wrong value for a coefficient: ' +
                             'it should be strictly greater than -1')

        a = float(self.PRECISION_A.format(a))

        super().__init__(
                'LAGUERRE_'+self.PRECISION_A.format(a),
                [0, np.inf], mDico['LAGUERRE'](a))

    @property
    def a(self):
        return float(self.name.split('_')[1])

    def buildAlphaBeta(self, n):
        k = np.arange(n, dtype=float)
        self._alpha = 2*k + self.a + 1
        self._beta = k*(k + self.a)
        self._beta[0] = gamma(1 + self.a)


class GenHermite(_KnownThreeTermRec):

    PRECISION_MU = '{:3.2f}'

    def __init__(self, mu=0):

        if not mu > -0.5:
            raise ValueError('Wrong value for mu coefficient: ' +
                             'it should be strictly greater than -0.5')

        mu = float(self.PRECISION_MU.format(mu))

        super().__init__(
                'HERMITE_'+self.PRECISION_MU.format(mu),
                [-np.inf, np.inf], mDico['HERMITE'](mu))

    @property
    def mu(self):
        return float(self.name.split('_')[1])

    def buildAlphaBeta(self, n):
        k = np.arange(n, dtype=float)
        self._alpha = 0*k
        self._beta = k/2
        if n > 1:
            self._beta[1::2] += self.mu
        self._beta[0] = gamma(self.mu + 0.5)


class MeixnerPollaczek(_KnownThreeTermRec):

    PRECISION_THETA = '{:3.2f}'
    PRECISION_LAM = '{:3.2f}'

    def __init__(self, theta, lam):

        if not lam > 0:
            raise ValueError('Wrong value for lam(bda) coefficient: ' +
                             'it should be strictly greater than 0')

        if not theta > 0 or not theta < np.pi:
            raise ValueError('Wrong value for theta coefficient: ' +
                             'it should be strictly included in ]0, pi[')

        theta = float(self.PRECISION_THETA.format(theta))
        lam = float(self.PRECISION_LAM.format(lam))

        # Initialize object
        def weightFunc(t):
            return 1/(2*np.pi)*np.exp((2*theta-1)*np.pi*t) * \
                abs(gamma(lam+1j*t))**2

        super().__init__(
                'MEIXNER-POLLACZEK_' + self.PRECISION_THETA.format(theta)
                + '_' + self.PRECISION_LAM.format(lam),
                [-np.inf, np.inf], weightFunc)

    @property
    def theta(self):
        return float(self.name.split('_')[1])

    @property
    def lam(self):
        return float(self.name.split('_')[2])

    def buildAlphaBeta(self, n):
        k = np.arange(n, dtype=float)
        tpi = self.theta*np.pi
        if np.sin(tpi) == 1:
            self._alpha = 0*k
        else:
            self._alpha = -(k+self.lam)/np.tan(tpi)
        self._beta = k*(k+2*self.lam-1)/(2*np.sin(tpi))**2
        self._beta[0] = gamma(2*self.lam)/(2*np.sin(tpi))**(2*self.lam)


class Bimodal(_KnownThreeTermRec):

    PRECISION_EPSILON = '{:5.4f}'

    def __init__(self, epsilon):

        if not epsilon > 0:
            raise ValueError('Wrong value for epsilon coefficient: ' +
                             'it should be strictly greater than 0')

        epsilon = float(self.PRECISION_EPSILON.format(epsilon))

        # Initialize object
        def weightFunc(t):
            return np.exp(-1/epsilon*(t**4/4-t**2/2))

        super().__init__(
                'BIMODAL_' + self.PRECISION_EPSILON.format(epsilon),
                [-np.inf, np.inf], weightFunc)

    @property
    def epsilon(self):
        return float(self.name.split('_')[1])

    def buildAlphaBeta(self, n):
        k = np.arange(n, dtype=float)
        self._alpha = 0*k
        self._beta = 0*k

        def wt2(t):
            return self.weightFunc(t)*t**2

        if n > 1:
            self._beta[1] = quad(wt2, -np.inf, np.inf)[0] / \
                quad(self.weightFunc, -np.inf, np.inf)[0]
        if n > 2:
            for i in np.array(k[2:], dtype=int):
                self._beta[i] = (i-1)*self.epsilon/self._beta[i-1] - \
                    self._beta[i-1] - self._beta[i-2] + 1


class Logistic(_KnownThreeTermRec):

    def __init__(self):

        # Initialize object
        def weightFunc(t):
            return np.exp(-t)/(1+np.exp(-t))**2

        super().__init__('LOGISTIC', [-np.inf, np.inf], weightFunc)

    def buildAlphaBeta(self, n):
        """
        TODO: doc
        """

        # If the n first (alpha,beta) are already computed, do nothing
        if self.nCoeff >= n:
            return

        # Use Gautschi algorithm to compute the n (alpha,beta)
        print('Building the {:d} first (alpha,beta) with Gautschi algorithm'
              .format(n))
        print(' -- using a combination of Laguerre quadrature rules')

        from .comp import Orthpol as Orthpol
        from ..quadrature.classicgauss import LaguerreRule

        discrQuadRule = LaguerreRule()

        def approxQuadRule(n):
            nL, wL = discrQuadRule.getNodesAndWeights(n)
            wL /= (1+np.exp(-nL))**2
            nodes = np.hstack([-nL[-1::-1], nL])
            weights = np.hstack([wL[-1::-1], wL])
            return nodes, weights

        algo = Orthpol(approxQuadRule)

        algo = Orthpol(
            approxQuadRule, self.ORTHPOL_PRECISION, self.ORTHPOL_FACN0,
            self.ORTHPOL_DELTAN, self.ORTHPOL_NITERMAX)

        self._alpha, self._beta = algo.buildAlphaBeta(n)
