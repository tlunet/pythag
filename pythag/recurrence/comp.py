#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Description
-----------

TODO
"""
# Dependency packages
import numpy as np
# Pythag packages
from ..algebra.tridiag import tridiagRPKW as _tridiagRPKW
from ..algebra.tridiag import quadRuleMatrix as _quadRuleMatrix


class Orthpol(object):

    def __init__(self, approxQuadRule,
                 precision=1e-10, facN0=2, deltaN='N0', nIterMax=20,
                 computeCond=True, nComputeCondMax=2000):
        approxQuadRule.__doc__ = self.approxQuadRule.__doc__
        self.approxQuadRule = approxQuadRule

        self.precision = precision
        self.facN0 = facN0
        self.deltaN = deltaN
        self.nIterMax = nIterMax
        self.computeCond = computeCond
        self.nComputeCondMax = nComputeCondMax

    def approxQuadRule(self, n):
        """
        Function generating the nodes and weights to approximate the "exact"
        Gaussian quadrature rule

        .. math::
            \\int_{a}^{b} f(t)w(t) =
            \\sum_{i=1}^{n} \\omega_i f(\\tau_i) + R_n(f)

        with :math:`\\displaystyle \\lim_{n \\to +\\infty} R_n(f)=0` for
        any function and :math:`R_n(p)=0` for any polynomials
        :math:`p` of order :math:`2n+1`.
        The approximate nodes and weight :math:`(\\tau_i^D, \\omega_i^D)`
        should be such as

        .. math::
            \\sum_{i=1}^{n} \\omega_i p(\\tau_i) =
            \\sum_{i=1}^{n} \\omega_i^D p(\\tau_i^D)+ G_n(p)

        with :math:`\\displaystyle \\lim_{n \\to +\\infty} G_n(p)=0`
        for any polynomials of a given order.

        Parameter
        ---------
        n : int
            Number of nodes and weights

        Returns
        -------
        nodes : np.array of list of float
            The approximate nodes
        weights : np.array of list of float
            The approximate weights
        """
        raise NotImplementedError()

    def computeDiscreteAlphaBeta(self, n, N):
        nodes, weights = self.approxQuadRule(N)
        if self.computeCond and N <= self.nComputeCondMax:
            mA = _quadRuleMatrix(nodes, weights)
            print(' -- Tridiag. of arrow matrix (N={}), '.format(N) +
                  'condition number : {:3.2f}'.format(np.linalg.cond(mA)))
        alphaDiscr, betaDiscr = _tridiagRPKW(nodes, weights)
        return alphaDiscr[:n], betaDiscr[:n]

    def buildAlphaBeta(self, n):
        """
        TODO: doc
        """
        N0 = self.facN0*n
        deltaN = N0 if self.deltaN == 'N0' else int(self.deltaN)
        N1 = N0 + deltaN

        print('Running Gautschi algorithm (ORTHPOL)')
        print(' -- N0={:d}, deltaN={:d}'.format(N0, deltaN))
        print(' -- desired precision: {}'.format(self.precision))

        alpha0, beta0 = self.computeDiscreteAlphaBeta(n, N0)

        eps = np.finfo(alpha0.dtype).eps
        finish = False
        count = 0

        # Function evaluating the variation between two sets of coefficients
        def smallError(alpha0, alpha1, beta0, beta1):
            maxVarAlpha = max(abs(alpha0 - alpha1) - 10*eps)
            maxVarBeta = max(abs(beta0 - beta1) - 10*eps)
            print((' -- iter {:d}: N1={}, ' +
                   'maxVarAlpha={:1.2e}, maxVarBeta={:1.2e}')
                  .format(count, N1, maxVarAlpha, maxVarBeta))
            return np.all(maxVarAlpha <= self.precision) * \
                np.all(maxVarBeta <= self.precision)

        while not finish:
            # Update counter and escape if to many iterations
            count += 1
            if count > self.nIterMax:
                print(' -- Max number of iteration reached, exiting')
                break
            # Compute discrete (alpha, beta) for N1
            alpha1, beta1 = self.computeDiscreteAlphaBeta(n, N1)
            # Check variation with (alpha, beta) for N0
            finish = smallError(alpha0, alpha1, beta0, beta1)
            # Update values
            N0 = N1
            N1 = N0 + deltaN
            alpha0, beta0 = alpha1, beta1

        print(' -- Finished Gautschi algorithm')

        return alpha0, beta0
