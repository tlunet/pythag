#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 16:06:37 2019

@author: lunet
"""
import numpy as np
import matplotlib.pyplot as plt


class wF(object):
    """Weight function for a given measure
    (convert to string or print for more informations)

    Parameters
    ----------
    t : int or float or numpy.array
        Value(s) where to evaluate the weight function

    Return
    ------
    out : int or float or numpy.array
        Evaluation of the weight function (same format as t)
    """

    def __init__(self, func, expr):
        self.func = func
        self.expr = expr

    def __call__(self, t):
        t = np.asarray(t)
        return self.func(t)

    def __str__(self):
        return self.expr

    def __repr__(self):
        return 'w(t)='+self.__str__()

    def plot(self, interval=[-1, 1], figName=None):
        t = np.linspace(*interval, num=500)
        plt.figure(figName)
        plt.plot(t, self.__call__(t), lw=1.4,
                 label='$w(t)={}$'.format(self))
        plt.legend()
        plt.xlabel('$t$')
        plt.ylabel('$w(t)$')


class MeasureDataBase(object):

    def __init__(self):
        self._dico = {
            # Measure between [-1, 1]
            # -- classic
            'legendre':
                ['leg', 'legend', 'unit', 'unitary',
                 wF(lambda t: 0*t + 1, '1')],
            'chebyshev':
                ['cheby', 'cheb', 'chebychev', 'chebytchev',
                 lambda i:
                     wF(lambda t: (1-t**2)**(-0.5),
                        '1/\\sqrt{1-t^2}') if i == 1 else
                     wF(lambda t: (1-t**2)**(0.5),
                        '\\sqrt{1-t^2}') if i == 2 else
                     wF(lambda t: (1-t)**(-0.5)*(1+t)**(0.5),
                        '\\sqrt{1+t}/\\sqrt{1-t}') if i == 3 else
                     wF(lambda t: (1-t)**(0.5)*(1+t)**(-0.5),
                        '\\sqrt{1-t}/\\sqrt{1+t}') if i == 4 else None],
            'jacobi':
                ['jac', 'jacob',
                 lambda a, b:
                     wF(lambda t: (1-t)**a * (1+t)**b,
                        '(1-t)^{'+str(a)+'}(1+t)^{'+str(b)+'}')],
            # -- custom
            'pyram':
                [wF(lambda t: 1-abs(t), '1-|t|')],
            'saw':
                [wF(lambda t: t + (t < 0), 't + (t < 0)')],
            'electro':
                [wF(lambda t: 5/2*(t+1)*(t < -1+2/5) +
                    5/2*(t-3/5)*(t > 3/5) + 1*(t >= -1+2/5) +
                    5/2*(t+1/5)*(t > -1/5)*(t < 0) +
                    5/2*(t-1/5)*(t < 1/5)*(t >= 0), 'ELECTRO')],
            'funnel':
                [wF(lambda t: (t < 0)*(1-np.sqrt(abs(1-(t+1)**2))) +
                    (t >= 0)*(1-np.sqrt(abs(1-(t-1)**2))), 'FUNNEL')],
            'radiative':
                [lambda c: wF(lambda t: np.exp(-c/t), 'e^{-'+str(c)+'/t}')],

            # Measure between [0, +infty]
            'laguerre':
                ['lag', 'laguer',
                 lambda a:
                     wF(lambda t: t**a * np.exp(-t), 't^{'+str(a)+'}e^{-t}')],

            # Measure between [-infty, +infty]
            'hermite':
                ['herm', 'hermit',
                 lambda mu:
                     wF(lambda t: abs(t)**(2*mu) * np.exp(-t**2),
                        '|t|^{2'+str(mu)+'}e^{-t^2}')],
            }

        self._synonyms = {}
        for name in self._dico:
            for syn in self._dico[name][:-1]:
                self._synonyms.update({syn: name})

    def getMeasure(self, name):
        name = name.lower()
        if name in self._dico:
            return self._dico[name][-1]
        elif name in self._synonyms:
            return self._dico[self._synonyms[name]][-1]
        else:
            raise ValueError('{} measure not in database'.format(name))

    @property
    def names(self):
        return list(self._dico.keys())

    def __getitem__(self, key):
        return self.getMeasure(key)

    def __str__(self):
        return ' '.join(self.names)

    def __repr__(self):
        return 'Measures : {}'.format(self)


dico = MeasureDataBase()
