# -*- coding: utf-8 -*-
"""
Description
-----------

TODO
"""
# Dependency packages
import numpy as np
from inspect import signature as _sig
# Pythag packages
from .comp import Orthpol as _Orthpol
from ..polynomials.base import OrthogPoly as _OrthogPoly


class ThreeTermRec(object):

    # Parameters for Gautschi algorithm
    ORTHPOL_PRECISION = 1e-10
    ORTHPOL_FACN0 = 2
    ORTHPOL_DELTAN = 'N0'
    ORTHPOL_NITERMAX = 20
    ORTHPOL_DISCRULE = 'FEJER-1'
    ORTHPOL_COMPUTECOND = False
    ORTHPOL_NCOMPUTECONDMAX = 2000

    def __init__(self, name, support, weightFunc):

        # Name of the measure associated to the 3-term recurrence relation
        self.name = str(name)

        # Support of the measure
        self.support = np.array(support, dtype=float).ravel()

        # Check support and weight function
        self._checkMeasure(weightFunc)

        # Store weight function
        weightFunc.__doc__ = self.weightFunc.__doc__
        self.weightFunc = weightFunc

        # alpha-beta coefficients
        self._alpha = np.array([])
        self._beta = np.array([])

    def _checkMeasure(self, weightFunc):
        # -- check support
        if self.support.size != 2:
            raise ValueError('Support has size {}, has to be 2'
                             .format(self.support.size))
        if self.support[0] >= self.support[1]:
            raise ValueError(
                'Left support bound ({}) greater or equal to right one ({})'
                .format(*self.support))

        # -- check measure function
        if not callable(weightFunc):
            raise ValueError('weightFunc should be a function')
        if len(_sig(weightFunc).parameters) != 1:
            raise ValueError('weightFunc should take only one argument')
        aTest = -1 if self.support[0] == -np.inf else self.support[0]
        bTest = 1 if self.support[0] == np.inf else self.support[0]
        xTest = np.linspace(aTest, bTest, num=13)[1:-1]
        try:
            xTestEval = weightFunc(xTest)
        except Exception:
            raise ValueError('weight function does not work properly with ' +
                             'numpy ndarray')
        if not isinstance(xTestEval, np.ndarray):
            raise ValueError('weight function must return a numpy array')
        if not xTestEval.shape == xTest.shape:
            raise ValueError('weight function output array must have ' +
                             'the sameshape as the input array')

    @property
    def nCoeff(self):
        return int((len(self._alpha) + len(self._beta))/2)

    def weightFunc(self, t):
        """
        Weight function :math:`w` of the measure :math:`\\sigma` associated
        to the tree-terms recurrence relation.
        The scalar product of the measure is then :

        .. math::
            \\langle p, q\\rangle_{\\sigma} = \\int_{a}^{b} p(t)q(t)w(t)

        Parameter
        ---------
        t : numpy.ndarray, or float, or int
            The value(s) where to evaluate the function
        """
        raise NotImplementedError()

    # --- Variable change when support != [-1, 1]

    def phi(self, tau):
        a, b = self.support
        if a != -np.inf and b != np.inf:
            out = 0.5*tau
            out *= (b - a)
            out += 0.5*(b + a)
        elif b != np.inf:
            out = tau - 1
            out /= tau + 1
            out += b
        elif a != -np.inf:
            out = tau + 1
            out /= 1 - tau
            out += a
        else:
            out = tau/(1-tau**2)
        return out

    def phiPrim(self, tau):
        a, b = self.support
        if a != -np.inf and b != np.inf:
            out = np.full_like(tau, 0.5*(b-a))
        elif b != np.inf:
            out = 2./(tau + 1)**2
        elif a != -np.inf:
            out = 2./(tau - 1)**2
        else:
            out = (1-3*tau**2)/(1-tau**2)**2
        return out

    # --- (alpha,beta) computation

    def buildAlphaBeta(self, n):
        """
        TODO: doc
        """

        # If the n first (alpha,beta) are already computed, do nothing
        if self.nCoeff >= n:
            return

        # Use Gautschi algorithm to compute the n (alpha,beta)
        print('Building the {:d} first (alpha,beta) with Gautschi algorithm'
              .format(n))

        if self.ORTHPOL_DISCRULE == 'FEJER-1':
            from pythag.quadrature.classic import FejerRule
            discrQuadRule = FejerRule()

            def approxQuadRule(n):
                nodes, weights = discrQuadRule.getNodesAndWeights(n)
                if np.all(self.support == [-1, 1]):
                    weights *= self.weightFunc(nodes)
                else:
                    weights *= self.phiPrim(nodes)
                    nodes = self.phi(nodes)
                    weights *= self.weightFunc(nodes)
                return nodes, weights

        elif self.ORTHPOL_DISCRULE == 'HERMITE':
            from pythag.quadrature.classicgauss import HermiteRule
            discrQuadRule = HermiteRule()

            def approxQuadRule(n):
                nodes, weights = discrQuadRule.getNodesAndWeights(n)
                weights *= self.weightFunc(nodes)
                weights /= np.exp(-nodes**2)
                weights = np.nan_to_num(weights)
                return nodes, weights

        elif self.ORTHPOL_DISCRULE == 'AA':
            from pythag.quadrature.classicgauss import GaussQuadRule
            discrQuadRule = GaussQuadRule(
                self.weightFunc, self.support, self.name)

            def approxQuadRule(n):
                return discrQuadRule.getAsymptoticApproximation(n)

        else:
            raise ValueError('ORTHPOL_DISCRULE = {} not implemented'
                             .format(self.ORTHPOL_DISCRULE))

        print(' -- using {} for discrete quadrature rule'
              .format(discrQuadRule.name))

        algo = _Orthpol(
            approxQuadRule, self.ORTHPOL_PRECISION, self.ORTHPOL_FACN0,
            self.ORTHPOL_DELTAN, self.ORTHPOL_NITERMAX,
            self.ORTHPOL_COMPUTECOND, self.ORTHPOL_NCOMPUTECONDMAX)

        self._alpha, self._beta = algo.buildAlphaBeta(n)

    def getAlphaBeta(self, n):
        if self.nCoeff < n:
            self.buildAlphaBeta(n)
        return np.array(self._alpha[:n]), np.array(self._beta[:n])

    def resetAlphaBeta(self):
        self._alpha, self._beta = [], []

    # --- Orthogonal polynomials

    # -- monic polynomials --
    def evalMonicPoly(self, t, n):
        t = np.asarray(t)
        if self.nCoeff < n:
            self.buildAlphaBeta(n)
        return _OrthogPoly(*self.getAlphaBeta(n), normal=False)(t)

    def getMonicPoly(self, n):
        if self.nCoeff < n:
            self.buildAlphaBeta(n)
        return _OrthogPoly(*self.getAlphaBeta(n), normal=False).toPolynom()

    # -- orthonormal polynomials --
    def evalOrthnPoly(self, t, n):
        t = np.asarray(t)
        if self.nCoeff < n+1:
            self.buildAlphaBeta(n+1)
        return _OrthogPoly(*self.getAlphaBeta(n+1), normal=True)(t)

    def getOrthnPoly(self, n):
        if self.nCoeff < n+1:
            self.buildAlphaBeta(n+1)
        return _OrthogPoly(*self.getAlphaBeta(n+1), normal=True).toPolynom()

    # -- normalized polynomials --
    def evalNormzPoly(self, x, n, xNorm=1.):
        if self.nCoeff < n:
            self.buildAlphaBeta(n)
        out = self.evalMonicPoly(x, n)
        normFac = self.evalMonicPoly(xNorm, n)
        out /= normFac
        return out

    def getNormzPoly(self, n, xNorm=1.):
        if self.nCoeff < n:
            self.buildAlphaBeta(n)

        p = self.getMonicPoly(n)
        p /= p(xNorm)
        return p

    # -- special functions --
    def getChristoffelFunc(self, n):
        if self.nCoeff < n:
            self.buildAlphaBeta(n)

        def christoffel(x):
            res = np.zeros_like(np.asarray(x))
            for k in range(n):
                res += abs(self.evalOrthoPoly(x, k))**2
                res **= -1
            return res

        return christoffel


class KnownThreeTermRec(ThreeTermRec):

    def _checkMeasure(self, weightFunc):
        # Cancel support and weight function checking
        pass

    def buildAlphaBeta(self, n):
        # This method has to be implemented in the inherited class
        raise NotImplementedError(
            'Cannot use this abstract class, use an implemented one')
