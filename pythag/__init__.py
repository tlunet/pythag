import os
import sys
import glob
import subprocess
import matplotlib.pyplot as _plt

# General matplotlib settings
_plt.rc('font', size=12)
_plt.rcParams['lines.linewidth'] = 2
_plt.rcParams['axes.titlesize'] = 18
_plt.rcParams['axes.labelsize'] = 16
_plt.rcParams['xtick.labelsize'] = 16
_plt.rcParams['ytick.labelsize'] = 16
_plt.rcParams['xtick.major.pad'] = 5
_plt.rcParams['ytick.major.pad'] = 5
_plt.rcParams['axes.labelpad'] = 6
_plt.rcParams['markers.fillstyle'] = 'none'
_plt.rcParams['lines.markersize'] = 7.0
_plt.rcParams['lines.markeredgewidth'] = 1.5
_plt.rcParams['mathtext.fontset'] = 'cm'
_plt.rcParams['mathtext.rm'] = 'serif'
_plt.rcParams['figure.max_open_warning'] = 100


ABSPATH = os.path.dirname(os.path.abspath(__file__))
LIBFORPATHS = ['/algebra/', '/polynomials/', '/applications/']


def _genLibFor(overwrite=False):
    cd = os.path.abspath('.')
    for lfp in LIBFORPATHS:
        lfp = ABSPATH + lfp
        if not os.path.isfile(lfp+'libfor.so'.format()) or overwrite:
            os.chdir(lfp)
            if overwrite:
                os.system('rm -rf libfor*.so')
            print(' -- generating Fortran library libfor.so in {}'.format(lfp))
            __compileLibFor()
    os.chdir(cd)


def __compileLibFor():
    cmd = 'f2py -c -m libfor libfor.f95'
    with open(os.devnull, 'wb') as devnull:
        subprocess.check_call(
            cmd.split(), stdout=devnull, stderr=subprocess.STDOUT,
            env=os.environ.copy())
    if sys.version_info[0] == 3:
        libName = glob.glob('libfor.*.so')
        if len(libName) == 0:
            raise SystemError('Something went wrong with libfor compilation')
        if os.path.isfile('libfor.so'):
            os.remove('libfor.so')
        os.symlink(libName[0], 'libfor.so')


def updateLibFor():
    _genLibFor(overwrite=True)


# Generate libfor library at import (if necessary)
_genLibFor()

try:
    from .polynomials import base as _polybase
    from .recurrence import measures as _mDataBase
except ImportError:
    raise ImportError()

Polynom = _polybase.Polynom
OrthogPoly = _polybase.OrthogPoly
FactorPoly = _polybase.FactorPoly

mDict = _mDataBase.dico

__all__ = ["Polynom", "OrthogPoly", "FactorPoly", "mDict"]
