#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the definition of Lagrange polynomials objects,

Function
-------
lagrange :
    build a Lagrange polynomial
"""
from pythag.polynomials.base import Polynom


def lagrange(nodes, j):
    """
    Build the :math:`j^{th}` lagrange polynomial on the :math:`n`
    points :math:`(x_m)_{0 \\leq m \\leq n-1}`, defined as following :

    .. math::
        l_{j,(x_m)}(x) = \\prod_{0 \\leq m \\leq n, \\hspace{1mm} m \\neq j}
        \\frac{x-x_m}{x_j-x_m}

    Parameters
    ----------
    nodes : ``numpy.ndarray``
        The points :math:`(x_i)_{0 \\leq i < n}`
    j : ``int``
        The index of the lagrange polynomial (:math:`0 \\leq j < n`)

    Returns
    -------
    poly : ``Polynom``
        The lagrange polynomial
    """
    poly = Polynom('1')
    x = Polynom('x')
    xj = nodes[j]
    for xm in nodes[:j]:
        poly = poly*(x-xm)/(xj-xm)
    for xm in nodes[j+1:]:
        poly = poly*(x-xm)/(xj-xm)
    return Polynom(poly.c)
