subroutine polyval_horner(coeffs, xVal, yVal, nCoeff, nVal)
   implicit none

   ! Argument variables
   integer, intent(in) :: nCoeff, nVal
   double precision, intent(in) :: coeffs(nCoeff), xVal(nVal)
   double precision, intent(inout) :: yVal(nVal)

   ! Loop indexes
   integer :: i

   yVal(:) = 0.
   do i=1,nCoeff
      yVal(:) = yVal(:)*xVal(:) + coeffs(i)
   end do

end subroutine

subroutine orthogpoly_eval_monic(alpha, beta, xVal, yVal, deg, nVal)
   implicit none

   ! Argument variables
   integer, intent(in) :: deg, nVal
   double precision, intent(in) :: alpha(deg), beta(deg), xVal(nVal)
   double precision, intent(inout) :: yVal(nVal)

   ! Loop indexes
   integer :: j

   ! Internal variables
   double precision :: pi(nVal, 2)

   ! Recurrence computation
   pi(:, 1) = 0.
   pi(:, 2) = 1
   yVal(:) = 1
   do j=1,deg
      yVal(:) = yVal(:) * (xVal(:)-alpha(j))
      pi(:, 1) = pi(:, 1) * beta(j)
      yVal(:) = yVal(:) - pi(:, 1)
      pi(:, 1) = pi(:, 2)
      pi(:, 2) = yVal(:)
   end do

end subroutine

subroutine orthogpoly_eval_orthn(alpha, beta, xVal, yVal, deg, nVal)
   implicit none

   ! Argument variables
   integer, intent(in) :: deg, nVal
   double precision, intent(in) :: alpha(deg+1), beta(deg+1), xVal(nVal)
   double precision, intent(inout) :: yVal(nVal)

   ! Loop indexes
   integer :: j

   ! Internal variables
   double precision :: pi(nVal, 2)

   ! Recurrence computation
   pi(:, 1) = 0.
   pi(:, 2) = 1./sqrt(beta(1))
   yVal(:) = 1./sqrt(beta(1))
   do j=1,deg
      yVal(:) = yVal(:) * (xVal(:)-alpha(j))
      pi(:, 1) = pi(:, 1) * sqrt(beta(j))
      yVal(:) = yVal(:) - pi(:, 1)
      yVal(:) = yVal(:) / sqrt(beta(j+1))
      pi(:, 1) = pi(:, 2)
      pi(:, 2) = yVal(:)
   end do

end subroutine

subroutine factorpoly_eval(roots, coeff, xVal, yVal, deg, nVal)
   implicit none

   ! Argument variables
   integer, intent(in) :: deg, nVal
   double precision, intent(in) :: coeff, roots(deg), xVal(nVal)
   double precision, intent(inout) :: yVal(nVal)

   ! Loop indexes
   integer :: i

   ! Initialization
   yVal(:) = coeff

   do i=1,deg
      yVal(:) = yVal(:)*(xVal(:) - roots(i))
   end do


end subroutine

subroutine factorpoly_eval_with_deriv(roots, coeff, xVal, yVal, dVal, deg, nVal)
   implicit none

   ! Argument variables
   integer, intent(in) :: deg, nVal
   double precision, intent(in) :: coeff, roots(deg), xVal(nVal)
   double precision, intent(inout) :: yVal(nVal), dVal(nVal)

   ! Loop indexes
   integer :: i, j

   ! Internal variables
   double precision :: fac(nVal), prods(nVal, deg)

   ! Initialization
   yVal(:) = coeff
   dVal(:) = 0.
   prods(:,:) = 1.

   ! Computation loop
   do i=1,deg
      fac(:) = xVal(:) - roots(i)
      do j=1,i-1
         prods(:, j) = prods(:, j)*fac(:)
      end do
      do j=i+1,deg
         prods(:, j) = prods(:, j)*fac(:)
      end do
      yVal(:) = yVal(:)*fac(:)
   end do
   dVal(:) = sum(prods, 2)

   ! Multiply by coefficient if different from 1
   if (coeff /= 1) then
      dVal(:) = dVal(:)*coeff
   end if

end subroutine