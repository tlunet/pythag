# -*- coding: utf-8 -*-
"""
Description
-----------

Module containing the definition of polynomial objects, compatible with all
standard operations

Classes
-------
Polynom : overloaded(numpy.poly1d)
    Define a polynomial object
"""
import numpy as np

import pythag.polynomials.libfor as _libfor


class Polynom(np.poly1d):
    """
    Class defining a polynomial, with operator overloading, so a polynome
    object is compatible with classical ``+``, ``-``, ``*`` with ``integer``,
    ``float`` or ``Polynom`` objects, and ``**`` with ``integer``.
    This class is inherited from numpy.poly1d objects

    Parameters
    ----------
    descr : ``list`` or ``numpy.ndarray``, or ``str``
        The polynomial's coefficients, in decreasing powers, or if
        useRoots is set to True, the polynomial's roots.
        The polynomial can also be represented by a string, for example
        '1 + x^2 - x + 2'.

    useRoots : ``boolean``
        If the values given as a list

    Attributes
    ----------
    coeffs : ``numpy.ndarray`` of ``float``
        Coefficients of the polynomial, starting from the larger degree.
        Attribute aliases are: **c**, **coef**, **coefficients**
    deg : ``int``
        Degree of the polynomial
    nc : ``int``
        Number of coefficient of the polynomial (deg+1)
    r : ``numpy.ndarray`` of ``float`` or ``complex``
        The roots of the polynomial

    Examples
    --------
    >>> p = Polynom([3, 2, 1, 0])
    >>> print(p)
    3x^3 + 2x^2 + x
    >>> q = Polynom('x^2 + 1 - x + 5.9x^5')
    >>> print(q)
    5.9x^5 + x^2 - x + 1

    Methods
    -------
    eval(self, x) :
        Evaluates the polynomial with x

    deriv(self, m=0) :
        Returns the n-th derivative of the polynomial

    evalDeriv(self, m=0) :
        Evaluates the n-th derivative of the polynomial

    prim(self, c=0) :
        Returns the primitive of the polynomial

    integ(self, a, b) :
        Evaluate the integral of the polynomial between a and b

    roots(self, d=0) :
        Returns the roots of the n-th derivative of the polynomial
    """

    PRINT_PRECISION = '{:3.2f}'
    PRINT_MONOM = 'x'
    POLYVAL_METHOD = 'FORTRAN'

    # -------------------------------------------------------------------------
    # --- Constructor
    # -------------------------------------------------------------------------
    def __init__(self, descr, useRoots=False):
        if isinstance(descr, str):
            descr = self._readString(descr)
            useRoots = False
        super().__init__(np.asarray(descr, dtype=float), useRoots)

    def _readString(self, s):
        lm = [m.split('x')
              for m in s.replace(' ', '').replace('-', '+-').split('+')]
        for i in range(len(lm)):
            if lm[i][0] in ['', '-']:
                lm[i][0] += '1'
            if len(lm[i]) == 1:
                lm[i].append('0')
            else:
                lm[i][1] = lm[i][1].replace('^', '')
                if lm[i][1] == '':
                    lm[i][1] += '1'
            lm[i] = tuple(lm[i])
        lm = np.array(lm, dtype=[('c', float), ('p', int)])
        lm[::-1].sort(order='p')
        pMax = lm[0][1]
        coeffs = np.zeros(pMax + 1)
        for c, p in lm:
            coeffs[pMax-p] += c

        return coeffs

    # -------------------------------------------------------------------------
    # --- Properties
    # -------------------------------------------------------------------------
    @property
    def nc(self):
        return self.coeffs.size

    @property
    def deg(self):
        return self.order

    @property
    def coeffs(self):
        """ A copy of the polynomial coefficients """
        return self._coeffs

    # alias attributes
    c = coef = coefficients = coeffs

    # -------------------------------------------------------------------------
    # --- Class methods
    # -------------------------------------------------------------------------
    def eval(self, xVal):
        """
        Evaluate the polynomial in x

        Parameters
        ----------
        xVal : ``numpy.ndarray`` or ``list``
            Value where the polynomial is evaluated

        Returns
        -------
        out : ``numpy.ndarray`` or ``list``
            Evaluated values
        """
        return self.__call__(xVal)

    def deriv(self, m=1):
        """
        Compute the m-th derivative of the polynomial.

        Parameters
        ----------
        m : int
            Number of derivation (default=1)

        Returns
        -------
        out : Polynom
            The m-th derivative polynomial
        """
        return Polynom(super(Polynom, self).deriv(m=m)._coeffs)

    def evalDeriv(self, xVal, m=1):
        """Evaluate the m-th derivative of the polynomial

        Parameters
        ----------
        xVal : ``numpy.ndarray`` or ``list``
            Value where the polynomial derivative is evaluated
        m : int
            Number of derivation (default=1)

        Returns
        -------
        out : ``numpy.ndarray`` or ``list``
            Evaluated values
        """
        return self.deriv(m).__call__(xVal)

    def prim(self, c=0):
        """
        Return the primitive of the polynomial, associated to the constant c

        Parameters
        ----------
        c : ``float``
            The primitive constant (default=0)

        Returns
        -------
        out : Polynom
            The primitive polynomial
        """
        return Polynom(super(Polynom, self).integ(k=c))

    def integ(self, a, b):
        """
        Integrate the polynomial between ``a`` and ``b``, exactly.

        Parameters
        ----------
        a : ``float``
            Left bound of the integration interval
        b : ``float``
            Right bound of the integration interval
        """
        polyPrim = self.prim()
        return polyPrim(b)-polyPrim(a)

    def roots(self, d=0):
        """
        Compute the roots of the n-th derivative of the polynomial
        (uses numpy function, based on eigenvalue computation of the
        compagnon matrix)

        Note
        ----
        Roots of the polynomial can be accessed directly with the
        **r** attribute.

        Parameters
        ----------
        d : int
            Number of derivation (default=0)

        Returns
        -------
        out : ``numpy.ndarray``
            Roots (eventually complex) of the (derivated) polynomial
        """
        if d == 0:
            return np.sort(super(Polynom, self).roots)
        else:
            return np.sort(self.deriv(d).roots())

    def copy(self):
        """Return a copy of the polynomial"""
        return 1*self

    # -------------------------------------------------------------------------
    # --- Memory methods
    # -------------------------------------------------------------------------
    @property
    def memLoc(self):
        return hex(id(self._coeffs))

    @property
    def memSize(self):
        return self._coeffs.nbytes, self._coeffs.itemsize

    @property
    def memType(self):
        return self._coeffs.dtype

    # -------------------------------------------------------------------------
    # --- Evaluation methods
    # -------------------------------------------------------------------------
    def __call__(self, xVal):
        return self._polyval(xVal)

    def _polyval(self, xVal):
        if self.POLYVAL_METHOD == 'FORTRAN':
            return self._polyval_FORTRAN(xVal)
        elif self.POLYVAL_METHOD == 'NUMPY':
            return self._polyval_NUMPY(xVal)
        else:
            raise ValueError("POLYVAL_METHOD='{}' not implemented".format(
                self.POLYVAL_METHOD))

    def _polyval_NUMPY(self, xVal):
        return super(Polynom, self).__call__(xVal)

    def _polyval_FORTRAN(self, xVal):
        p = np.asarray(self.coeffs)
        if isinstance(xVal, np.poly1d):
            raise ValueError(
                'xVal cannot be a polynomial for this optimization of polyval')
        else:
            xVal = np.asarray(xVal, dtype=float)
            y = np.empty_like(xVal)
        _libfor.polyval_horner(p, xVal, y, p.size, xVal.size)
        return y

    def setPolyvalMethod(self, name=None):
        """Set the polyval method (used to evaluate the polynomial).

        Parameter
        ---------
        name : str
            The name of the method, possibilities are

            - name='NUMPY'
                Use the numpy implementation of polyval
            - name='FORTRAN'
                Use a (faster) Fortran implementation of polyval, default
                configuration in Pythag implementation.

            If called for an instance, name has to be specified, or an
            error is thrown.

        Note
        ----
        This function can be called on the Polynom class or any of its
        instances.

        - call on Polynom class :
            This modifies the polyval method for any other Polynom instance
            on wich the function has not been called.
        - call on Polynom instance :
            This modifies the polyval method only for this Polynom instance.

        Examples
        --------

        >>> from pythag import Polynom
        >>> p1 = Polynom('2x^2 + 4x+0.2')
        >>> p2 = Polynom('x^2 + 4x-1')
        >>> p1.POLYVAL_METHOD, p2.POLYVAL_METHOD
        ('FORTRAN', 'FORTRAN')
        >>> Polynom.setPolyvalMethod('NUMPY')
        >>> p1.POLYVAL_METHOD, p2.POLYVAL_METHOD
        ('NUMPY', 'NUMPY')
        >>> p1.setPolyvalMethod('FORTRAN')
        ('FORTRAN', 'NUMPY')
        >>> Polynom.setPolyvalMethod('FORTRAN')
        ('FORTRAN', 'FORTRAN')
        >>> Polynom.setPolyvalMethod('NUMPY')
        ('FORTRAN', 'NUMPY')

        The polyval method can be changed explicitely by setting a new
        value for the POLYVAL_METHOD attribute :

        >>> Polynom.POLYVAL_METHOD = 'NUMPY'

        However, this should be avoided, since no error will be trown if the
        new value is incorrect. But an error will occur latter when evaluating
        the polynomial :

        >>> p1.POLYVAL_METHOD = 'FORTRNA'
        >>> p1(1)
        ValueError: POLYVAL_METHOD='FORTRNA' not implemented
        """
        if name is None:
            if not isinstance(self, Polynom):
                name = self
                self = Polynom
            else:
                raise ValueError('name argument should be given')
        if name in ['FORTRAN', 'NUMPY']:
            self.POLYVAL_METHOD = name
        else:
            raise ValueError('POLYVAL_METHOD={} not implemented'.format(name))

    # -------------------------------------------------------------------------
    # --- Special methods - representation
    # -------------------------------------------------------------------------
    def __str__(self):
        coeffs = self.coeffs[-1::-1]
        s = ''
        for i in list(range(self.nc))[-1::-1]:
            c = coeffs[i]
            c = int(c) if int(c) == c else \
                float(self.PRINT_PRECISION.format(c))
            s = s + ' + '*int(c > 0)*int(s != '') + ' - '*int(c < 0) + \
                (str(abs(c))*int(abs(c) != 1 or i == 0) +
                 self.PRINT_MONOM*int(i > 0) + ('^'+str(i))*int(i > 1)) * \
                int(c != 0)
        if s == '':
            s = str(coeffs[0])
        return s

    def __repr__(self):
        return self.__str__() + \
            ' (Polynom of degree {}, memory location: {})'.format(
                    self.deg, self.memLoc)

    # -------------------------------------------------------------------------
    # --- Special methods - standard operations
    # -------------------------------------------------------------------------

    def __neg__(self):
        return Polynom(-self.coeffs)

    def __mul__(self, other):
        return Polynom(super(Polynom, self).__mul__(other).coeffs)

    def __rmul__(self, other):
        return Polynom(super(Polynom, self).__rmul__(other).coeffs)

    def __add__(self, other):
        return Polynom(super(Polynom, self).__add__(other).coeffs)

    def __radd__(self, other):
        return Polynom(super(Polynom, self).__radd__(other).coeffs)

    def __pow__(self, val):
        return Polynom(super(Polynom, self).__pow__(val).coeffs)

    def __sub__(self, other):
        return Polynom(super(Polynom, self).__sub__(other).coeffs)

    def __rsub__(self, other):
        return Polynom(super(Polynom, self).__rsub__(other).coeffs)

    def __truediv__(self, other):
        out = super(Polynom, self).__truediv__(other)
        if isinstance(out, np.poly1d):
            return Polynom(out.coeffs)
        else:
            return Polynom(out[0].coeffs), Polynom(out[1].coeffs)

    def __floordiv__(self, other):
        out = super(Polynom, self).__div__(other)
        if isinstance(out, np.poly1d):
            return Polynom(out.coeffs)
        else:
            return Polynom(out[0].coeffs), Polynom(out[1].coeffs)

    def __rtruediv__(self, other):
        out = super(Polynom, self).__rtruediv__(other)
        if isinstance(out, np.poly1d):
            return Polynom(out.coeffs)
        else:
            return Polynom(out[0].coeffs), Polynom(out[1].coeffs)

    def __rfloordiv__(self, other):
        out = super(Polynom, self).__rdiv__(other)
        if isinstance(out, np.poly1d):
            return Polynom(out.coeffs)
        else:
            return Polynom(out[0].coeffs), Polynom(out[1].coeffs)

    def __imul__(self, other):
        if np.isscalar(other):
            self._coeffs[:] *= other
            return self
        else:
            raise ValueError(
                    'Polynomials only support *= operation with scalar')

    def __iadd__(self, other):
        if np.isscalar(other):
            self._coeffs[-1] += other
            return self
        else:
            raise ValueError(
                    'Polynomials only support += operation with scalar')

    def __isub__(self, other):
        if np.isscalar(other):
            self._coeffs[-1] -= other
            return self
        else:
            raise ValueError(
                    'Polynomials only support -= operation with scalar')

    def __itruediv__(self, other):
        if np.isscalar(other):
            self._coeffs /= other
            return self
        else:
            raise ValueError(
                    'Polynomials only support /= operation with scalar')

    @staticmethod
    def monom():
        """Build a simple monomial polynomial

        .. math::
            P(x) = x
        """
        return Polynom('x')

    @staticmethod
    def unity():
        """Build the unity polynomials

        .. math::
            P(x) = 1
        """
        return Polynom('1')


monom = Polynom.monom
unity = Polynom.unity


class OrthogPoly(object):
    """DOCTODO"""

    PRINT_PRECISION = '{:3.2f}'
    EVALMONIC_METHOD = 'FORTRAN'
    EVALORTHN_METHOD = 'FORTRAN'

    # -------------------------------------------------------------------------
    # --- Constructor
    # -------------------------------------------------------------------------
    def __init__(self, alpha, beta, normal=False, deg=None):
        # Normal of monic orthogonal polynomial
        if not isinstance(normal, bool):
            raise ValueError('normal should be a boolean')
        # Default initialization using degree
        if deg is not None:
            if not isinstance(deg, int):
                raise ValueError('deg should be an integer')
            alpha = [0.]*(deg+normal)
            beta = [1.]*(deg+normal)
        # Alpha and beta coefficients
        alpha = np.asarray(alpha, dtype=float).ravel()
        beta = np.asarray(beta, dtype=float).ravel()
        if not len(alpha) == len(beta):
            raise ValueError('alpha and beta not of the same size')
        # Data structure
        self.normal = normal
        self._coeffs = np.vstack([alpha, beta])
        self._null = (deg == -1)

    # -------------------------------------------------------------------------
    # --- Properties
    # -------------------------------------------------------------------------
    @property
    def alpha(self):
        return self._coeffs[0]

    @property
    def beta(self):
        return self._coeffs[1]

    @property
    def recCoeffs(self):
        return self._coeffs

    @property
    def deg(self):
        return len(self.alpha) - self.normal if not self._null else -1

    # -------------------------------------------------------------------------
    # --- Class methods
    # -------------------------------------------------------------------------
    def eval(self, t):
        return self.__call__(t)

    def toPolynom(self):
        if self.normal:
            return self._toOrthnPoly()
        else:
            return self._toMonicPoly()

    def copy(self):
        """Return a copy of the polynomial"""
        return OrthogPoly(self.alpha, self.beta, self.normal)

    # -------------------------------------------------------------------------
    # --- Memory methods
    # -------------------------------------------------------------------------
    @property
    def memLoc(self):
        return hex(id(self._coeffs))

    @property
    def memSize(self):
        return self._coeffs.nbytes, self._coeffs.itemsize

    @property
    def memType(self):
        return self._coeffs.dtype

    # -------------------------------------------------------------------------
    # --- Private methods - conversion to Polynom
    # -------------------------------------------------------------------------
    def _toMonicPoly(self):
        t = monom()
        if self.deg == -1:
            return 0*t
        elif self.deg == 0:
            return 0*t + 1.
        else:
            pi_j = 0*t + 1.
            pi_jm1 = 0*t
            for alpha_j, beta_j in zip(self.alpha, self.beta):
                pi_jp1 = (t-alpha_j)*pi_j - beta_j*pi_jm1
                pi_jm1 = pi_j
                pi_j = pi_jp1
            return pi_jp1

    def _toOrthnPoly(self):
        t = monom()
        if self.deg == -1:
            return 0*t
        elif self.deg == 0:
            return 0*t + 1./self.beta[0]**0.5
        else:
            pi_j = 0*t + 1./self.beta[0]**0.5
            pi_jm1 = 0*t
            for alpha_j, beta_j, beta_jp1 in zip(self.alpha, self.beta,
                                                 self.beta[1:]):
                pi_jp1 = (t-alpha_j)*pi_j - (beta_j**0.5)*pi_jm1
                pi_jp1 /= (beta_jp1**0.5)
                pi_jm1 = pi_j
                pi_j = pi_jp1
            return pi_jp1

    # -------------------------------------------------------------------------
    # --- Private methods - evaluation
    # -------------------------------------------------------------------------
    def _evalMonic(self, t):
        if self.EVALMONIC_METHOD == 'FORTRAN':
            return self._evalMonic_FORTRAN(t)
        elif self.EVALMONIC_METHOD == 'PYTHON':
            return self._evalMonic_PYTHON(t)
        else:
            raise ValueError('EVALMONIC_METHOD={} not implemented'.format(
                self.EVALMONIC_METHOD))

    def _evalMonic_PYTHON(self, t):
        t = np.asarray(t, dtype=float)
        if self.deg == -1:
            return 0*t
        elif self.deg == 0:
            return 0*t + 1.
        else:
            pi = np.array([np.zeros_like(t) for i in range(3)])
            pi[1:] += 1
            for alpha_j, beta_j in zip(self.alpha, self.beta):
                pi[2] *= (t-alpha_j)
                pi[0] *= beta_j
                pi[2] -= pi[0]
                pi[0] = pi[1]
                pi[1] = pi[2]
            out = np.copy(pi[2])
            return out

    def _evalMonic_FORTRAN(self, t):
        t = np.asarray(t, dtype=float)
        if self.deg == -1:
            return 0*t
        elif self.deg == 0:
            return 0*t + 1.
        else:
            out = np.empty_like(t)
            _libfor.orthogpoly_eval_monic(
                self.alpha, self.beta, t, out, self.deg, t.size)
            return out

    def _evalOrthn(self, t):
        if self.EVALORTHN_METHOD == 'FORTRAN':
            return self._evalOrthn_FORTRAN(t)
        elif self.EVALORTHN_METHOD == 'PYTHON':
            return self._evalOrthn_PYTHON(t)
        else:
            raise ValueError('EVALORTHN_METHOD={} not implemented'.format(
                self.EVALORTHN_METHOD))

    def _evalOrthn_PYTHON(self, t):
        t = np.asarray(t, dtype=float)
        if self.deg == -1:
            return 0*t
        elif self.deg == 0:
            return 0*t + 1./self.beta[0]**0.5
        else:
            pi = np.array([np.zeros_like(t) for i in range(3)])
            pi[1:] += 1./self.beta[0]**0.5
            for alpha_j, beta_j, beta_jp1 in zip(self.alpha, self.beta,
                                                 self.beta[1:]):
                pi[2] *= (t-alpha_j)
                pi[0] *= beta_j**0.5
                pi[2] -= pi[0]
                pi[2] /= beta_jp1**0.5
                pi[0] = pi[1]
                pi[1] = pi[2]
            out = np.copy(pi[2])
            return out

    def _evalOrthn_FORTRAN(self, t):
        t = np.asarray(t, dtype=float)
        if self.deg == -1:
            return 0*t
        elif self.deg == 0:
            return 0*t + 1./self.beta[0]**0.5
        else:
            out = np.empty_like(t)
            _libfor.orthogpoly_eval_orthn(
                self.alpha, self.beta, t, out, self.deg, t.size)
            return out

    # -------------------------------------------------------------------------
    # --- Special methods - representation
    # -------------------------------------------------------------------------
    def __str__(self):
        if self.deg == -1:
            return '0'
        elif self.deg == 0:
            if self.normal:
                c = self.beta[0]**(-0.5)
                return str(int(c)) if int(c) == c else \
                    self.PRINT_PRECISION.format(c)
            else:
                return '1'
        else:
            s = 'alpha = ['
            for a in self.alpha:
                s += str(int(a)) if int(a) == a else \
                    self.PRINT_PRECISION.format(a)
                s += ' '
            s = s[:-1]
            s += '], beta = ['
            for b in self.beta:
                s += str(int(b)) if int(b) == b else \
                    self.PRINT_PRECISION.format(b)
                s += ' '
            s = s[:-1]
            s += ']'
            return s

    def __repr__(self):
        return self.__str__() + \
            ' ({} polynom of degree {}, memory location: {})'.format(
                    'Orthonormal' if self.normal else 'Monic orthogonal',
                    self.deg, self.memLoc)

    # -------------------------------------------------------------------------
    # --- Special methods - attributes
    # -------------------------------------------------------------------------
    def __setattr__(self, name, value):
        if hasattr(self, name):
            if name == 'normal' and not isinstance(value, bool):
                raise ValueError('normal attribute must be a boolean')
            elif name in ['alpha', 'beta', 'deg']:
                raise AttributeError('cannot set class methods')
            elif name in ['_coeffs', '_null']:
                raise AttributeError('cannot set private attribute')
        super(OrthogPoly, self).__setattr__(name, value)

    # -------------------------------------------------------------------------
    # --- Special methods - evaluation
    # -------------------------------------------------------------------------
    def __call__(self, t):
        if self.normal:
            return self._evalOrthn(t)
        else:
            return self._evalMonic(t)


class FactorPoly(object):
    """DOCTODO"""

    PRINT_MONOM = 'x'
    PRINT_PRECISION = '{:3.2f}'
    EVAL_METHOD = 'FORTRAN'
    EVAL_WITH_DERIV_METHOD = 'FORTRAN'

    def __init__(self, roots, coeff=1, deg=None):

        # Monic coefficient
        try:
            coeff = float(coeff)
        except Exception:
            raise ValueError('coeff must be convertible to float ({})'
                             .format(coeff))

        # Default initialization using degree
        if deg is not None:
            if not isinstance(deg, int):
                raise ValueError('deg must be an integer')
            roots = [0]*deg
            coeff *= (deg != -1)

        # Roots
        roots = np.asarray(roots).tolist()

        # Data structure
        self._data = np.array([coeff] + roots, dtype=float)

    # -------------------------------------------------------------------------
    # --- Properties
    # -------------------------------------------------------------------------
    @property
    def roots(self):
        return self._data[1:]

    @property
    def coeff(self):
        return self._data[0]

    @property
    def deg(self):
        return len(self.roots) - (self.coeff == 0)

    @property
    def rootsMult(self):
        singleRoots = []
        mult = []
        r0 = None
        for r in np.sort(self.roots):
            if r != r0:
                singleRoots.append(r)
                mult.append(1)
                r0 = r
            else:
                mult[-1] += 1
        return singleRoots, mult

    # -------------------------------------------------------------------------
    # --- Class methods
    # -------------------------------------------------------------------------
    def eval(self, t, dVal=False):
        return self.__call__(t, dVal)

    def toPolynom(self):
        t = monom()
        p = 0*t + 1.
        for r in self.roots:
            p = p*(t-r)
        p *= self.coeff
        return p

    def copy(self):
        """Return a copy of the polynomial"""
        return FactorPoly(self.roots, self.coeff)

    # -------------------------------------------------------------------------
    # --- Memory methods
    # -------------------------------------------------------------------------
    @property
    def memLoc(self):
        return hex(id(self._data))

    @property
    def memSize(self):
        return self._data.nbytes, self._data.itemsize

    @property
    def memType(self):
        return self._data.dtype

    # -------------------------------------------------------------------------
    # --- Private methods - evaluation
    # -------------------------------------------------------------------------
    def _eval(self, t):
        if self.EVAL_METHOD == 'FORTRAN':
            return self._eval_FORTRAN(t)
        elif self.EVAL_METHOD == 'NUMPY':
            return self._eval_NUMPY(t)
        else:
            raise ValueError(
                'EVAL_METHOD={} not implemented'.format(
                    self.EVAL_METHOD))

    def _eval_NUMPY(self, t):
        t = np.asarray(t, self.memType)
        out = np.prod([t-r for r in self.roots], axis=0)
        if self.coeff != 1:
            out *= self.coeff
        return out

    def _eval_FORTRAN(self, t):
        t = np.asarray(t, dtype=self.memType).ravel()
        val = np.empty_like(t)
        nVal = t.size
        _libfor.factorpoly_eval(
            self.roots, self.coeff, t, val, self.deg, nVal)
        return val

    def _evalWithDeriv(self, t):
        if self.EVAL_WITH_DERIV_METHOD == 'FORTRAN':
            return self._evalWithDeriv_FORTRAN
        elif self.EVAL_WITH_DERIV_METHOD == 'PYTHON':
            return self._evalWithDeriv_PYTHON
        else:
            raise ValueError(
                'EVAL_WITH_DERIV_METHOD={} not implemented'.format(
                    self.EVAL_WITH_DERIV_METHOD))

    def _evalWithDeriv_PYTHON(self, t):
        t = np.asarray(t, dtype=self.memType).ravel()
        val = np.full_like(t, self.coeff)
        prods = np.array([np.ones_like(val) for i in range(self.deg)])
        for i in range(self.deg):
            fac = t-self.roots[i]
            prods[:i] *= fac
            prods[i+1:] *= fac
            val *= fac
        dVal = np.sum(prods, axis=0)
        val *= self.coeff
        if self.coeff != 1:
            dVal *= self.coeff
        return val, dVal

    def _evalWithDeriv_FORTRAN(self, t):
        t = np.asarray(t, dtype=self.memType).ravel()
        val = np.empty_like(t)
        dVal = np.empty_like(t)
        nVal = t.size
        _libfor.factorpoly_eval_with_deriv(
            self.roots, self.coeff, t, val, dVal, self.deg, nVal)
        return val, dVal

    # -------------------------------------------------------------------------
    # --- Special methods - representation
    # -------------------------------------------------------------------------
    def __str__(self):
        if self.deg == -1:
            return '0'
        elif self.deg == 0:
            return str(int(self.coeff)) if int(self.coeff) == self.coeff \
                else self.PRINT_PRECISION.format(self.coeff)
        roots, mult = self.rootsMult
        par = (len(roots) > 1) or (self.coeff != 1)
        s = ''
        for r, m in zip(roots, mult):
            rPar = (par or (m > 1))*(r != 0)
            rStr = str(int(abs(r))) if int(r) == r \
                else self.PRINT_PRECISION.format(abs(r))
            s += rPar*'(' + self.PRINT_MONOM + \
                (r != 0)*(' - '*(r > 0) + ' + '*(r < 0) + rStr) + \
                rPar*')' + ('^{}'.format(m))*(m > 1)
            if len(roots) > 1:
                s += ' '
        if self.coeff != 1:
            cStr = str(int(self.coeff)) if int(self.coeff) == self.coeff \
                else self.PRINT_PRECISION.format(self.coeff)
            s = cStr+' '+s
        return s

    def __repr__(self):
        return self.__str__() + \
            ' (Factorized polynom of degree {}, memory location: {})'.format(
                    self.deg, self.memLoc)

    # -------------------------------------------------------------------------
    # --- Special methods - attributes
    # -------------------------------------------------------------------------
    def __setattr__(self, name, value):
        if hasattr(self, name):
            if name == 'coeff':
                try:
                    float(value)
                except Exception:
                    raise ValueError('coeff must be convertible to float')
                self._data[0] = value
            elif name in ['roots', 'deg']:
                raise AttributeError('cannot set class methods')
            elif name in ['_data']:
                raise AttributeError('cannot set private attribute')
            else:
                super(FactorPoly, self).__setattr__(name, value)
        else:
            super(FactorPoly, self).__setattr__(name, value)

    # -------------------------------------------------------------------------
    # --- Special methods - evaluation
    # -------------------------------------------------------------------------
    def __call__(self, t, dVal=False):
        if dVal:
            return self._evalWithDeriv(t)
        else:
            return self._eval(t)
