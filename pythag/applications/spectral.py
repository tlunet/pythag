#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 14:12:01 2021

@author: telu
"""
import numpy as np
import pythag.quadrature.classicgauss as qr
from pythag.quadrature.classic import FejerRule

# Pre-instantiated Gauss quadrature rules
APPROX_TYPES = {
    'LEGENDRE': qr.LegendreRule(),
    'CHEBY-1': qr.ChebyshevRule(1),
    'CHEBY-2': qr.ChebyshevRule(2),
    'CHEBY-3': qr.ChebyshevRule(3),
    'CHEBY-4': qr.ChebyshevRule(4)}


class SpectralApproximation(object):

    def __init__(self, n, pType='LEGENDRE', qType='GAUSS', bounds=[-1, 1]):
        self.n = n
        self.bounds = bounds
        try:
            self.qr = APPROX_TYPES[pType]
        except KeyError:
            if isinstance(pType, qr.GaussQuadRule):
                if np.allclose(pType.support, [-1, 1]):
                    self.qr = pType
                else:
                    raise ValueError(
                        'GaussQuadRule must have [-1,1] for support, ',
                        f'got {pType.support}')
            else:
                raise NotImplementedError(f'pType={pType}')
        self.nodes, self.weights = self.qr.getNodesAndWeights(n, qType)
        self.points = self.phi(self.nodes)

        Pi = self.getPi(self.nodes)
        gamma = 1./(Pi**2).dot(self.weights)

        self.P = gamma[:, None]*Pi*self.weights[None, :]

    def phi(self, tau):
        a, b = self.bounds
        return tau*(b-a)/2 + (b+a)/2

    def phiInv(self, t):
        a, b = self.bounds
        return (2*t - b - a)/(b-a)

    def pi(self, t, n):
        return self.qr.rec.evalMonicPoly(t, n)

    def getPi(self, tau):
        return np.array([self.pi(tau, i) for i in range(self.n)])

    def getInterpolationMatrix(self, times, rescale=True):
        times = np.asarray(times)
        if rescale:
            times = self.phiInv(times)
        PInter = self.getPi(times).T.dot(self.P)
        return PInter

    def getIntegrationMatrix(self, intervals, rescale=True):
        if rescale:
            intervals = [
                [self.phiInv(a), self.phiInv(b)] for a, b in intervals]

        iNodes, iWeights = FejerRule().getNodesAndWeights(self.n)

        def prod(t, i):
            return t * self.pi(t, i)

        # Simple implementation using for loops
        # -- 100x slower than numpy
        # intProd = np.sum(
        #         [[[(bj-aj)/2 * omega * prod((bj-aj)/2*tau + (bj+aj)/2, i)
        #            for tau, omega in zip(iNodes, iWeights)]
        #           for aj, bj in intervals]
        #          for i in range(self.n-1)], axis=-1)

        # Optimized implementation using numpy
        # --- could be optimized more, but it'll get ugly
        intervals = np.array(intervals)
        aj, bj = intervals[:, 0][:, None], intervals[:, 1][:, None]
        tau, omega = iNodes[None, :], iWeights[None, :]
        tEval = (bj-aj)/2*tau + (bj+aj)/2
        shape = tEval.shape
        intProd = [
            np.sum((bj-aj)/2 * omega * prod(tEval.ravel(), i).reshape(shape),
                   axis=-1)
            for i in range(self.n-1)]

        intPi = [np.array([0]*len(intervals)),
                 np.array([bj-aj for aj, bj in intervals])]

        alpha, beta = self.qr.getAlphaBeta(self.n-1)

        for i, al, be in zip(range(self.n-1), alpha, beta):
            intPi.append(intProd[i] - al*intPi[-1] - be*intPi[-2])
        intPi = np.array(intPi)[1:].T

        PInteg = intPi.dot(self.P)
        a, b = self.bounds
        PInteg *= (b-a)/2
        return PInteg

    def getDerivationMatrix(self, times, rescale=True):
        times = np.asarray(times)
        if rescale:
            times = self.phiInv(times)

        dPi = [0*times,
               0*times]

        alpha, beta = self.qr.getAlphaBeta(self.n-1)

        for i, al, be in zip(range(self.n-1), alpha, beta):
            dPi.append((times-al)*dPi[-1] + self.pi(times, i) - be*dPi[-2])
        dPi = np.array(dPi)[1:].T
        PDeriv = dPi.dot(self.P)
        a, b = self.bounds
        PDeriv /= (b-a)/2
        return PDeriv


if __name__ == '__main__':

    import matplotlib.pyplot as plt

    nQuad = 20
    sa = SpectralApproximation(nQuad, pType='CHEBY-4', qType='RADAU-II',
                               bounds=[-2, 1])

    def iFun(t):
        return np.exp(-t**2 * 10) - 1

    def fun(t):
        return -20*t*np.exp(-t**2 * 10)

    def dFun(t):
        return -20*np.exp(-t**2 * 10) + 400*t**2 * np.exp(-t**2 * 10)

    f = fun(sa.points)

    tEval = np.linspace(*sa.bounds, num=2000)
    dt = tEval[1]-tEval[0]
    fEval = sa.getInterpolationMatrix(tEval).dot(f)

    plt.figure('Interpolation')
    plt.plot(tEval, fun(tEval))
    plt.plot(sa.points, f, 'o')
    plt.plot(tEval, fEval)

    PInteg = sa.getIntegrationMatrix([(0, t) for t in tEval])
    ifEval = PInteg.dot(f)

    plt.figure('Integration')
    plt.plot(tEval, iFun(tEval))
    plt.plot(sa.points, iFun(sa.points), 'o')
    plt.plot(tEval, ifEval)

    PDeriv = sa.getDerivationMatrix(tEval)
    dfEval = PDeriv.dot(f)

    plt.figure('Derivative')
    plt.plot(tEval, dFun(tEval))
    plt.plot(sa.points, dFun(sa.points), 'o')
    plt.plot(tEval, dfEval)