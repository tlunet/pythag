#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 16:44:45 2021

@author: telu
"""
import numpy as np
from scipy.special import binom, roots_legendre

import pythag.quadrature.classicgauss as gqr
import pythag.quadrature.classic as cqr

DICO_GQR = {
    'LEGENDRE': gqr.LegendreRule(),
    'CHEBY-1': gqr.ChebyshevRule(kind=1),
    'CHEBY-2': gqr.ChebyshevRule(kind=2),
    'CHEBY-3': gqr.ChebyshevRule(kind=3),
    'CHEBY-4': gqr.ChebyshevRule(kind=4)}

DICO_CQR = {
    'FEJER-1': cqr.FejerRule(kind=1),
    'FEJER-2': cqr.FejerRule(kind=2),
    'CLENSHAW-CURTIS': cqr.ClenshawCurtisRule()}


class LagrangeApproximation(object):

    def __init__(self, points, weightComputation='STABLE_FORTRAN',
                 scaleRef='MAX', useAnalytic=True,
                 gaussWeight=lambda t: 0*t + 1, **kwargs):

        if isinstance(points, tuple):
            # Use classic point distribution
            pType, n = points
            # Distribution with weights already known
            j = np.arange(n)
            if pType == 'EQUID':
                points = np.linspace(-1, 1, num=n)
                self.weights = binom(n-1, j) * (-1)**j
            elif pType == 'CHEBY-I':
                j = j[-1::-1]
                points = np.cos((2*j+1)*np.pi/(2*n))
                self.weights = np.sin((2*j+1)*np.pi/(2*n)) * (-1)**j
            elif pType == 'CHEBY-II':
                j = j[-1::-1]
                points = np.cos(j*np.pi/(n-1))
                self.weights = np.array([0.5]+[1]*(n-2)+[0.5]) * (-1)**j
            elif pType == 'CHEBY-SCALED':
                j = j[-1::-1]
                points = np.cos((2*j+1)*np.pi/(2*n))
                points /= np.cos(np.pi/(2*n))
            # Distribution from Gauss quadrature rules
            elif pType in DICO_GQR:
                qr = DICO_GQR[pType]
                qType = kwargs.get('qType', 'GAUSS')
                points, _ = qr.getNodesAndWeights(n, qType)
            # Distribution from asymptotic limit of Gauss nodes
            elif pType == 'ASYMPT':
                qr = DICO_GQR['LEGENDRE']
                points, _ = qr.getAsymptoticApproximation(n)
                wt = gaussWeight(points)
                wt **= 0.5
                wt *= (1-points**2)**(0.75)
                wt *= (-1)**(j+1)
                self.weights = wt
            # Distribution from classic quadrature rules
            elif pType in DICO_CQR:
                qr = DICO_CQR[pType]
                points, _ = qr.getNodesAndWeights(n)
            else:
                raise NotImplementedError(f'pType={pType}')

        if hasattr(self, 'weights') and useAnalytic:
            # Quit initialization if weights are already computed analytically
            self.points = points
            self.useAnalytic = True
            return
        else:
            # Store interpolation points to compute the weights afterward
            self.useAnalytic = False
            points = np.asarray(points).ravel()

        # Compute weigths from given points
        if weightComputation.endswith('_FORTRAN'):

            import pythag.applications.libfor as libfor
            weightComputation = weightComputation[:-8]

            weights = np.empty_like(points)
            if weightComputation == 'FAST':
                libfor.barycentric_weights(points, weights)
            elif weightComputation == 'STABLE':
                libfor.barycentric_weights_logscale(points, weights)
            elif weightComputation == 'CHEBFUN':
                libfor.barycentric_weights_chebfun(points, weights)
            else:
                raise NotImplementedError(
                    f'weightComputation={weightComputation}_FORTRAN')
        else:

            diffs = points[:, None] - points[None, :]
            diffs[np.diag_indices_from(diffs)] = 1

            def analytic(diffs):
                # Fast implementation (unstable for large number of points)
                invProd = np.prod(diffs, axis=1)
                invProd **= -1
                return invProd

            def logScale(diffs):
                # Stable implementation (more expensive)
                sign = np.sign(diffs).prod(axis=1)
                wLog = -np.log(np.abs(diffs)).sum(axis=1)
                if scaleRef == 'ZERO':
                    wScale = wLog[np.argmin(np.abs(points))]
                elif scaleRef == 'MAX':
                    wScale = wLog.max()
                else:
                    raise NotImplementedError(f'scaleRef={scaleRef}')
                invProd = np.exp(wLog-wScale)
                invProd *= sign
                return invProd

            def chebfun(diffs):
                # Implementation used in chebfun
                diffs *= 4 / (points.max() - points.min())
                sign = np.sign(diffs).prod(axis=1)
                vv = np.exp(np.log(np.abs(diffs)).sum(axis=1))
                invProd = 1./(sign*vv)
                invProd /= np.linalg.norm(invProd, np.inf)
                return invProd

            if weightComputation == 'AUTO':
                with np.errstate(divide='raise', over='ignore'):
                    try:
                        invProd = analytic(diffs)
                    except FloatingPointError:
                        invProd = logScale(diffs)
            elif weightComputation == 'FAST':
                invProd = analytic(diffs)
            elif weightComputation == 'STABLE':
                invProd = logScale(diffs)
            elif weightComputation == 'CHEBFUN':
                invProd = chebfun(diffs)
            else:
                raise NotImplementedError(
                    f'weightComputation={weightComputation}')
            weights = invProd

        # Store attributes
        self.points = points
        self.weights = weights
        self.weightComputation = weightComputation

    @property
    def n(self):
        return self.points.size

    def getInterpolationMatrix(self, times):
        times = np.asarray(times)

        with np.errstate(divide='ignore'):
            iDiff = 1/(times[:, None] - self.points[None, :])
        concom = (iDiff == np.inf) | (iDiff == -np.inf)
        i, j = np.where(concom)
        iDiff[i, :] = concom[i, :]

        PInter = iDiff * self.weights
        PInter /= PInter.sum(axis=-1)[:, None]
        return PInter

    def getIntegrationMatrix(self, intervals, numQuad='FEJER-1'):

        if numQuad == 'FEJER-1':
            iNodes, iWeights = DICO_CQR['FEJER-1'].getNodesAndWeights(
                self.n - ((self.n + 1) % 2))
        elif numQuad == 'LEGENDRE_NUMPY':
            iNodes, iWeights = roots_legendre(self.n//2)
        else:
            raise NotImplementedError(f'numQuad={numQuad}')

        intervals = np.array(intervals)
        aj, bj = intervals[:, 0][:, None], intervals[:, 1][:, None]
        tau, omega = iNodes[None, :], iWeights[None, :]
        tEval = (bj-aj)/2*tau + (bj+aj)/2

        integrand = self.getInterpolationMatrix(tEval.ravel()).T.reshape(
            (-1,) + tEval.shape)
        integrand *= omega
        integrand *= (bj-aj)/2

        PInter = integrand.sum(axis=-1).T

        return PInter


if __name__ == '__main__':

    import matplotlib.pyplot as plt
    # from scipy.interpolate import BarycentricInterpolator

    nQuad = 31
    la = LagrangeApproximation(
        ('EQUID', nQuad), useAnalytic=False,
        weightComputation='CHEBFUN_FORTRAN')

    def iFun(t):
        return np.abs(t)*t/2 + t**2/4 - t**3/3

    def fun(t):
        return np.abs(t) + t/2 - t**2

    f = fun(la.points)

    tEval = np.linspace(-1, 1, num=2000)
    dt = tEval[1]-tEval[0]
    fEval = la.getInterpolationMatrix(tEval).dot(f)

    plt.figure('Interpolation')
    plt.plot(tEval, fun(tEval))
    plt.plot(la.points, f, 'o')
    plt.plot(tEval, fEval)

    PInteg = la.getIntegrationMatrix([(0, t) for t in tEval])
    ifEval = PInteg.dot(f)

    plt.figure('Integration')
    plt.plot(tEval, iFun(tEval))
    plt.plot(la.points, iFun(la.points), 'o')
    plt.plot(tEval, ifEval)
    plt.show()

    # nPoints = 6

    # fac = []
    # lN = [20]  # , 10, 20, 30, 40, 50, 100, 200, 300, 400, 500, 1000, 2000]
    # for nPoints in lN:
    #     points = qr.getAsymptoticApproximation(nPoints)[0]
    #     # points = np.cos(np.arange(nPoints)*np.pi/(nPoints-1))
    #     # points = np.linspace(-1, 1, nPoints)
    #     la = LagrangeApproximation(points)
    #     fac.append(max(np.abs(la.weights)))

    # PInteg = la.getIntegrationMatrix([[-1, 0], [0, 1], [-1, 1]])

    # plt.figure('Max weights')
    # plt.loglog(lN, fac)

    # ba = BarycentricInterpolator(points)

    # nEval = 100001

    # times = np.linspace(-1, 1, num=nEval)
    # res = la.getInterpolationMatrix(times)

    # fEval = fun(la.points)
    # plt.figure('Interpolation')
    # plt.plot(la.points, fEval, 'o')
    # plt.plot(times, res.dot(fEval))
    # plt.plot(times, fun(times))
