# Import submodules
from . import spectral
from . import lagrange

# Import main classes provided by each submodules
from .spectral import SpectralApproximation
from .lagrange import LagrangeApproximation

__all__ = ['spectral', 'SpectralApproximation',
           'lagrange', 'LagrangeApproximation']
