subroutine barycentric_weights_logscale(x, w, n)

   implicit none

   ! Argument variables
   integer, intent(in) :: n
   double precision, intent(in) :: x(n)
   double precision, intent(inout) :: w(n)

   ! Loop indexes
   integer :: i, k

   ! Intialize w
   w(:) = 0.

   ! Loops
   do i=1,n
      do k=1,i-1
         w(i) = w(i) - log(x(i)-x(k))
      end do
      do k=i+1,n
         w(i) = w(i) - log(x(k)-x(i))
      end do
   end do
   w(:) = w(:) - maxval(w)
   do i=1,n
      w(i) = exp(w(i))*(-1)**i
   end do

end subroutine

subroutine barycentric_weights(x, w, n)

   implicit none

   ! Argument variables
   integer, intent(in) :: n
   double precision, intent(in) :: x(n)
   double precision, intent(inout) :: w(n)

   ! Loop indexes
   integer :: i, k

   ! Intialize w
   w(:) = 1.

   ! Loops
   do i=1,n
      do k=1,i-1
         w(i) = w(i)*(x(i)-x(k))
      end do
      do k=i+1,n
         w(i) = w(i)*(x(i)-x(k))
      end do
   end do
   w(:) = 1./w(:)

end subroutine

subroutine barycentric_weights_chebfun(x, w, n)

   implicit none

   ! Argument variables
   integer, intent(in) :: n
   double precision, intent(in) :: x(n)
   double precision, intent(inout) :: w(n)

   ! Loop indexes
   integer :: j
   double precision :: C, v(n), vv

   ! Intialize w
   w(:) = 1.

   ! Compute interval capacity
   C = 4 / (maxval(x)-minval(x))

   ! Loops
   do j=1,n
      v(:) = C*(x(j)-x(:))
      v(j) = 1
      vv = exp(sum(log(abs(v))))
      w(j) = (-1.)**j/vv
   end do

   w(:) = w(:)/maxval(abs(w))


end subroutine

